#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/sched.h>
#include <linux/errno.h>
#include <linux/string.h>
#include <linux/tty.h>
#include <linux/kd.h>
#include <linux/vt.h>
#include <linux/console_struct.h>
#include <linux/skbuff.h>
#include <net/net_namespace.h>
#include <linux/spinlock.h>
#include <linux/kthread.h>
#include <linux/kfifo.h>

#define DCC_DEBUG
#define DCC_PREFIX	"DCC MGMT"

#include "dcc_cross.h"
#include "dcc_mgmt.h"
#include "dcc_err.h"


static int mode = G5CC;

struct ndlType_ndl_database *global_ndl_database;
struct dpType_dp_table *global_dp_state_sch;
//struct dpType_dchannel_state *global_dp_state_cch;

MODULE_AUTHOR ("Vit Vancura <vancuvit@fel.cvut.cz>, CTU in Prague");
MODULE_DESCRIPTION ("DCC_access support module of part 802.11p");
MODULE_LICENSE("GPL");

/**
*
*	NETLINK related
*
**/
static int init_netlink_kernel(void);
static int destroy_netlink_kernel(void);
static int ndl_save_received_attributes(struct sk_buff *_skb, struct genl_info *_info);
static int ndl_send_measured_attributes(struct sk_buff *_skb, struct genl_info *_info);
static int ndl_save_rx_stats(struct sk_buff *_skb, struct genl_info *_info);




static struct task_struct *control_loop;

struct kfifo dcc_miq_data;
EXPORT_SYMBOL(dcc_miq_data);

struct kfifo dcc_imq_data;
EXPORT_SYMBOL(dcc_imq_data);



static int init_memory(int _mode)
{
        int _status = 0;
        unsigned long flags;
        _status |= init_NDL_memory(&global_ndl_database);
        //_status |= init_DP_table(&global_dp_state_cch);
        _status |= init_DP_table(&global_dp_state_sch);

        spin_lock_irqsave(&(global_ndl_database->database_in_use), flags);
        _status |= set_NDL_default_values(&global_ndl_database, _mode);
        spin_unlock_irqrestore(&(global_ndl_database->database_in_use),flags);

        //spin_lock_irqsave(&(global_dp_state_cch->table_in_use), flags);
        //_status |= set_default_values_DP_cch(&global_dp_state_cch);
        //spin_unlock_irqrestore(&(global_dp_state_cch->table_in_use),flags);

        spin_lock_irqsave(&(global_dp_state_sch->table_in_use), flags);
        _status |= set_default_values_DP_sch(&global_dp_state_sch);
        spin_unlock_irqrestore(&(global_dp_state_sch->table_in_use),flags);

        return _status;
}

static int destroy_memory()
{
        int _status = 0;
        destroy_NDL_memory(&global_ndl_database);
        //destroy_DP_table(&global_dp_state_cch);
        destroy_DP_table(&global_dp_state_sch);
        return _status;
}

/**
*
*       Exchange data with local access entities. Possible to add array of all and communicate with more access entities
*
**/
static void dcc_process_global_data(u8 index)
{
	void *tempStruct;
	struct ndlType_ndl_database *database_ptr;
	struct dpType_dp_table *dp_table_ptr;
	struct ndlType_channel_load_measures *channel_load_measures_ptr;
	struct ndlType_transmit_packet_statictics *tx_statistics_ptr;
	struct ndlType_communication_ranges *comm_ranges_ptr;

	int temp = 0;
	int _status = 0;
//if
	while(!kfifo_is_empty(&dcc_imq_data)) {
		_status = kfifo_out(&dcc_imq_data, &tempStruct, sizeof(void *));
		temp = *((unsigned char*)tempStruct);

		switch (temp) {
		case msg_NDL_DATABASE:
			database_ptr = (struct ndlType_ndl_database *)tempStruct;
			dcc_save_ref_values_to_gNDL_database(&database_ptr, &global_ndl_database);
			dcc_process_NDL_databases(&database_ptr, &global_ndl_database);
			break;
		case msg_DP_TABLE:
			dp_table_ptr = (struct dpType_dp_table *)tempStruct;
			dcc_process_DP_tables(&dp_table_ptr, &global_dp_state_sch);
			break;
		case msg_NDL_COMM_RANGES:
			comm_ranges_ptr = (struct ndlType_communication_ranges *)tempStruct;
			dcc_save_comm_ranges_to_gNDL_database(&comm_ranges_ptr, &global_ndl_database);
			break;
		case msg_NDL_CHANNEL_LOAD_MEASURES:
			channel_load_measures_ptr = (struct ndlType_channel_load_measures *)tempStruct;
			dcc_save_load_measures_to_gNDL_database(&channel_load_measures_ptr, &global_ndl_database);
			break;
		case msg_NDL_TRANSMIT_PCK_STATS:
			tx_statistics_ptr = (struct ndlType_transmit_packet_statictics *)tempStruct;
			dcc_save_tx_stats_to_gNDL_database(&tx_statistics_ptr, &global_ndl_database);
			break;
		case msg_MI_COMMAND_REQ:
				break;
		case msg_MI_COMMAND_CONF:
				break;
		case msg_MI_REQUEST_REQ:
				break;
		case msg_MI_REQUEST_CONF:
				break;
		case msg_MI_SET_REQ:
				break;
		case msg_MI_SET_CONF:
				break;
		case msg_MI_GET_REQ:
				break;
			case msg_MI_GET_CONF:
				break;
		}
	}
}

int t_loop(void *arg)
{

        while (!kthread_should_stop())
	{
		kfifo_in(&dcc_miq_data, &global_dp_state_sch, sizeof(struct dpType_dp_table *));

		// receive data from access
                dcc_process_global_data(0);
                msleep(1000);
	}
	return 0;
}

static int init_control_loop()
{
        int _status = 0;

        control_loop = kthread_create(t_loop, NULL,"DCC_MGMT"); // instead of NULL can be pushed attribute
        wake_up_process(control_loop);

        return _status;
}


static int destroy_control_loop()
{
        int _status = 0;
        kthread_stop(control_loop);
        return _status;
}


int init_dcc_module(void)
{
	int _status = 0;
        _status |= init_memory(mode);
        _status |= init_netlink_kernel();

        _status |= init_control_loop();

        print_info("DCC_mgmt support module loaded successfully!");
        return _status;
}


void cleanup_dcc_module(void)
{
        destroy_control_loop();
	destroy_netlink_kernel();

        destroy_memory();

        print_info("DCC_mgmt support module unloaded successfully!");
}

module_init(init_dcc_module);
module_exit(cleanup_dcc_module);


/**
*
*	NETLINK related functions
*
*
**/


static struct genl_family ndl_nlm_family = {
        .id         = GENL_ID_GENERATE,
        .name       = "DCC_PROTOCOL",
        .version    = 1,
        .maxattr    = NDL_ATTR_MAX,
};

static struct nla_policy ndl_attr_msg_policy [NDL_ATTR_MAX + 1]= {
        [NDL_MIN_TX_POWER] = { .len = sizeof(ndlType_tx_power) },
        [NDL_MAX_TX_POWER] = { .len = sizeof(ndlType_tx_power) },
        [NDL_DEF_TX_POWER_INDEX_0] = { .len = sizeof(ndlType_tx_power) },
        [NDL_DEF_TX_POWER_INDEX_1] = { .len = sizeof(ndlType_tx_power) },
        [NDL_DEF_TX_POWER_INDEX_2] = { .len = sizeof(ndlType_tx_power) },
        [NDL_DEF_TX_POWER_INDEX_3] = { .len = sizeof(ndlType_tx_power) },
        [NDL_REF_TX_POWER_INDEX_0] = { .len = sizeof(ndlType_tx_power) },
        [NDL_REF_TX_POWER_INDEX_1] = { .len = sizeof(ndlType_tx_power) },
        [NDL_REF_TX_POWER_INDEX_2] = { .len = sizeof(ndlType_tx_power) },
        [NDL_REF_TX_POWER_INDEX_3] = { .len = sizeof(ndlType_tx_power) },

        [NDL_MAX_PACKET_DURATION_INDEX_0] = { .len = sizeof(ndlType_packet_duration) },
        [NDL_MAX_PACKET_DURATION_INDEX_1] = { .len = sizeof(ndlType_packet_duration) },
        [NDL_MAX_PACKET_DURATION_INDEX_2] = { .len = sizeof(ndlType_packet_duration) },
        [NDL_MAX_PACKET_DURATION_INDEX_3] = { .len = sizeof(ndlType_packet_duration) },
        [NDL_MIN_PACKET_INTERVAL] = { .len = sizeof(ndlType_packet_interval) },
        [NDL_MAX_PACKET_INTERVAL] = { .len = sizeof(ndlType_packet_interval) },
        [NDL_DEF_PACKET_INTERVAL_INDEX_0] = { .len = sizeof(ndlType_packet_interval) },
        [NDL_DEF_PACKET_INTERVAL_INDEX_1] = { .len = sizeof(ndlType_packet_interval) },
        [NDL_DEF_PACKET_INTERVAL_INDEX_2] = { .len = sizeof(ndlType_packet_interval) },
        [NDL_DEF_PACKET_INTERVAL_INDEX_3] = { .len = sizeof(ndlType_packet_interval) },
        [NDL_REF_PACKET_INTERVAL_INDEX_0] = { .len = sizeof(ndlType_packet_interval) },
        [NDL_REF_PACKET_INTERVAL_INDEX_1] = { .len = sizeof(ndlType_packet_interval) },
        [NDL_REF_PACKET_INTERVAL_INDEX_2] = { .len = sizeof(ndlType_packet_interval) },
        [NDL_REF_PACKET_INTERVAL_INDEX_3] = { .len = sizeof(ndlType_packet_interval) },

        [NDL_MIN_DATARATE] = { .len = sizeof(ndlType_datarate) },
        [NDL_MAX_DATARATE] = { .len = sizeof(ndlType_datarate) },
        [NDL_DEF_DATARATE_INDEX_0] = { .len = sizeof(ndlType_datarate) },
        [NDL_DEF_DATARATE_INDEX_1] = { .len = sizeof(ndlType_datarate) },
        [NDL_DEF_DATARATE_INDEX_2] = { .len = sizeof(ndlType_datarate) },
        [NDL_DEF_DATARATE_INDEX_3] = { .len = sizeof(ndlType_datarate) },
        [NDL_REF_DATARATE_INDEX_0] = { .len = sizeof(ndlType_datarate) },
        [NDL_REF_DATARATE_INDEX_1] = { .len = sizeof(ndlType_datarate) },
        [NDL_REF_DATARATE_INDEX_2] = { .len = sizeof(ndlType_datarate) },
        [NDL_REF_DATARATE_INDEX_3] = { .len = sizeof(ndlType_datarate) },

        [NDL_MIN_CARRIER_SENSE] = { .len = sizeof(ndlType_rx_power) },
        [NDL_MAX_CARRIER_SENSE] = { .len = sizeof(ndlType_rx_power) },
        [NDL_DEF_CARRIER_SENSE] = { .len = sizeof(ndlType_rx_power) },
        [NDL_REF_CARRIER_SENSE] = { .len = sizeof(ndlType_rx_power) },

        [NDL_DEF_DCC_SENSITIVITY] = { .len = sizeof(ndlType_rx_power) },
        [NDL_MAX_CS_RANGE] = { .len = sizeof(ndlType_distance) },
        [NDL_REF_PATHLOSS] = { .len = sizeof(ndlType_pathloss) },
        [NDL_MIN_SNR] = { .len = sizeof(ndlType_snr) },

        [NDL_SNR_BACKOFF_INDEX_0] = { .len = sizeof(ndlType_snr) },
        [NDL_SNR_BACKOFF_INDEX_1] = { .len = sizeof(ndlType_snr) },
        [NDL_SNR_BACKOFF_INDEX_2] = { .len = sizeof(ndlType_snr) },
        [NDL_SNR_BACKOFF_INDEX_3] = { .len = sizeof(ndlType_snr) },
        [NDL_SNR_BACKOFF_INDEX_4] = { .len = sizeof(ndlType_snr) },
        [NDL_SNR_BACKOFF_INDEX_5] = { .len = sizeof(ndlType_snr) },
        [NDL_SNR_BACKOFF_INDEX_6] = { .len = sizeof(ndlType_snr) },
        [NDL_SNR_BACKOFF_INDEX_7] = { .len = sizeof(ndlType_snr) },

        [NDL_TM_PACKET_ARRIVAL_RATE_INDEX_0] = { .len = sizeof(ndlType_arrival_rate) },
        [NDL_TM_PACKET_ARRIVAL_RATE_INDEX_1] = { .len = sizeof(ndlType_arrival_rate) },
        [NDL_TM_PACKET_ARRIVAL_RATE_INDEX_2] = { .len = sizeof(ndlType_arrival_rate) },
        [NDL_TM_PACKET_ARRIVAL_RATE_INDEX_3] = { .len = sizeof(ndlType_arrival_rate) },
        [NDL_TM_PACKET_AVG_DURATION_INDEX_0] = { .len = sizeof(ndlType_packet_duration) },
        [NDL_TM_PACKET_AVG_DURATION_INDEX_1] = { .len = sizeof(ndlType_packet_duration) },
        [NDL_TM_PACKET_AVG_DURATION_INDEX_2] = { .len = sizeof(ndlType_packet_duration) },
        [NDL_TM_PACKET_AVG_DURATION_INDEX_3] = { .len = sizeof(ndlType_packet_duration) },
        [NDL_TM_SIGNAL_AVG_POWER_INDEX_0] = { .len = sizeof(ndlType_tx_power) },
        [NDL_TM_SIGNAL_AVG_POWER_INDEX_1] = { .len = sizeof(ndlType_tx_power) },
        [NDL_TM_SIGNAL_AVG_POWER_INDEX_2] = { .len = sizeof(ndlType_tx_power) },
        [NDL_TM_SIGNAL_AVG_POWER_INDEX_3] = { .len = sizeof(ndlType_tx_power) },
        [NDL_MAX_CHANNEL_USE] = { .len = sizeof(ndlType_channel_use) },
        [NDL_TM_CHANNEL_USE_INDEX_0] = { .len = sizeof(ndlType_channel_use) },
        [NDL_TM_CHANNEL_USE_INDEX_1] = { .len = sizeof(ndlType_channel_use) },
        [NDL_TM_CHANNEL_USE_INDEX_2] = { .len = sizeof(ndlType_channel_use) },
        [NDL_TM_CHANNEL_USE_INDEX_3] = { .len = sizeof(ndlType_channel_use) },

        [NDL_MIN_CHANNEL_LOAD] = { .len = sizeof(ndlType_channel_load) },
        [NDL_MAX_CHANNEL_LOAD] = { .len = sizeof(ndlType_channel_load) },

        [NDL_NUM_QUEUE] = { .len = sizeof(ndlType_ac_prio) },
        [NDL_REF_QUEUE_STATUS_INDEX_0] = { .len = sizeof(enum ndlType_queue_status) },
        [NDL_REF_QUEUE_STATUS_INDEX_1] = { .len = sizeof(enum ndlType_queue_status) },
        [NDL_REF_QUEUE_STATUS_INDEX_2] = { .len = sizeof(enum ndlType_queue_status) },
        [NDL_REF_QUEUE_STATUS_INDEX_3] = { .len = sizeof(enum ndlType_queue_status) },
        [NDL_QUEUE_LEN_INDEX_0] = { .len = sizeof(ndlType_number_elements) },
        [NDL_QUEUE_LEN_INDEX_1] = { .len = sizeof(ndlType_number_elements) },
        [NDL_QUEUE_LEN_INDEX_2] = { .len = sizeof(ndlType_number_elements) },
        [NDL_QUEUE_LEN_INDEX_3] = { .len = sizeof(ndlType_number_elements) },

        [NDL_REQUIRED_SNR_INDEX_0] = { .len = sizeof(ndlType_snr) },
        [NDL_REQUIRED_SNR_INDEX_1] = { .len = sizeof(ndlType_snr) },
        [NDL_REQUIRED_SNR_INDEX_2] = { .len = sizeof(ndlType_snr) },
        [NDL_REQUIRED_SNR_INDEX_3] = { .len = sizeof(ndlType_snr) },
        [NDL_REQUIRED_SNR_INDEX_4] = { .len = sizeof(ndlType_snr) },
        [NDL_REQUIRED_SNR_INDEX_5] = { .len = sizeof(ndlType_snr) },
        [NDL_REQUIRED_SNR_INDEX_6] = { .len = sizeof(ndlType_snr) },
        [NDL_REQUIRED_SNR_INDEX_7] = { .len = sizeof(ndlType_snr) },

        [NDL_CARRIER_SENSE_RANGE] = { .len = sizeof(ndlType_distance) },
        [NDL_EST_COMM_RANGE] = { .len = sizeof(ndlType_distance) },
        [NDL_EST_COMM_RANGE_INTF] = { .len = sizeof(ndlType_distance) },

        [NDL_CHANNEL_LOAD] = { .len = sizeof(ndlType_channel_load) },
        [NDL_LOAD_ARRIVAL_RATE] = { .len = sizeof(ndlType_arrival_rate) },
        [NDL_LOAD_AVG_DURATION] = { .len = sizeof(ndlType_packet_duration) },
        [NDL_PACKET_ARRIVAL_RATE] = { .len = sizeof(ndlType_arrival_rate) },
        [NDL_PACKET_AVG_DURATION] = { .len = sizeof(ndlType_packet_duration) },
        [NDL_CHANNEL_BUSY_TIME] = { .len = sizeof(ndlType_channel_load) },

        [NDL_TX_PACKET_ARRIVAL_RATE_INDEX_0] = { .len = sizeof(ndlType_arrival_rate) },
        [NDL_TX_PACKET_ARRIVAL_RATE_INDEX_1] = { .len = sizeof(ndlType_arrival_rate) },
        [NDL_TX_PACKET_ARRIVAL_RATE_INDEX_2] = { .len = sizeof(ndlType_arrival_rate) },
        [NDL_TX_PACKET_ARRIVAL_RATE_INDEX_3] = { .len = sizeof(ndlType_arrival_rate) },
        [NDL_TX_PACKET_AVG_DURATION_INDEX_0] = { .len = sizeof(ndlType_packet_duration) },
        [NDL_TX_PACKET_AVG_DURATION_INDEX_1] = { .len = sizeof(ndlType_packet_duration) },
        [NDL_TX_PACKET_AVG_DURATION_INDEX_2] = { .len = sizeof(ndlType_packet_duration) },
        [NDL_TX_PACKET_AVG_DURATION_INDEX_3] = { .len = sizeof(ndlType_packet_duration) },
        [NDL_TX_CHANNEL_USE_INDEX_0] = { .len = sizeof(ndlType_channel_use) },
        [NDL_TX_CHANNEL_USE_INDEX_1] = { .len = sizeof(ndlType_channel_use) },
        [NDL_TX_CHANNEL_USE_INDEX_2] = { .len = sizeof(ndlType_channel_use) },
        [NDL_TX_CHANNEL_USE_INDEX_3] = { .len = sizeof(ndlType_channel_use) },
        [NDL_TX_SIGNAL_AVG_POWER_INDEX_0] = { .len = sizeof(ndlType_tx_power) },
        [NDL_TX_SIGNAL_AVG_POWER_INDEX_1] = { .len = sizeof(ndlType_tx_power) },
        [NDL_TX_SIGNAL_AVG_POWER_INDEX_2] = { .len = sizeof(ndlType_tx_power) },
        [NDL_TX_SIGNAL_AVG_POWER_INDEX_3] = { .len = sizeof(ndlType_tx_power) },

        [NDL_TIME_UP] = { .len = sizeof(ndlType_timing) },
        [NDL_TIME_DOWN] = { .len = sizeof(ndlType_timing) },
        [NDL_NUM_ACTIVE_STATE] = { .len = sizeof(ndlType_number_elements) },
        [NDL_CUR_ACRIVE_STATE] = { .len = sizeof(ndlType_number_elements) },

        [NDL_AS_STATE_ID] = { .len = sizeof(ndlType_number_elements) },
        [NDL_AS_TIME_DN] = { .len = sizeof(ndlType_timing) },
        [NDL_AS_CHAN_LOAD] = { .len = sizeof(ndlType_channel_load) },
        [NDL_AS_DCC_INDEX_0] = { .len = sizeof(enum ndlType_dcc_mechanism) },
        [NDL_AS_DCC_INDEX_1] = { .len = sizeof(enum ndlType_dcc_mechanism) },
        [NDL_AS_DCC_INDEX_2] = { .len = sizeof(enum ndlType_dcc_mechanism) },
        [NDL_AS_DCC_INDEX_3] = { .len = sizeof(enum ndlType_dcc_mechanism) },
        [NDL_AS_TX_POWER_INDEX_0] = { .len = sizeof(ndlType_tx_power) },
        [NDL_AS_TX_POWER_INDEX_1] = { .len = sizeof(ndlType_tx_power) },
        [NDL_AS_TX_POWER_INDEX_2] = { .len = sizeof(ndlType_tx_power) },
        [NDL_AS_TX_POWER_INDEX_3] = { .len = sizeof(ndlType_tx_power) },
        [NDL_AS_PACKET_INTERVAL_INDEX_0] = { .len = sizeof(ndlType_packet_interval) },
        [NDL_AS_PACKET_INTERVAL_INDEX_1] = { .len = sizeof(ndlType_packet_interval) },
        [NDL_AS_PACKET_INTERVAL_INDEX_2] = { .len = sizeof(ndlType_packet_interval) },
        [NDL_AS_PACKET_INTERVAL_INDEX_3] = { .len = sizeof(ndlType_packet_interval) },
        [NDL_AS_DATARATE_INDEX_0] = { .len = sizeof(ndlType_datarate) },
        [NDL_AS_DATARATE_INDEX_1] = { .len = sizeof(ndlType_datarate) },
        [NDL_AS_DATARATE_INDEX_2] = { .len = sizeof(ndlType_datarate) },
        [NDL_AS_DATARATE_INDEX_3] = { .len = sizeof(ndlType_datarate) },
        [NDL_AS_CARRIER_SENSE_INDEX_0] = { .len = sizeof(ndlType_rx_power) },
        [NDL_AS_CARRIER_SENSE_INDEX_1] = { .len = sizeof(ndlType_rx_power) },
        [NDL_AS_CARRIER_SENSE_INDEX_2] = { .len = sizeof(ndlType_rx_power) },
        [NDL_AS_CARRIER_SENSE_INDEX_3] = { .len = sizeof(ndlType_rx_power) },
};

static const struct genl_ops ndl_nlm_ops [] = {
        {
                .cmd        = NDL_CMD_SET_DATABASE_ATTRIBUTES,
                .doit       = ndl_save_received_attributes,
                .policy     = ndl_attr_msg_policy
        },
        {
                .cmd        = NDL_CMD_GET_DATABASE_ATTRIBUTES,
                .doit       = ndl_send_measured_attributes,
        },
        {
                .cmd        = NDL_CMD_SEND_RX_STATS,
                .doit       = ndl_save_rx_stats,
        },
};


static int init_netlink_kernel()
{
        int _status = 0;
        _status = genl_register_family_with_ops(&ndl_nlm_family,ndl_nlm_ops);

        if (_status) {
                print_error("Error during netlink init. Code: %d", _status);
        }
        return _status;
}

static int destroy_netlink_kernel()
{
        int _status = 0;
        _status |= genl_unregister_family(&ndl_nlm_family);
        return _status;
}

static int ndl_save_received_attributes(struct sk_buff *_skb, struct genl_info *_info)
{
        int _status = 0;
        struct nlattr *attributes;
        int temp = 0;
        int i = 0;
        struct ndlType_ndl_database *database_ptr = global_ndl_database;

        print_debug("Handler set attributes entered");


	spin_lock(&(global_ndl_database->database_in_use));
// LOCK MEMORY FOR NDL DATABASE
        attributes = _info->attrs[NDL_MIN_TX_POWER];
        if (attributes) {
		temp = nla_get_s32(attributes);
		database_ptr->NDL_transmit_power_tresholds->NDL_min_tx_power = encode_to_NDL_tx_power(temp);
        } else {
		print_warning("No attribute %d.", NDL_MIN_TX_POWER);
        }

        attributes = _info->attrs[NDL_MAX_TX_POWER];
        if (attributes) {
		temp = nla_get_s32(attributes);
		database_ptr->NDL_transmit_power_tresholds->NDL_max_tx_power = encode_to_NDL_tx_power(temp);
        } else {
		print_warning("No attribute %d.", NDL_MAX_TX_POWER);
	}

	for (i=0 ; i<4 ; i++) {
		attributes = _info->attrs[NDL_DEF_TX_POWER_INDEX_0+i];
		if (attributes) {
			temp = nla_get_s32(attributes);
			database_ptr->NDL_transmit_power_tresholds->NDL_def_tx_power[i]= encode_to_NDL_tx_power(temp);
		} else {
			print_warning("No attribute %d.", NDL_DEF_TX_POWER_INDEX_0+i);
		}
	}

	for (i=0 ; i<4 ; i++) {
		attributes = _info->attrs[NDL_MAX_PACKET_DURATION_INDEX_0+i];
		if (attributes) {
			temp = nla_get_u32(attributes);
			database_ptr->NDL_packet_timing_tresholds->NDL_max_packet_duration[i]= encode_to_NDL_packet_duration(temp);
		} else {
			print_warning("No attribute %d.", NDL_MAX_PACKET_DURATION_INDEX_0+i);
		}
	}

	attributes = _info->attrs[NDL_MIN_PACKET_INTERVAL];
        if (attributes) {
		temp = nla_get_u32(attributes);
		database_ptr->NDL_packet_timing_tresholds->NDL_min_packet_interval= encode_to_NDL_packet_interval(temp);
        } else {
		print_warning("No attribute %d.", NDL_MIN_PACKET_INTERVAL);
	}

	attributes = _info->attrs[NDL_MAX_PACKET_INTERVAL];
        if (attributes) {
		temp = nla_get_u32(attributes);
		database_ptr->NDL_packet_timing_tresholds->NDL_max_packet_interval= encode_to_NDL_packet_interval(temp);
        } else {
		print_warning("No attribute %d.", NDL_MAX_PACKET_INTERVAL);
	}

	for (i=0 ; i<4 ; i++) {
		attributes = _info->attrs[NDL_DEF_PACKET_INTERVAL_INDEX_0+i];
		if (attributes) {
			temp = nla_get_u32(attributes);
			database_ptr->NDL_packet_timing_tresholds->NDL_def_packet_interval[i] = encode_to_NDL_packet_interval(temp);
		} else {
			print_warning("No attribute %d.", NDL_DEF_PACKET_INTERVAL_INDEX_0+i);
		}
	}

	attributes = _info->attrs[NDL_MIN_DATARATE];
        if (attributes) {
		temp = nla_get_u16(attributes);
		database_ptr->NDL_packet_datarate_tresholds->NDL_min_datarate = temp;
        } else {
		print_warning("No attribute %d.", NDL_MIN_DATARATE);
	}

	attributes = _info->attrs[NDL_MAX_DATARATE];
        if (attributes) {
		temp = nla_get_u16(attributes);
		database_ptr->NDL_packet_datarate_tresholds->NDL_max_datarate = temp;
        } else {
		print_warning("No attribute %d.", NDL_MAX_DATARATE);
	}

	for (i=0 ; i<4 ; i++) {
		attributes = _info->attrs[NDL_DEF_DATARATE_INDEX_0+i];
		if (attributes) {
			temp = nla_get_u16(attributes);
			database_ptr->NDL_packet_datarate_tresholds->NDL_def_datarate[i] = temp;
		} else {
			print_warning("No attribute %d.", NDL_DEF_DATARATE_INDEX_0+i);
		}
	}

	attributes = _info->attrs[NDL_MIN_CARRIER_SENSE];
        if (attributes) {
		temp = nla_get_s32(attributes);
		database_ptr->NDL_receive_signal_tresholds->NDL_min_carrier_sense = encode_to_NDL_rx_power(temp);
        } else {
		print_warning("No attribute %d.", NDL_MIN_CARRIER_SENSE);
	}

	attributes = _info->attrs[NDL_MAX_CARRIER_SENSE];
        if (attributes) {
		temp = nla_get_s32(attributes);
		database_ptr->NDL_receive_signal_tresholds->NDL_max_carrier_sense = encode_to_NDL_rx_power(temp);
        } else {
		print_warning("No attribute %d.", NDL_MAX_CARRIER_SENSE);
	}
	attributes = _info->attrs[NDL_DEF_CARRIER_SENSE];
        if (attributes) {
		temp = nla_get_s32(attributes);
		database_ptr->NDL_receive_signal_tresholds->NDL_def_carrier_sense = encode_to_NDL_rx_power(temp);
        } else {
		print_warning("No attribute %d.", NDL_DEF_CARRIER_SENSE);
	}

	attributes = _info->attrs[NDL_DEF_DCC_SENSITIVITY];
        if (attributes) {
		temp = nla_get_s32(attributes);
		database_ptr->NDL_receive_model_parameters->NDL_def_dcc_sensitivity = encode_to_NDL_rx_power(temp);
        } else {
		print_warning("No attribute %d.", NDL_DEF_DCC_SENSITIVITY);
	}

	attributes = _info->attrs[NDL_MAX_CS_RANGE];
        if (attributes) {
		temp = nla_get_u32(attributes);
		database_ptr->NDL_receive_model_parameters->NDL_max_cs_range = temp;
        } else {
		print_warning("No attribute %d.", NDL_MAX_CS_RANGE);
	}

	attributes = _info->attrs[NDL_REF_PATHLOSS];
        if (attributes) {
		temp = nla_get_u32(attributes);
		database_ptr->NDL_receive_model_parameters->NDL_ref_pathloss = encode_to_NDL_pathloss(temp);
        } else {
		print_warning("No attribute %d.", NDL_REF_PATHLOSS);
	}

	attributes = _info->attrs[NDL_MIN_SNR];
        if (attributes) {
		temp = nla_get_u32(attributes);
		database_ptr->NDL_receive_model_parameters->NDL_min_snr = encode_to_NDL_snr(temp);
        } else {
		print_warning("No attribute %d.", NDL_MIN_SNR);
	}

	for (i=0 ; i<8 ; i++) {
		attributes = _info->attrs[NDL_SNR_BACKOFF_INDEX_0+i];
		if (attributes) {
			temp = nla_get_s32(attributes);
			database_ptr->NDL_demodulation_model_parameters->NDL_snr_backoff[i] = encode_to_NDL_snr(temp);
		} else {
			print_warning("No attribute %d.", NDL_SNR_BACKOFF_INDEX_0+i);
		}
	}

	attributes = _info->attrs[NDL_MIN_CHANNEL_LOAD];
        if (attributes) {
		temp = nla_get_u32(attributes);
		database_ptr->NDL_channel_load_tresholds->NDL_min_channel_load = encode_to_NDL_channel_load(temp);
        } else {
		print_warning("No attribute %d.", NDL_MIN_CHANNEL_LOAD);
	}
	attributes = _info->attrs[NDL_MAX_CHANNEL_LOAD];
        if (attributes) {
		temp = nla_get_u32(attributes);
		database_ptr->NDL_channel_load_tresholds->NDL_max_channel_load = encode_to_NDL_channel_load(temp);
        } else {
		print_warning("No attribute %d.", NDL_MAX_CHANNEL_LOAD);
	}

	attributes = _info->attrs[NDL_NUM_QUEUE];
        if (attributes) {
		temp = nla_get_u16(attributes);
		database_ptr->NDL_transmit_queue_parameters->NDL_num_queue = temp;
        } else {
		print_warning("No attribute %d.", NDL_NUM_QUEUE);
	}

	for (i=0 ; i<4 ; i++) {
		attributes = _info->attrs[NDL_QUEUE_LEN_INDEX_0+i];
		if (attributes) {
			temp = nla_get_u16(attributes);
			database_ptr->NDL_transmit_queue_parameters->NDL_queue_len[i] = temp;
		} else {
			print_warning("No attribute %d.", NDL_QUEUE_LEN_INDEX_0+i);
		}
	}

	spin_unlock(&(global_ndl_database->database_in_use));
        global_ndl_database = database_ptr;
        return _status;
}

static int ndl_send_measured_attributes(struct sk_buff *_skb, struct genl_info *_info)
{
	int _status = 0;
	struct sk_buff *skb;
	struct genlmsghdr *genlhdr;
	struct ndlType_ndl_database *database_ptr = global_ndl_database;
	void *reply;
	int temp = 0;
	int i = 0;
	print_debug("Handler of sending measured attributes entered");

	skb = genlmsg_new(NLMSG_GOODSIZE, GFP_ATOMIC);
	check(skb, "Error during socket init!");
	if (!skb)
		return -1;

	genlmsg_put_reply(skb,_info, &ndl_nlm_family,0, NDL_CMD_GET_DATABASE_ATTRIBUTES);
	nla_reserve(skb,0,0);

	spin_lock(&(global_ndl_database->database_in_use));
// LOCK MEMORY FOR NDL DATABASE

	for (i=0 ; i<4 ; i++) {
		temp = decode_from_NDL_tx_power(database_ptr->NDL_transmit_power_tresholds->NDL_ref_tx_power[i]);
		nla_put_s32(skb,NDL_REF_TX_POWER_INDEX_0+i, temp);
	}


	for (i=0 ; i<4 ; i++) {
		temp = decode_from_NDL_packet_interval(database_ptr->NDL_packet_timing_tresholds->NDL_ref_packet_interval[i]);
		nla_put_u32(skb,NDL_REF_PACKET_INTERVAL_INDEX_0+i, temp);
	}

	for (i=0 ; i<4 ; i++) {
		temp = database_ptr->NDL_packet_datarate_tresholds->NDL_ref_datarate[i];
		nla_put_u16(skb,NDL_REF_DATARATE_INDEX_0+i, temp);
	}

	temp = decode_from_NDL_rx_power(database_ptr->NDL_receive_signal_tresholds->NDL_ref_carrier_sense);
	nla_put_s32(skb,NDL_REF_CARRIER_SENSE, temp);

	for (i=0 ; i<4 ; i++) {
		temp = decode_from_NDL_arrival_rate(database_ptr->NDL_transmit_model_parameters->NDL_tm_packet_arrival_rate[i]);
		nla_put_u32(skb,NDL_TM_PACKET_ARRIVAL_RATE_INDEX_0+i, temp);
	}

	for (i=0 ; i<4 ; i++) {
		temp = decode_from_NDL_packet_duration(database_ptr->NDL_transmit_model_parameters->NDL_tm_packet_avg_duration[i]);
		nla_put_u32(skb,NDL_TM_PACKET_AVG_DURATION_INDEX_0+i, temp);
	}

	for (i=0 ; i<4 ; i++) {
		temp = decode_from_NDL_tx_power(database_ptr->NDL_transmit_model_parameters->NDL_tm_signal_avg_power[i]);
		nla_put_s32(skb,NDL_TM_SIGNAL_AVG_POWER_INDEX_0+i, temp);
	}

	temp = decode_from_NDL_channel_use(database_ptr->NDL_transmit_model_parameters->NDL_max_channel_use);
	nla_put_u32(skb,NDL_MAX_CHANNEL_USE, temp);

	for (i=0 ; i<4 ; i++) {
		temp = decode_from_NDL_channel_use(database_ptr->NDL_transmit_model_parameters->NDL_tm_channel_use[i]);
		nla_put_u32(skb,NDL_TM_CHANNEL_USE_INDEX_0+i, temp);
	}

	for (i=0 ; i<4 ; i++) {
		temp = (int)database_ptr->NDL_transmit_queue_parameters->NDL_ref_queue_status[i];
		nla_put_u16(skb,NDL_REF_QUEUE_STATUS_INDEX_0+i, temp);
	}

	for (i=0 ; i<8 ; i++) {
		temp = decode_from_NDL_snr(database_ptr->NDL_demodulation_model_parameters->NDL_required_snr[i]);
		nla_put_s32(skb,NDL_REQUIRED_SNR_INDEX_0+i, temp);
	}

	temp = database_ptr->NDL_communication_ranges->NDL_carrier_sense_range;
	nla_put_u32(skb,NDL_CARRIER_SENSE_RANGE, temp);

	temp = database_ptr->NDL_communication_ranges->NDL_est_comm_range;
	nla_put_u32(skb,NDL_EST_COMM_RANGE, temp);

	temp = database_ptr->NDL_communication_ranges->NDL_est_comm_range_intf;
	nla_put_u32(skb,NDL_EST_COMM_RANGE_INTF, temp);

	temp = decode_from_NDL_channel_load(database_ptr->NDL_channel_load_measures->NDL_channel_load);
	nla_put_u32(skb,NDL_CHANNEL_LOAD, temp);

	temp = decode_from_NDL_arrival_rate(database_ptr->NDL_channel_load_measures->NDL_load_arrival_rate);
	nla_put_u32(skb,NDL_LOAD_ARRIVAL_RATE, temp);

	temp = decode_from_NDL_packet_duration(database_ptr->NDL_channel_load_measures->NDL_load_avg_duration);
	nla_put_u32(skb,NDL_LOAD_AVG_DURATION, temp);

	temp = decode_from_NDL_arrival_rate(database_ptr->NDL_channel_load_measures->NDL_packet_arrival_rate);
	nla_put_u32(skb,NDL_PACKET_ARRIVAL_RATE, temp);

	temp = decode_from_NDL_packet_duration(database_ptr->NDL_channel_load_measures->NDL_packet_avg_duration);
	nla_put_u32(skb,NDL_PACKET_AVG_DURATION, temp);

	temp = decode_from_NDL_channel_load(database_ptr->NDL_channel_load_measures->NDL_channel_busy_time);
	nla_put_u32(skb,NDL_CHANNEL_BUSY_TIME, temp);

	for (i=0 ; i<4 ; i++) {
		temp = decode_from_NDL_arrival_rate(database_ptr->NDL_transmit_packet_statistics->NDL_tx_packet_arrival_rate[i]);
		nla_put_u32(skb,NDL_TX_PACKET_ARRIVAL_RATE_INDEX_0+i, temp);
	}

	for (i=0 ; i<4 ; i++) {
		temp = decode_from_NDL_packet_duration(database_ptr->NDL_transmit_packet_statistics->NDL_tx_packet_avg_duration[i]);
		nla_put_u32(skb,NDL_TX_PACKET_AVG_DURATION_INDEX_0+i, temp);
	}

	for (i=0 ; i<4 ; i++) {
		temp = decode_from_NDL_channel_use(database_ptr->NDL_transmit_packet_statistics->NDL_tx_channel_use[i]);
		nla_put_u32(skb,NDL_TX_CHANNEL_USE_INDEX_0+i, temp);
	}

	for (i=0 ; i<4 ; i++) {
		temp = decode_from_NDL_tx_power(database_ptr->NDL_transmit_packet_statistics->NDL_tx_signal_avg_power[i]);
		nla_put_s32(skb,NDL_TX_SIGNAL_AVG_POWER_INDEX_0+i, temp);
	}

	spin_unlock(&(global_ndl_database->database_in_use));
// UNLOCK MEMORY FOR NDL DATABASE

	genlhdr = nlmsg_data(nlmsg_hdr(skb));
	reply = genlmsg_data(genlhdr);
	genlmsg_end(skb, reply);
	genlmsg_reply(skb,_info);


	global_ndl_database = database_ptr;
	return _status;
}


static int ndl_save_rx_stats(struct sk_buff *_skb, struct genl_info *_info)
{
        int _status = 0;
        struct nlattr *attributes;
        int temp = 0;
	struct ndlType_channel_load_measures *channel_load_measures = kmalloc( sizeof(struct ndlType_channel_load_measures),GFP_ATOMIC);

        channel_load_measures->msg_type = msg_NDL_CHANNEL_LOAD_MEASURES;

	attributes = _info->attrs[NDL_STATS_CHANNEL_LOAD];
        if (attributes) {
		temp = nla_get_u32(attributes);
		channel_load_measures->NDL_channel_load = temp;
        } else {
		print_warning("No attribute %d.", NDL_STATS_CHANNEL_LOAD);
	}

	attributes = _info->attrs[NDL_STATS_LOAD_ARR_RATE];
        if (attributes) {
		temp = nla_get_u32(attributes);
		channel_load_measures->NDL_load_arrival_rate = temp;
        } else {
		print_warning("No attribute %d.", NDL_STATS_LOAD_ARR_RATE);
	}

	attributes = _info->attrs[NDL_STATS_LOAD_AVG_DURATION];
        if (attributes) {
		temp = nla_get_u32(attributes);
		channel_load_measures->NDL_load_avg_duration = temp;
        } else {
		print_warning("No attribute %d.", NDL_STATS_LOAD_AVG_DURATION);
	}

	attributes = _info->attrs[NDL_STATS_PKT_ARR_RATE];
        if (attributes) {
		temp = nla_get_u32(attributes);
		channel_load_measures->NDL_packet_arrival_rate = temp;
        } else {
		print_warning("No attribute %d.", NDL_STATS_PKT_ARR_RATE);
	}

	attributes = _info->attrs[NDL_STATS_PKT_AVG_DURATION];
        if (attributes) {
		temp = nla_get_u32(attributes);
		channel_load_measures->NDL_packet_avg_duration = temp;
        } else {
		print_warning("No attribute %d.", NDL_STATS_PKT_AVG_DURATION);
	}

	attributes = _info->attrs[NDL_STATS_CHANNEL_BUSY];
        if (attributes) {
		temp = nla_get_u32(attributes);
		channel_load_measures->NDL_channel_busy_time = temp;
        } else {
		print_warning("No attribute %d.", NDL_STATS_CHANNEL_BUSY);
	}


	kfifo_in(&dcc_miq_data, &channel_load_measures, sizeof(struct ndlType_channel_load_measures *));

	return _status;
}
