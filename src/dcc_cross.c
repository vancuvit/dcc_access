
#define DCC_PREFIX	"DCC CROSS"
//#define DCC_DEBUG

#include "dcc_cross.h"
#include "dcc_err.h"

int ndl_datarate_table [8] = {30, 45, 60, 90, 120, 280, 240, 270};
int ndl_SNR_table [8] = {0, 10, 30, 50, 80, 120, 160, 170};


int init_DP_table (struct dpType_dp_table ** _state)
{

        struct dpType_dp_table * state;
	int i;
        state = kmalloc(sizeof(struct dpType_dp_table), GFP_KERNEL);

        state->msg_type = msg_DP_TABLE;
#ifdef __KERNEL__
	spin_lock_init(&(state->table_in_use));
#else
        pthread_mutex_init(&(state->table_in_use), NULL);
#endif // __KERNEL__

        state->active = kmalloc(32 * sizeof(struct dpType_dcc_profile *), GFP_KERNEL);
	state->relaxed = kmalloc(32 * sizeof(struct dpType_dcc_profile *), GFP_KERNEL);
	state->restrictive = kmalloc(32 * sizeof(struct dpType_dcc_profile *), GFP_KERNEL);

	for (i=0; i<32; i++)
	{
		state->active[i] = kmalloc(sizeof(struct dpType_dcc_profile), GFP_KERNEL);
		state->relaxed[i] = kmalloc(sizeof(struct dpType_dcc_profile), GFP_KERNEL);
		state->restrictive[i] = kmalloc(sizeof(struct dpType_dcc_profile), GFP_KERNEL);

		state->active[i]->id = i;
		state->active[i]->dp_not_available = 1;
		state->relaxed[i]->id = i;
		state->relaxed[i]->dp_not_available = 1;
		state->restrictive[i]->id = i;
		state->restrictive[i]->dp_not_available = 1;
	}

	*_state = state;
	return 0;
}

int destroy_DP_table (struct dpType_dp_table ** _state)
{

        struct dpType_dp_table * state = *_state;
	int i;

	for (i=0; i<32; i++)
	{
		kfree(state->active[i]);
		kfree(state->relaxed[i]);
		kfree(state->restrictive[i]);
	}

        kfree(state->active);
	kfree(state->relaxed);
	kfree(state->restrictive);

#ifndef __KERNEL__
        pthread_mutex_destroy(&(state->table_in_use));
#endif // __KERNEL__

	kfree(state);

	*_state = state;
	return 0;
}


int set_default_values_DP_cch (struct dpType_dp_table ** _state)
{

        struct dpType_dp_table * state = *_state;

        state->relaxed[0]->num_queueu = 1;
        state->relaxed[0]->t_off = 50;
	state->active[0]->num_queueu = 1;
        state->active[0]->t_off = 50;
        state->restrictive[0]->num_queueu = 1;
        state->restrictive[0]->t_off = 50;
        state->relaxed[1]->num_queueu = 1;
        state->relaxed[1]->t_off = 95;
	state->active[1]->num_queueu = 1;
        state->active[1]->t_off = 190;
        state->restrictive[1]->num_queueu = 1;
        state->restrictive[1]->t_off = 250;
	state->relaxed[2]->num_queueu = 2;
        state->relaxed[2]->t_off = 95;
	state->active[2]->num_queueu = 2;
        state->active[2]->t_off = 190;
        state->restrictive[2]->num_queueu = 2;
        state->restrictive[2]->t_off = 250;
	state->relaxed[3]->num_queueu = 3;
        state->relaxed[3]->t_off = 250;
	state->active[3]->num_queueu = 3;
        state->active[3]->t_off = 500;
        state->restrictive[3]->num_queueu = 3;
        state->restrictive[3]->t_off = 1000;
	state->relaxed[4]->num_queueu = 4;
        state->relaxed[4]->t_off = 500;
	state->relaxed[5]->num_queueu = 4;
        state->relaxed[5]->t_off = 1000;
	state->relaxed[6]->num_queueu = 4;
        state->relaxed[6]->t_off = 5000;
	state->relaxed[7]->num_queueu = 4;
        state->relaxed[7]->t_off = 10000;
	state->relaxed[8]->num_queueu = 4;
        state->relaxed[8]->t_off = 10000;

	*_state = state;
	return 0;
}

int set_default_values_DP_sch (struct dpType_dp_table ** _state)
{

        struct dpType_dp_table * state = *_state;
	int i = 2;

        for (; i < 18; i++) {
		state->relaxed[i]->dp_not_available = 0;
	}

	for (i = 9; i < 11; i++) {
		state->active[i]->dp_not_available = 0;
		state->restrictive[i]->dp_not_available = 0;
	}

	state->relaxed[2]->num_queueu = 2;
        state->relaxed[2]->t_off = 95;
	state->relaxed[3]->num_queueu = 3;
        state->relaxed[3]->t_off = 95;
	state->active[3]->num_queueu = 3;
        state->active[3]->t_off = 500;
        state->relaxed[4]->num_queueu = 3;
        state->relaxed[4]->t_off = 500;
	state->active[4]->num_queueu = 3;
        state->active[4]->t_off = 500;
	state->relaxed[5]->num_queueu = 4;
        state->relaxed[5]->t_off = 500;
	state->relaxed[6]->num_queueu = 4;
        state->relaxed[6]->t_off = 500;
	state->relaxed[7]->num_queueu = 4;
        state->relaxed[7]->t_off = 1000;
	state->relaxed[8]->num_queueu = 4;
        state->relaxed[8]->t_off = 1000;
	state->relaxed[9]->num_queueu = 1;
	state->relaxed[9]->t_off = 100;
	state->active[9]->num_queueu = 1;
	state->active[9]->t_off = 200;
	state->restrictive[9]->num_queueu = 1;
	state->restrictive[9]->t_off = 250;
	state->relaxed[10]->num_queueu = 2;
	state->relaxed[10]->t_off = 100;
	state->active[10]->num_queueu = 2;
	state->active[10]->t_off = 200;
	state->restrictive[10]->num_queueu = 2;
	state->restrictive[10]->t_off = 250;
	state->relaxed[11]->num_queueu = 3;
	state->relaxed[11]->t_off = 100;
	state->active[11]->dp_not_available = 0;
	state->active[11]->num_queueu = 3;
	state->active[11]->t_off = 500;
	state->relaxed[12]->num_queueu = 4;
	state->relaxed[12]->t_off = 500;
	state->relaxed[13]->num_queueu = 4;
	state->relaxed[13]->t_off = 1000;
	state->relaxed[14]->num_queueu = 4;
	state->relaxed[14]->t_off = 5000;
	state->relaxed[15]->num_queueu = 4;
	state->relaxed[15]->t_off = 10000;
	state->relaxed[16]->num_queueu = 4;
	state->relaxed[17]->t_off = 10000;

	*_state = state;
	return 0;
}

        /** Correct way of commenting
        *  intiMemoryNDL - description of correct comment for this purpose
        *  @**_database:   pointer to database structure
        *
        *  longer description of this function
        *
        *  Return: error code
        */
int init_NDL_memory(struct ndlType_ndl_database **_database)
{
        int _status = 0;
        struct ndlType_ndl_database *database_ptr;
        int i;

        print_debug("Place of memory allocation.");

        database_ptr = kmalloc(sizeof(struct ndlType_ndl_database), GFP_KERNEL);

#ifdef __KERNEL__
	spin_lock_init(&(database_ptr->database_in_use));
#else
        pthread_mutex_init(&(database_ptr->database_in_use), NULL);
#endif // __KERNEL__

	database_ptr->NDL_transmit_power_tresholds = kmalloc(sizeof(struct ndlType_transmit_power_tresholds), GFP_KERNEL);

        database_ptr->NDL_transmit_power_tresholds->NDL_def_tx_power = kmalloc(NL80211_NUM_ACS * sizeof(ndlType_tx_power) ,GFP_KERNEL);
        database_ptr->NDL_transmit_power_tresholds->NDL_ref_tx_power = kmalloc(NL80211_NUM_ACS * sizeof(ndlType_tx_power) ,GFP_KERNEL);

        database_ptr->NDL_packet_timing_tresholds = kmalloc(sizeof(struct ndlType_packet_timing_tresholds), GFP_KERNEL);
        database_ptr->NDL_packet_timing_tresholds->NDL_max_packet_duration = kmalloc(NL80211_NUM_ACS * sizeof(ndlType_packet_duration), GFP_KERNEL);
        database_ptr->NDL_packet_timing_tresholds->NDL_def_packet_interval = kmalloc(NL80211_NUM_ACS * sizeof(ndlType_packet_interval), GFP_KERNEL);
        database_ptr->NDL_packet_timing_tresholds->NDL_ref_packet_interval = kmalloc(NL80211_NUM_ACS * sizeof(ndlType_packet_interval), GFP_KERNEL);

        database_ptr->NDL_packet_datarate_tresholds = kmalloc(sizeof(struct ndlType_packet_datarate_tresholds), GFP_KERNEL);
        database_ptr->NDL_packet_datarate_tresholds->NDL_def_datarate = kmalloc(NL80211_NUM_ACS * sizeof(ndlType_datarate), GFP_KERNEL);
        database_ptr->NDL_packet_datarate_tresholds->NDL_ref_datarate = kmalloc(NL80211_NUM_ACS * sizeof(ndlType_datarate), GFP_KERNEL);

        database_ptr->NDL_receive_signal_tresholds = kmalloc(sizeof(struct ndlType_receive_signal_tresholds), GFP_KERNEL);

        database_ptr->NDL_receive_model_parameters = kmalloc(sizeof(struct ndlType_receive_model_parameters), GFP_KERNEL);

        database_ptr->NDL_demodulation_model_parameters = kmalloc(sizeof(struct ndlType_demodulation_model_parameters), GFP_KERNEL);
        database_ptr->NDL_demodulation_model_parameters->NDL_snr_backoff = kmalloc(8 * sizeof(ndlType_snr), GFP_KERNEL);
        database_ptr->NDL_demodulation_model_parameters->NDL_required_snr = kmalloc(8 * sizeof(ndlType_snr), GFP_KERNEL);

        database_ptr->NDL_transmit_model_parameters = kmalloc(sizeof(struct ndlType_transmit_model_parameters), GFP_KERNEL);
        database_ptr->NDL_transmit_model_parameters->NDL_tm_channel_use = kmalloc(NL80211_NUM_ACS * sizeof(ndlType_channel_use), GFP_KERNEL);
        database_ptr->NDL_transmit_model_parameters->NDL_tm_packet_arrival_rate = kmalloc(NL80211_NUM_ACS * sizeof(ndlType_arrival_rate), GFP_KERNEL);
        database_ptr->NDL_transmit_model_parameters->NDL_tm_packet_avg_duration = kmalloc(NL80211_NUM_ACS * sizeof(ndlType_packet_duration), GFP_KERNEL);
        database_ptr->NDL_transmit_model_parameters->NDL_tm_signal_avg_power = kmalloc(NL80211_NUM_ACS * sizeof(ndlType_tx_power), GFP_KERNEL);

        database_ptr->NDL_channel_load_tresholds = kmalloc(sizeof(struct ndlType_channel_load_tresholds), GFP_KERNEL);

        database_ptr->NDL_transmit_queue_parameters = kmalloc(sizeof(struct ndlType_transmit_queue_parameters), GFP_KERNEL);
        database_ptr->NDL_transmit_queue_parameters->NDL_queue_len = kmalloc(NL80211_NUM_ACS * sizeof(ndlType_number_elements), GFP_KERNEL);
        database_ptr->NDL_transmit_queue_parameters->NDL_ref_queue_status = kmalloc(NL80211_NUM_ACS * sizeof(enum ndlType_queue_status), GFP_KERNEL);

        database_ptr->NDL_communication_ranges = kmalloc(sizeof(struct ndlType_communication_ranges), GFP_KERNEL);

        database_ptr->NDL_channel_load_measures = kmalloc(sizeof(struct ndlType_channel_load_measures), GFP_KERNEL);

        database_ptr->NDL_transmit_packet_statistics = kmalloc(sizeof(struct ndlType_transmit_packet_statictics), GFP_KERNEL);
        database_ptr->NDL_transmit_packet_statistics->NDL_tx_packet_arrival_rate = kmalloc(NL80211_NUM_ACS * sizeof(ndlType_arrival_rate), GFP_KERNEL);
        database_ptr->NDL_transmit_packet_statistics->NDL_tx_packet_avg_duration = kmalloc(NL80211_NUM_ACS * sizeof(ndlType_packet_duration), GFP_KERNEL);
        database_ptr->NDL_transmit_packet_statistics->NDL_tx_channel_use = kmalloc(NL80211_NUM_ACS * sizeof(ndlType_channel_use), GFP_KERNEL);
        database_ptr->NDL_transmit_packet_statistics->NDL_tx_signal_avg_power = kmalloc(NL80211_NUM_ACS * sizeof(ndlType_tx_power), GFP_KERNEL);

        database_ptr->NDL_general_configuration = kmalloc(sizeof(struct ndlType_general_configuration), GFP_KERNEL);

        database_ptr->NDL_state_list = kmalloc(6 * sizeof(struct ndlType_state_configuration *), GFP_KERNEL);


        for (i=0;i<6;i++) {
                database_ptr->NDL_state_list[i] = kmalloc(sizeof(struct ndlType_state_configuration), GFP_KERNEL);

                database_ptr->NDL_state_list[i]->NDL_as_dcc = kmalloc(NL80211_NUM_ACS * sizeof(enum ndlType_dcc_mechanism), GFP_KERNEL);
                database_ptr->NDL_state_list[i]->NDL_as_tx_power = kmalloc(NL80211_NUM_ACS * sizeof(ndlType_tx_power), GFP_KERNEL);
                database_ptr->NDL_state_list[i]->NDL_as_packet_interval = kmalloc(NL80211_NUM_ACS * sizeof(ndlType_packet_interval), GFP_KERNEL);
                database_ptr->NDL_state_list[i]->NDL_as_datarate = kmalloc(NL80211_NUM_ACS * sizeof(ndlType_datarate), GFP_KERNEL);
                database_ptr->NDL_state_list[i]->NDL_as_carrier_sense = kmalloc(NL80211_NUM_ACS * sizeof(ndlType_rx_power), GFP_KERNEL);
        }

    //error checks
        *_database = database_ptr;
        return _status;
}

int destroy_NDL_memory(struct ndlType_ndl_database **_database)
{
        int _status = 0;
        struct ndlType_ndl_database *database_ptr = *_database;
        int i;
        for (i=0;i<6;i++) {
                kfree(database_ptr->NDL_state_list[i]->NDL_as_dcc);
                kfree(database_ptr->NDL_state_list[i]->NDL_as_tx_power);
                kfree(database_ptr->NDL_state_list[i]->NDL_as_packet_interval);
                kfree(database_ptr->NDL_state_list[i]->NDL_as_datarate);
                kfree(database_ptr->NDL_state_list[i]->NDL_as_carrier_sense);

                kfree(database_ptr->NDL_state_list[i]);
        }

        kfree(database_ptr->NDL_state_list);

        kfree(database_ptr->NDL_general_configuration);

        kfree(database_ptr->NDL_transmit_packet_statistics->NDL_tx_packet_arrival_rate);
        kfree(database_ptr->NDL_transmit_packet_statistics->NDL_tx_packet_avg_duration);
        kfree(database_ptr->NDL_transmit_packet_statistics->NDL_tx_channel_use);
        kfree(database_ptr->NDL_transmit_packet_statistics->NDL_tx_signal_avg_power);
        kfree(database_ptr->NDL_transmit_packet_statistics);

        kfree(database_ptr->NDL_channel_load_measures);

        kfree(database_ptr->NDL_communication_ranges);

        kfree(database_ptr->NDL_transmit_queue_parameters->NDL_queue_len);
        kfree(database_ptr->NDL_transmit_queue_parameters->NDL_ref_queue_status);
        kfree(database_ptr->NDL_transmit_queue_parameters);

        kfree(database_ptr->NDL_channel_load_tresholds);

        kfree(database_ptr->NDL_transmit_model_parameters->NDL_tm_channel_use);
        kfree(database_ptr->NDL_transmit_model_parameters->NDL_tm_packet_arrival_rate);
        kfree(database_ptr->NDL_transmit_model_parameters->NDL_tm_packet_avg_duration);
        kfree(database_ptr->NDL_transmit_model_parameters->NDL_tm_signal_avg_power);
        kfree(database_ptr->NDL_transmit_model_parameters);

        kfree(database_ptr->NDL_demodulation_model_parameters->NDL_snr_backoff);
        kfree(database_ptr->NDL_demodulation_model_parameters->NDL_required_snr);
        kfree(database_ptr->NDL_demodulation_model_parameters);

        kfree(database_ptr->NDL_receive_model_parameters);

        kfree(database_ptr->NDL_receive_signal_tresholds);

        kfree(database_ptr->NDL_packet_datarate_tresholds->NDL_def_datarate);
        kfree(database_ptr->NDL_packet_datarate_tresholds->NDL_ref_datarate);
        kfree(database_ptr->NDL_packet_datarate_tresholds);

        kfree(database_ptr->NDL_packet_timing_tresholds->NDL_max_packet_duration);
        kfree(database_ptr->NDL_packet_timing_tresholds->NDL_def_packet_interval);
        kfree(database_ptr->NDL_packet_timing_tresholds->NDL_ref_packet_interval);
        kfree(database_ptr->NDL_packet_timing_tresholds);

        kfree(database_ptr->NDL_transmit_power_tresholds->NDL_def_tx_power);
        kfree(database_ptr->NDL_transmit_power_tresholds->NDL_ref_tx_power);
        kfree(database_ptr->NDL_transmit_power_tresholds);

#ifndef __KERNEL__
        pthread_mutex_destroy(&(database_ptr->database_in_use));
#endif // __KERNEL__

        kfree(database_ptr);
        *_database = database_ptr;
        return _status;
}


ndlType_snr encode_to_NDL_snr(int physical_value)
{
        ndlType_snr ndl_value;
        int fixed_point = 10;
        ndl_value = (physical_value - (-10) * fixed_point) / 5;
        return ndl_value;
}
int decode_from_NDL_snr(ndlType_snr ndl_value)
{
        int physical_value;
        int fixed_point = 10;
        physical_value = (-10) * fixed_point + ndl_value * 5;
        return physical_value;
}


ndlType_tx_power encode_to_NDL_tx_power(int physical_value)
{
        ndlType_tx_power ndl_value;
        int fixed_point = 10;
        ndl_value = (physical_value - (-20) * fixed_point) / 5;
        return ndl_value;
}

int decode_from_NDL_tx_power(ndlType_tx_power ndl_value)
{
        int physical_value;
        int fixed_point = 10;
        physical_value = (-20) * fixed_point + ndl_value * 5;
        return physical_value;
}

ndlType_rx_power encode_to_NDL_rx_power(int physical_value)
{
        ndlType_rx_power ndl_value;
        int fixed_point = 10;
        ndl_value = (physical_value - (-40) * fixed_point) / (-5);
        return ndl_value;
}
int decode_from_NDL_rx_power(ndlType_rx_power ndl_value)
{
        int physical_value;
        int fixed_point = 10;
        physical_value = (-40) * fixed_point + ndl_value * (-5);
        return physical_value;
}

ndlType_timing encode_to_NDL_timing(int physical_value)
{
        ndlType_timing ndl_value;
        ndl_value = physical_value / 10;
        return ndl_value;
}
int decode_from_NDL_timing(ndlType_timing ndl_value)
{
        int physical_value;
        physical_value = ndl_value * 10;
        return physical_value;
}

ndlType_pathloss encode_to_NDL_pathloss(int physical_value)
{
        ndlType_pathloss ndl_value;
        int fixed_point = 10;
        ndl_value = (physical_value - 1 * fixed_point) / 1;
        return ndl_value;
}
int decode_from_NDL_pathloss(ndlType_pathloss ndl_value)
{
        int physical_value;
        int fixed_point = 10;
        physical_value = 1 * fixed_point + ndl_value * 1;
        return physical_value;
}

ndlType_packet_interval encode_to_NDL_packet_interval(int physical_value)
{
        ndlType_packet_interval ndl_value;
        ndl_value = physical_value / 10;
        return ndl_value;
}
int decode_from_NDL_packet_interval(ndlType_packet_interval ndl_value)
{
        int physical_value;
        physical_value = ndl_value * 10;
        return physical_value;
}

ndlType_exponent encode_to_NDL_exponent(int physical_value)
{
        ndlType_pathloss ndl_value;
        ndl_value = physical_value;
        return ndl_value;
}
int decode_from_NDL_exponent(ndlType_exponent ndl_value)
{
        int physical_value;
        physical_value = ndl_value;
        return physical_value;
}

ndlType_channel_load encode_to_NDL_channel_load(int physical_value)
{
        ndlType_channel_load ndl_value;
        ndl_value = physical_value;
        return ndl_value;
}
int decode_from_NDL_channel_load(ndlType_channel_load ndl_value)
{
        int physical_value;
        physical_value = ndl_value;
        return physical_value;
}

ndlType_channel_use encode_to_NDL_channel_use(int physical_value)
{
        ndlType_channel_use ndl_value;
        ndl_value = physical_value / 125;
        return ndl_value;
}
int decode_from_NDL_channel_use(ndlType_channel_use ndl_value)
{
        int physical_value;
        physical_value = ndl_value * 125;
        return physical_value;
}

ndlType_arrival_rate encode_to_NDL_arrival_rate(int physical_value)
{
        ndlType_channel_use ndl_value;
        ndl_value = physical_value;
        return ndl_value;
}
int decode_from_NDL_arrival_rate(ndlType_arrival_rate ndl_value)
{
        int physical_value;
        physical_value = ndl_value;
        return physical_value;
}

ndlType_packet_duration encode_to_NDL_packet_duration(int physical_value)
{
        ndlType_packet_duration ndl_value;
        ndl_value = physical_value / 10;
        return ndl_value;
}
int decode_from_NDL_packet_duration(ndlType_packet_duration ndl_value)
{
        int physical_value;
        physical_value = ndl_value * 10;
        return physical_value;
}

int set_NDL_default_values(struct ndlType_ndl_database **_database, int mode)
{
        int _status = 0;
        int restrictive_state_id = 0;
        struct ndlType_ndl_database *database_ptr = *_database;
        int i;
	database_ptr->msg_type = msg_NDL_DATABASE;
        database_ptr->NDL_transmit_power_tresholds->NDL_min_tx_power = encode_to_NDL_tx_power(-100);
        database_ptr->NDL_transmit_power_tresholds->NDL_max_tx_power = encode_to_NDL_tx_power(330);

        database_ptr->NDL_receive_signal_tresholds->NDL_min_carrier_sense = encode_to_NDL_rx_power(-950);
        database_ptr->NDL_receive_signal_tresholds->NDL_max_carrier_sense = encode_to_NDL_rx_power(-650);
        database_ptr->NDL_receive_signal_tresholds->NDL_def_carrier_sense = encode_to_NDL_rx_power(-850);
        database_ptr->NDL_receive_signal_tresholds->NDL_ref_carrier_sense = encode_to_NDL_rx_power(-850);

        database_ptr->NDL_receive_model_parameters->NDL_def_dcc_sensitivity = encode_to_NDL_rx_power(-850);
        database_ptr->NDL_receive_model_parameters->NDL_max_cs_range = 1000;
        database_ptr->NDL_receive_model_parameters->NDL_ref_pathloss = encode_to_NDL_pathloss(20);
        database_ptr->NDL_receive_model_parameters->NDL_min_snr = encode_to_NDL_snr(100);

        database_ptr->NDL_demodulation_model_parameters->NDL_snr_backoff[0] = encode_to_NDL_snr(0);
        database_ptr->NDL_demodulation_model_parameters->NDL_snr_backoff[1] = encode_to_NDL_snr(10);
        database_ptr->NDL_demodulation_model_parameters->NDL_snr_backoff[2] = encode_to_NDL_snr(30);
        database_ptr->NDL_demodulation_model_parameters->NDL_snr_backoff[3] = encode_to_NDL_snr(50);
        database_ptr->NDL_demodulation_model_parameters->NDL_snr_backoff[4] = encode_to_NDL_snr(80);
        database_ptr->NDL_demodulation_model_parameters->NDL_snr_backoff[5] = encode_to_NDL_snr(120);
        database_ptr->NDL_demodulation_model_parameters->NDL_snr_backoff[6] = encode_to_NDL_snr(160);
        database_ptr->NDL_demodulation_model_parameters->NDL_snr_backoff[7] = encode_to_NDL_snr(170);

        database_ptr->NDL_demodulation_model_parameters->NDL_required_snr[0] = encode_to_NDL_snr(100);
        database_ptr->NDL_demodulation_model_parameters->NDL_required_snr[1] = encode_to_NDL_snr(110);
        database_ptr->NDL_demodulation_model_parameters->NDL_required_snr[2] = encode_to_NDL_snr(130);
        database_ptr->NDL_demodulation_model_parameters->NDL_required_snr[3] = encode_to_NDL_snr(150);
        database_ptr->NDL_demodulation_model_parameters->NDL_required_snr[4] = encode_to_NDL_snr(180);
        database_ptr->NDL_demodulation_model_parameters->NDL_required_snr[5] = encode_to_NDL_snr(220);
        database_ptr->NDL_demodulation_model_parameters->NDL_required_snr[6] = encode_to_NDL_snr(260);
        database_ptr->NDL_demodulation_model_parameters->NDL_required_snr[7] = encode_to_NDL_snr(270);

        database_ptr->NDL_transmit_queue_parameters->NDL_num_queue = NL80211_NUM_ACS;

        for (i = 0; i< NL80211_NUM_ACS; i++) {
                database_ptr->NDL_transmit_power_tresholds->NDL_def_tx_power[i] = encode_to_NDL_tx_power(230);
                database_ptr->NDL_transmit_power_tresholds->NDL_ref_tx_power[i] = encode_to_NDL_tx_power(230);

                database_ptr->NDL_transmit_queue_parameters->NDL_ref_queue_status[i] = DCC_QUEUE_OPEN;
        }


        database_ptr->NDL_general_configuration->NDL_time_up = encode_to_NDL_timing(1000);
        database_ptr->NDL_general_configuration->NDL_time_down = encode_to_NDL_timing(5000);
        database_ptr->NDL_general_configuration->NDL_min_dcc_sampling = encode_to_NDL_timing(1000);


        switch (mode) {
        case G5CC:
                {
                    set_default_values_G5CC(&database_ptr);
                }
        case G5SC:
                {
                    set_default_values_G5SC(&database_ptr);
                }
        }

        database_ptr->NDL_state_list[0]->NDL_as_state_id = 0;
        database_ptr->NDL_state_list[0]->NDL_as_chan_load = database_ptr->NDL_channel_load_tresholds->NDL_min_channel_load;
        database_ptr->NDL_state_list[0]->NDL_as_dcc[NL80211_AC_VI] = DCCm_ALL;
        database_ptr->NDL_state_list[0]->NDL_as_dcc[NL80211_AC_VO] = DCCm_ALL;
        database_ptr->NDL_state_list[0]->NDL_as_dcc[NL80211_AC_BE] = DCCm_ALL;
        database_ptr->NDL_state_list[0]->NDL_as_dcc[NL80211_AC_BK] = DCCm_ALL;
        database_ptr->NDL_state_list[0]->NDL_as_tx_power[NL80211_AC_VI] = database_ptr->NDL_transmit_power_tresholds->NDL_max_tx_power;
        database_ptr->NDL_state_list[0]->NDL_as_tx_power[NL80211_AC_VO] = database_ptr->NDL_transmit_power_tresholds->NDL_max_tx_power;
        database_ptr->NDL_state_list[0]->NDL_as_tx_power[NL80211_AC_BE] = database_ptr->NDL_transmit_power_tresholds->NDL_max_tx_power;
        database_ptr->NDL_state_list[0]->NDL_as_tx_power[NL80211_AC_BK] = database_ptr->NDL_transmit_power_tresholds->NDL_max_tx_power;
        database_ptr->NDL_state_list[0]->NDL_as_packet_interval[NL80211_AC_VI] = database_ptr->NDL_packet_timing_tresholds->NDL_min_packet_interval;
        database_ptr->NDL_state_list[0]->NDL_as_packet_interval[NL80211_AC_VO] = database_ptr->NDL_packet_timing_tresholds->NDL_min_packet_interval;
        database_ptr->NDL_state_list[0]->NDL_as_packet_interval[NL80211_AC_BE] = database_ptr->NDL_packet_timing_tresholds->NDL_min_packet_interval;
        database_ptr->NDL_state_list[0]->NDL_as_packet_interval[NL80211_AC_BK] = database_ptr->NDL_packet_timing_tresholds->NDL_min_packet_interval;
        database_ptr->NDL_state_list[0]->NDL_as_datarate[NL80211_AC_VI] = database_ptr->NDL_packet_datarate_tresholds->NDL_min_datarate;
        database_ptr->NDL_state_list[0]->NDL_as_datarate[NL80211_AC_VO] = database_ptr->NDL_packet_datarate_tresholds->NDL_min_datarate;
        database_ptr->NDL_state_list[0]->NDL_as_datarate[NL80211_AC_BE] = database_ptr->NDL_packet_datarate_tresholds->NDL_min_datarate;
        database_ptr->NDL_state_list[0]->NDL_as_datarate[NL80211_AC_BK] = database_ptr->NDL_packet_datarate_tresholds->NDL_min_datarate;
        database_ptr->NDL_state_list[0]->NDL_as_carrier_sense[NL80211_AC_VI] = database_ptr->NDL_receive_signal_tresholds->NDL_min_carrier_sense;
        database_ptr->NDL_state_list[0]->NDL_as_carrier_sense[NL80211_AC_VO] = database_ptr->NDL_receive_signal_tresholds->NDL_min_carrier_sense;
        database_ptr->NDL_state_list[0]->NDL_as_carrier_sense[NL80211_AC_BE] = database_ptr->NDL_receive_signal_tresholds->NDL_min_carrier_sense;
        database_ptr->NDL_state_list[0]->NDL_as_carrier_sense[NL80211_AC_BK] = database_ptr->NDL_receive_signal_tresholds->NDL_min_carrier_sense;

        restrictive_state_id = (database_ptr->NDL_general_configuration->NDL_num_active_state) + 1;

        database_ptr->NDL_state_list[restrictive_state_id]->NDL_as_state_id = restrictive_state_id;
        database_ptr->NDL_state_list[restrictive_state_id]->NDL_as_chan_load = database_ptr->NDL_channel_load_tresholds->NDL_min_channel_load;
        database_ptr->NDL_state_list[restrictive_state_id]->NDL_as_dcc[NL80211_AC_VI] = DCCm_ALL;
        database_ptr->NDL_state_list[restrictive_state_id]->NDL_as_dcc[NL80211_AC_VO] = DCCm_ALL;
        database_ptr->NDL_state_list[restrictive_state_id]->NDL_as_dcc[NL80211_AC_BE] = DCCm_ALL;
        database_ptr->NDL_state_list[restrictive_state_id]->NDL_as_dcc[NL80211_AC_BK] = DCCm_ALL;
        database_ptr->NDL_state_list[restrictive_state_id]->NDL_as_tx_power[NL80211_AC_VI] = database_ptr->NDL_transmit_power_tresholds->NDL_min_tx_power;
        database_ptr->NDL_state_list[restrictive_state_id]->NDL_as_tx_power[NL80211_AC_VO] = database_ptr->NDL_transmit_power_tresholds->NDL_min_tx_power;
        database_ptr->NDL_state_list[restrictive_state_id]->NDL_as_tx_power[NL80211_AC_BE] = database_ptr->NDL_transmit_power_tresholds->NDL_min_tx_power;
        database_ptr->NDL_state_list[restrictive_state_id]->NDL_as_tx_power[NL80211_AC_BK] = database_ptr->NDL_transmit_power_tresholds->NDL_min_tx_power;
        database_ptr->NDL_state_list[restrictive_state_id]->NDL_as_packet_interval[NL80211_AC_VI] = database_ptr->NDL_packet_timing_tresholds->NDL_max_packet_interval;
        database_ptr->NDL_state_list[restrictive_state_id]->NDL_as_packet_interval[NL80211_AC_VO] = database_ptr->NDL_packet_timing_tresholds->NDL_max_packet_interval;
        database_ptr->NDL_state_list[restrictive_state_id]->NDL_as_packet_interval[NL80211_AC_BE] = database_ptr->NDL_packet_timing_tresholds->NDL_max_packet_interval;
        database_ptr->NDL_state_list[restrictive_state_id]->NDL_as_packet_interval[NL80211_AC_BK] = database_ptr->NDL_packet_timing_tresholds->NDL_max_packet_interval;
        database_ptr->NDL_state_list[restrictive_state_id]->NDL_as_datarate[NL80211_AC_VI] = database_ptr->NDL_packet_datarate_tresholds->NDL_max_datarate;
        database_ptr->NDL_state_list[restrictive_state_id]->NDL_as_datarate[NL80211_AC_VO] = database_ptr->NDL_packet_datarate_tresholds->NDL_max_datarate;
        database_ptr->NDL_state_list[restrictive_state_id]->NDL_as_datarate[NL80211_AC_BE] = database_ptr->NDL_packet_datarate_tresholds->NDL_max_datarate;
        database_ptr->NDL_state_list[restrictive_state_id]->NDL_as_datarate[NL80211_AC_BK] = database_ptr->NDL_packet_datarate_tresholds->NDL_max_datarate;
        database_ptr->NDL_state_list[restrictive_state_id]->NDL_as_carrier_sense[NL80211_AC_VI] = database_ptr->NDL_receive_signal_tresholds->NDL_max_carrier_sense;
        database_ptr->NDL_state_list[restrictive_state_id]->NDL_as_carrier_sense[NL80211_AC_VO] = database_ptr->NDL_receive_signal_tresholds->NDL_max_carrier_sense;
        database_ptr->NDL_state_list[restrictive_state_id]->NDL_as_carrier_sense[NL80211_AC_BE] = database_ptr->NDL_receive_signal_tresholds->NDL_max_carrier_sense;
        database_ptr->NDL_state_list[restrictive_state_id]->NDL_as_carrier_sense[NL80211_AC_BK] = database_ptr->NDL_receive_signal_tresholds->NDL_max_carrier_sense;



        database_ptr->NDL_transmit_power_tresholds->NDL_ref_tx_power[NL80211_AC_VI] = database_ptr->NDL_transmit_power_tresholds->NDL_max_tx_power;
        database_ptr->NDL_transmit_power_tresholds->NDL_ref_tx_power[NL80211_AC_VO] = database_ptr->NDL_transmit_power_tresholds->NDL_max_tx_power;
        database_ptr->NDL_transmit_power_tresholds->NDL_ref_tx_power[NL80211_AC_BE] = database_ptr->NDL_transmit_power_tresholds->NDL_max_tx_power;
        database_ptr->NDL_transmit_power_tresholds->NDL_ref_tx_power[NL80211_AC_BK] = database_ptr->NDL_transmit_power_tresholds->NDL_max_tx_power;
        database_ptr->NDL_packet_timing_tresholds->NDL_ref_packet_interval[NL80211_AC_VI] = database_ptr->NDL_packet_timing_tresholds->NDL_min_packet_interval;
        database_ptr->NDL_packet_timing_tresholds->NDL_ref_packet_interval[NL80211_AC_VO] = database_ptr->NDL_packet_timing_tresholds->NDL_min_packet_interval;
        database_ptr->NDL_packet_timing_tresholds->NDL_ref_packet_interval[NL80211_AC_BE] = database_ptr->NDL_packet_timing_tresholds->NDL_min_packet_interval;
        database_ptr->NDL_packet_timing_tresholds->NDL_ref_packet_interval[NL80211_AC_BK] = database_ptr->NDL_packet_timing_tresholds->NDL_min_packet_interval;
        database_ptr->NDL_packet_datarate_tresholds->NDL_ref_datarate[NL80211_AC_VI] = database_ptr->NDL_packet_datarate_tresholds->NDL_min_datarate;
        database_ptr->NDL_packet_datarate_tresholds->NDL_ref_datarate[NL80211_AC_VO] = database_ptr->NDL_packet_datarate_tresholds->NDL_min_datarate;
        database_ptr->NDL_packet_datarate_tresholds->NDL_ref_datarate[NL80211_AC_BE] = database_ptr->NDL_packet_datarate_tresholds->NDL_min_datarate;
        database_ptr->NDL_packet_datarate_tresholds->NDL_ref_datarate[NL80211_AC_BK] = database_ptr->NDL_packet_datarate_tresholds->NDL_min_datarate;

	database_ptr->NDL_channel_load_measures->NDL_channel_busy_time = 0;
        database_ptr->NDL_channel_load_measures->NDL_channel_load = 0;
        database_ptr->NDL_channel_load_measures->NDL_load_arrival_rate= 0;
        database_ptr->NDL_channel_load_measures->NDL_load_avg_duration = 0;
        database_ptr->NDL_channel_load_measures->NDL_packet_arrival_rate = 0;
        database_ptr->NDL_channel_load_measures->NDL_packet_avg_duration = 0;


        print_debug("All NDL database attributes set to default value.");
        *_database = database_ptr;
        return _status;
}


void set_default_values_G5CC(struct ndlType_ndl_database **_database)
{
        struct ndlType_ndl_database *database_ptr = *_database;
        int i;
        database_ptr->NDL_packet_timing_tresholds->NDL_max_packet_interval = encode_to_NDL_packet_interval(1000);
        database_ptr->NDL_packet_timing_tresholds->NDL_min_packet_interval = encode_to_NDL_packet_interval(40);

        database_ptr->NDL_packet_datarate_tresholds->NDL_max_datarate = 4;
        database_ptr->NDL_packet_datarate_tresholds->NDL_min_datarate = 0;

        database_ptr->NDL_channel_load_tresholds->NDL_max_channel_load = encode_to_NDL_channel_load(400);
        database_ptr->NDL_channel_load_tresholds->NDL_min_channel_load = encode_to_NDL_channel_load(150);

        for (i = 0; i< NL80211_NUM_ACS; i++) {
                database_ptr->NDL_packet_timing_tresholds->NDL_max_packet_duration[i] = encode_to_NDL_packet_duration(600);
                database_ptr->NDL_packet_timing_tresholds->NDL_def_packet_interval[i] = encode_to_NDL_packet_interval(500);
                database_ptr->NDL_packet_timing_tresholds->NDL_ref_packet_interval[i] = encode_to_NDL_packet_interval(500);

                database_ptr->NDL_packet_datarate_tresholds->NDL_def_datarate[i] = 2;
                database_ptr->NDL_packet_datarate_tresholds->NDL_ref_datarate[i] = 2;

                database_ptr->NDL_transmit_queue_parameters->NDL_queue_len[i] = 2;
        }

        database_ptr->NDL_general_configuration->NDL_num_active_state = 1;
        database_ptr->NDL_general_configuration->NDL_cur_active_state = 0;

        database_ptr->NDL_state_list[1]->NDL_as_state_id = 1;
        database_ptr->NDL_state_list[1]->NDL_as_chan_load = encode_to_NDL_channel_load(200);
        database_ptr->NDL_state_list[1]->NDL_as_dcc[NL80211_AC_VI] = DCCm_NONE;
        database_ptr->NDL_state_list[1]->NDL_as_dcc[NL80211_AC_VO] = DCCm_TPC_bit;
        database_ptr->NDL_state_list[1]->NDL_as_dcc[NL80211_AC_BE] = DCCm_TPC_bit;
        database_ptr->NDL_state_list[1]->NDL_as_dcc[NL80211_AC_BK] = DCCm_TPC_bit;
        database_ptr->NDL_state_list[1]->NDL_as_tx_power[NL80211_AC_VI] = encode_to_NDL_tx_power(230);
        database_ptr->NDL_state_list[1]->NDL_as_tx_power[NL80211_AC_VO] = encode_to_NDL_tx_power(250);
        database_ptr->NDL_state_list[1]->NDL_as_tx_power[NL80211_AC_BE] = encode_to_NDL_tx_power(200);
        database_ptr->NDL_state_list[1]->NDL_as_tx_power[NL80211_AC_BK] = encode_to_NDL_tx_power(150);
        database_ptr->NDL_state_list[1]->NDL_as_packet_interval[NL80211_AC_VI] = encode_to_NDL_packet_interval(500);
        database_ptr->NDL_state_list[1]->NDL_as_packet_interval[NL80211_AC_VO] = encode_to_NDL_packet_interval(500);
        database_ptr->NDL_state_list[1]->NDL_as_packet_interval[NL80211_AC_BE] = encode_to_NDL_packet_interval(500);
        database_ptr->NDL_state_list[1]->NDL_as_packet_interval[NL80211_AC_BK] = encode_to_NDL_packet_interval(500);
        database_ptr->NDL_state_list[1]->NDL_as_datarate[NL80211_AC_VI] = 2;
        database_ptr->NDL_state_list[1]->NDL_as_datarate[NL80211_AC_VO] = 2;
        database_ptr->NDL_state_list[1]->NDL_as_datarate[NL80211_AC_BE] = 2;
        database_ptr->NDL_state_list[1]->NDL_as_datarate[NL80211_AC_BK] = 2;
        database_ptr->NDL_state_list[1]->NDL_as_carrier_sense[NL80211_AC_VI] = encode_to_NDL_rx_power(-850);
        database_ptr->NDL_state_list[1]->NDL_as_carrier_sense[NL80211_AC_VO] = encode_to_NDL_rx_power(-850);
        database_ptr->NDL_state_list[1]->NDL_as_carrier_sense[NL80211_AC_BE] = encode_to_NDL_rx_power(-850);
        database_ptr->NDL_state_list[1]->NDL_as_carrier_sense[NL80211_AC_BK] = encode_to_NDL_rx_power(-850);
        *_database = database_ptr;
}

void set_default_values_G5SC(struct ndlType_ndl_database **_database)
{
        struct ndlType_ndl_database *database_ptr = *_database;
        int i;
        database_ptr->NDL_packet_timing_tresholds->NDL_max_packet_interval = encode_to_NDL_packet_interval(2000);
        database_ptr->NDL_packet_timing_tresholds->NDL_min_packet_interval = encode_to_NDL_packet_interval(40);

        database_ptr->NDL_packet_datarate_tresholds->NDL_max_datarate = 5;
        database_ptr->NDL_packet_datarate_tresholds->NDL_min_datarate = 2;

        database_ptr->NDL_channel_load_tresholds->NDL_max_channel_load = encode_to_NDL_channel_load(500);
        database_ptr->NDL_channel_load_tresholds->NDL_min_channel_load = encode_to_NDL_channel_load(200);

        for(i = 0; i< NL80211_NUM_ACS; i++) {
                database_ptr->NDL_packet_timing_tresholds->NDL_max_packet_duration[i] = encode_to_NDL_packet_duration(1000);
                database_ptr->NDL_packet_timing_tresholds->NDL_def_packet_interval[i] = encode_to_NDL_packet_interval(500);
                database_ptr->NDL_packet_timing_tresholds->NDL_ref_packet_interval[i] = encode_to_NDL_packet_interval(500);

                database_ptr->NDL_packet_datarate_tresholds->NDL_def_datarate[i] = 2;
                database_ptr->NDL_packet_datarate_tresholds->NDL_ref_datarate[i] = 2;

                database_ptr->NDL_transmit_queue_parameters->NDL_queue_len[i] = 8;
        }

        database_ptr->NDL_general_configuration->NDL_num_active_state = 4;
        database_ptr->NDL_general_configuration->NDL_cur_active_state = 0;

        database_ptr->NDL_state_list[1]->NDL_as_state_id = 1;
        database_ptr->NDL_state_list[1]->NDL_as_chan_load = encode_to_NDL_channel_load(250);
        database_ptr->NDL_state_list[1]->NDL_as_dcc[NL80211_AC_VI] = DCCm_NONE;
        database_ptr->NDL_state_list[1]->NDL_as_dcc[NL80211_AC_VO] = DCCm_NONE;
        database_ptr->NDL_state_list[1]->NDL_as_dcc[NL80211_AC_BE] = DCCm_TPC_bit;
        database_ptr->NDL_state_list[1]->NDL_as_dcc[NL80211_AC_BK] = DCCm_TPC_bit;
        database_ptr->NDL_state_list[1]->NDL_as_tx_power[NL80211_AC_VI] = encode_to_NDL_tx_power(230);
        database_ptr->NDL_state_list[1]->NDL_as_tx_power[NL80211_AC_VO] = encode_to_NDL_tx_power(230);
        database_ptr->NDL_state_list[1]->NDL_as_tx_power[NL80211_AC_BE] = encode_to_NDL_tx_power(250);
        database_ptr->NDL_state_list[1]->NDL_as_tx_power[NL80211_AC_BK] = encode_to_NDL_tx_power(200);
        database_ptr->NDL_state_list[1]->NDL_as_packet_interval[NL80211_AC_VI] = encode_to_NDL_packet_interval(500);
        database_ptr->NDL_state_list[1]->NDL_as_packet_interval[NL80211_AC_VO] = encode_to_NDL_packet_interval(500);
        database_ptr->NDL_state_list[1]->NDL_as_packet_interval[NL80211_AC_BE] = encode_to_NDL_packet_interval(500);
        database_ptr->NDL_state_list[1]->NDL_as_packet_interval[NL80211_AC_BK] = encode_to_NDL_packet_interval(500);
        database_ptr->NDL_state_list[1]->NDL_as_datarate[NL80211_AC_VI] = 2;
        database_ptr->NDL_state_list[1]->NDL_as_datarate[NL80211_AC_VO] = 2;
        database_ptr->NDL_state_list[1]->NDL_as_datarate[NL80211_AC_BE] = 2;
        database_ptr->NDL_state_list[1]->NDL_as_datarate[NL80211_AC_BK] = 2;
        database_ptr->NDL_state_list[1]->NDL_as_carrier_sense[NL80211_AC_VI] = encode_to_NDL_rx_power(-850);
        database_ptr->NDL_state_list[1]->NDL_as_carrier_sense[NL80211_AC_VO] = encode_to_NDL_rx_power(-850);
        database_ptr->NDL_state_list[1]->NDL_as_carrier_sense[NL80211_AC_BE] = encode_to_NDL_rx_power(-850);
        database_ptr->NDL_state_list[1]->NDL_as_carrier_sense[NL80211_AC_BK] = encode_to_NDL_rx_power(-850);

        database_ptr->NDL_state_list[2]->NDL_as_state_id = 2;
        database_ptr->NDL_state_list[2]->NDL_as_chan_load = encode_to_NDL_channel_load(300);
        database_ptr->NDL_state_list[2]->NDL_as_dcc[NL80211_AC_VI] = DCCm_TPC_bit;
        database_ptr->NDL_state_list[2]->NDL_as_dcc[NL80211_AC_VO] = DCCm_TPC_bit;
        database_ptr->NDL_state_list[2]->NDL_as_dcc[NL80211_AC_BE] = DCCm_TPC_bit;
        database_ptr->NDL_state_list[2]->NDL_as_dcc[NL80211_AC_BK] = DCCm_TPC_bit | DCCm_TRC_bit;
        database_ptr->NDL_state_list[2]->NDL_as_tx_power[NL80211_AC_VI] = encode_to_NDL_tx_power(250);
        database_ptr->NDL_state_list[2]->NDL_as_tx_power[NL80211_AC_VO] = encode_to_NDL_tx_power(250);
        database_ptr->NDL_state_list[2]->NDL_as_tx_power[NL80211_AC_BE] = encode_to_NDL_tx_power(200);
        database_ptr->NDL_state_list[2]->NDL_as_tx_power[NL80211_AC_BK] = encode_to_NDL_tx_power(100);
        database_ptr->NDL_state_list[2]->NDL_as_packet_interval[NL80211_AC_VI] = encode_to_NDL_packet_interval(500);
        database_ptr->NDL_state_list[2]->NDL_as_packet_interval[NL80211_AC_VO] = encode_to_NDL_packet_interval(500);
        database_ptr->NDL_state_list[2]->NDL_as_packet_interval[NL80211_AC_BE] = encode_to_NDL_packet_interval(500);
        database_ptr->NDL_state_list[2]->NDL_as_packet_interval[NL80211_AC_BK] = encode_to_NDL_packet_interval(1000);
        database_ptr->NDL_state_list[2]->NDL_as_datarate[NL80211_AC_VI] = 2;
        database_ptr->NDL_state_list[2]->NDL_as_datarate[NL80211_AC_VO] = 2;
        database_ptr->NDL_state_list[2]->NDL_as_datarate[NL80211_AC_BE] = 2;
        database_ptr->NDL_state_list[2]->NDL_as_datarate[NL80211_AC_BK] = 2;
        database_ptr->NDL_state_list[2]->NDL_as_carrier_sense[NL80211_AC_VI] = encode_to_NDL_rx_power(-850);
        database_ptr->NDL_state_list[2]->NDL_as_carrier_sense[NL80211_AC_VO] = encode_to_NDL_rx_power(-850);
        database_ptr->NDL_state_list[2]->NDL_as_carrier_sense[NL80211_AC_BE] = encode_to_NDL_rx_power(-850);
        database_ptr->NDL_state_list[2]->NDL_as_carrier_sense[NL80211_AC_BK] = encode_to_NDL_rx_power(-850);

        database_ptr->NDL_state_list[3]->NDL_as_state_id = 3;
        database_ptr->NDL_state_list[3]->NDL_as_chan_load = encode_to_NDL_channel_load(350);
        database_ptr->NDL_state_list[3]->NDL_as_dcc[NL80211_AC_VI] = DCCm_TPC_bit;
        database_ptr->NDL_state_list[3]->NDL_as_dcc[NL80211_AC_VO] = DCCm_TPC_bit;
        database_ptr->NDL_state_list[3]->NDL_as_dcc[NL80211_AC_BE] = DCCm_TPC_bit | DCCm_TRC_bit | DCCm_TDC_bit;
        database_ptr->NDL_state_list[3]->NDL_as_dcc[NL80211_AC_BK] = DCCm_TPC_bit | DCCm_TRC_bit | DCCm_TDC_bit;
        database_ptr->NDL_state_list[3]->NDL_as_tx_power[NL80211_AC_VI] = encode_to_NDL_tx_power(150);
        database_ptr->NDL_state_list[3]->NDL_as_tx_power[NL80211_AC_VO] = encode_to_NDL_tx_power(150);
        database_ptr->NDL_state_list[3]->NDL_as_tx_power[NL80211_AC_BE] = encode_to_NDL_tx_power(100);
        database_ptr->NDL_state_list[3]->NDL_as_tx_power[NL80211_AC_BK] = encode_to_NDL_tx_power(50);
        database_ptr->NDL_state_list[3]->NDL_as_packet_interval[NL80211_AC_VI] = encode_to_NDL_packet_interval(500);
        database_ptr->NDL_state_list[3]->NDL_as_packet_interval[NL80211_AC_VO] = encode_to_NDL_packet_interval(500);
        database_ptr->NDL_state_list[3]->NDL_as_packet_interval[NL80211_AC_BE] = encode_to_NDL_packet_interval(1000);
        database_ptr->NDL_state_list[3]->NDL_as_packet_interval[NL80211_AC_BK] = encode_to_NDL_packet_interval(1500);
        database_ptr->NDL_state_list[3]->NDL_as_datarate[NL80211_AC_VI] = 2;
        database_ptr->NDL_state_list[3]->NDL_as_datarate[NL80211_AC_VO] = 2;
        database_ptr->NDL_state_list[3]->NDL_as_datarate[NL80211_AC_BE] = 3;
        database_ptr->NDL_state_list[3]->NDL_as_datarate[NL80211_AC_BK] = 3;
        database_ptr->NDL_state_list[3]->NDL_as_carrier_sense[NL80211_AC_VI] = encode_to_NDL_rx_power(-850);
        database_ptr->NDL_state_list[3]->NDL_as_carrier_sense[NL80211_AC_VO] = encode_to_NDL_rx_power(-850);
        database_ptr->NDL_state_list[3]->NDL_as_carrier_sense[NL80211_AC_BE] = encode_to_NDL_rx_power(-850);
        database_ptr->NDL_state_list[3]->NDL_as_carrier_sense[NL80211_AC_BK] = encode_to_NDL_rx_power(-850);

        database_ptr->NDL_state_list[4]->NDL_as_state_id = 4;
        database_ptr->NDL_state_list[4]->NDL_as_chan_load = encode_to_NDL_channel_load(400);
        database_ptr->NDL_state_list[4]->NDL_as_dcc[NL80211_AC_VI] = DCCm_TPC_bit | DCCm_TDC_bit;
        database_ptr->NDL_state_list[4]->NDL_as_dcc[NL80211_AC_VO] = DCCm_TPC_bit | DCCm_TRC_bit | DCCm_TDC_bit;
        database_ptr->NDL_state_list[4]->NDL_as_dcc[NL80211_AC_BE] = DCCm_TPC_bit | DCCm_TRC_bit | DCCm_TDC_bit;
        database_ptr->NDL_state_list[4]->NDL_as_dcc[NL80211_AC_BK] = DCCm_TPC_bit | DCCm_TRC_bit | DCCm_TDC_bit;
        database_ptr->NDL_state_list[4]->NDL_as_tx_power[NL80211_AC_VI] = encode_to_NDL_tx_power(50);
        database_ptr->NDL_state_list[4]->NDL_as_tx_power[NL80211_AC_VO] = encode_to_NDL_tx_power(0);
        database_ptr->NDL_state_list[4]->NDL_as_tx_power[NL80211_AC_BE] = encode_to_NDL_tx_power(-50);
        database_ptr->NDL_state_list[4]->NDL_as_tx_power[NL80211_AC_BK] = encode_to_NDL_tx_power(-100);
        database_ptr->NDL_state_list[4]->NDL_as_packet_interval[NL80211_AC_VI] = encode_to_NDL_packet_interval(500);
        database_ptr->NDL_state_list[4]->NDL_as_packet_interval[NL80211_AC_VO] = encode_to_NDL_packet_interval(1000);
        database_ptr->NDL_state_list[4]->NDL_as_packet_interval[NL80211_AC_BE] = encode_to_NDL_packet_interval(1500);
        database_ptr->NDL_state_list[4]->NDL_as_packet_interval[NL80211_AC_BK] = encode_to_NDL_packet_interval(2000);
        database_ptr->NDL_state_list[4]->NDL_as_datarate[NL80211_AC_VI] = 4;
        database_ptr->NDL_state_list[4]->NDL_as_datarate[NL80211_AC_VO] = 4;
        database_ptr->NDL_state_list[4]->NDL_as_datarate[NL80211_AC_BE] = 5;
        database_ptr->NDL_state_list[4]->NDL_as_datarate[NL80211_AC_BK] = 5;
        database_ptr->NDL_state_list[4]->NDL_as_carrier_sense[NL80211_AC_VI] = encode_to_NDL_rx_power(-850);
        database_ptr->NDL_state_list[4]->NDL_as_carrier_sense[NL80211_AC_VO] = encode_to_NDL_rx_power(-850);
        database_ptr->NDL_state_list[4]->NDL_as_carrier_sense[NL80211_AC_BE] = encode_to_NDL_rx_power(-850);
        database_ptr->NDL_state_list[4]->NDL_as_carrier_sense[NL80211_AC_BK] = encode_to_NDL_rx_power(-850);
        *_database = database_ptr;
}

int check_dcc_message(unsigned char *toCheck)
{
        if(*toCheck != 'D')
		return 0;
	if(*(toCheck+1) != 'C')
		return 0;
	if(*(toCheck+2) != 'C')
		return 0;
	return 1;
}


int get_actual_state(struct ndlType_ndl_database **_database)
{
	struct ndlType_ndl_database *database_ptr = *_database;
	return database_ptr->NDL_general_configuration->NDL_cur_active_state;
}

void set_actual_state(struct ndlType_ndl_database **_database, int state)
{
	struct ndlType_ndl_database *database_ptr = *_database;
	int i;
	if(state > database_ptr->NDL_general_configuration->NDL_num_active_state+1)
		state = database_ptr->NDL_general_configuration->NDL_num_active_state +1;


	database_ptr->NDL_general_configuration->NDL_cur_active_state = state;


	for(i = 0; i< NL80211_NUM_ACS; i++) {
		database_ptr->NDL_packet_datarate_tresholds->NDL_ref_datarate[i] = database_ptr->NDL_state_list[state]->NDL_as_datarate[i];
		database_ptr->NDL_packet_timing_tresholds->NDL_ref_packet_interval[i] = database_ptr->NDL_state_list[state]->NDL_as_packet_interval[i];
		database_ptr->NDL_transmit_power_tresholds->NDL_ref_tx_power[i] = database_ptr->NDL_state_list[state]->NDL_as_tx_power[i];
		//database_ptr->NDL_receive_signal_tresholds->NDL_ref_carrier_sense = database_ptr->NDL_state_list[state]->NDL_as_carrier_sense[i];
	}

	*_database = database_ptr;
}

int get_air_time(unsigned int pkt_len, unsigned int datarate)
{
        int result = 0;
        result = 5 + ((20 * pkt_len) / datarate);
	result = result * 8;

        return result;
}

int get_index_snr_table(ndlType_snr snr)
{
    int i;
    for(i=0;i<8;i++) {
        if(ndl_SNR_table[i]==snr)
            break;
    }
    if(i>7)
        i =-1;

    return i;
}

int get_index_datarate_table(ndlType_datarate datarate)
{
    int i;
    for(i=0;i<8;i++) {
        if(ndl_datarate_table[i]==datarate)
            break;
    }
    if(i>7)
        i =-1;

    return i;
}


//	#define __KERNEL__

#ifdef __KERNEL__

/**
*
*	Copy limits and default values from global NDL database to local NDL database
*
**/
void dcc_process_NDL_databases(struct ndlType_ndl_database **local_ndl_database, struct ndlType_ndl_database **global_ndl_database)
{
	unsigned long flags;
	int i;
	ndlType_tx_power temp_tx_power;
        ndlType_rx_power temp_rx_power;
        ndlType_packet_duration temp_packet_duration;
        ndlType_packet_interval temp_packet_interval;
        ndlType_datarate temp_datarate;
	ndlType_channel_load temp_channel_load;

        struct ndlType_ndl_database *_local_ndl_database = *local_ndl_database;
	struct ndlType_ndl_database *_global_ndl_database = *global_ndl_database;

	spin_lock_irqsave(&(_local_ndl_database->database_in_use), flags);
	spin_lock_irqsave(&(_global_ndl_database->database_in_use), flags);


        temp_tx_power = _global_ndl_database->NDL_transmit_power_tresholds->NDL_min_tx_power;
	_local_ndl_database->NDL_transmit_power_tresholds->NDL_min_tx_power = temp_tx_power;
        temp_tx_power = _global_ndl_database->NDL_transmit_power_tresholds->NDL_max_tx_power;
	_local_ndl_database->NDL_transmit_power_tresholds->NDL_max_tx_power = temp_tx_power;

	temp_packet_interval = _global_ndl_database->NDL_packet_timing_tresholds->NDL_min_packet_interval;
	_local_ndl_database->NDL_packet_timing_tresholds->NDL_min_packet_interval = temp_packet_interval;
	temp_packet_interval = _global_ndl_database->NDL_packet_timing_tresholds->NDL_max_packet_interval;
	_local_ndl_database->NDL_packet_timing_tresholds->NDL_max_packet_interval = temp_packet_interval;

	temp_datarate = _global_ndl_database->NDL_packet_datarate_tresholds->NDL_min_datarate;
	_global_ndl_database->NDL_packet_datarate_tresholds->NDL_min_datarate = temp_datarate;
	temp_datarate = _global_ndl_database->NDL_packet_datarate_tresholds->NDL_max_datarate;
	_global_ndl_database->NDL_packet_datarate_tresholds->NDL_max_datarate = temp_datarate;

	temp_rx_power = _global_ndl_database->NDL_receive_signal_tresholds->NDL_min_carrier_sense;
	_local_ndl_database->NDL_receive_signal_tresholds->NDL_min_carrier_sense = temp_rx_power;

	temp_rx_power = _global_ndl_database->NDL_receive_signal_tresholds->NDL_max_carrier_sense;
	_local_ndl_database->NDL_receive_signal_tresholds->NDL_max_carrier_sense = temp_rx_power;

	temp_rx_power = _global_ndl_database->NDL_receive_signal_tresholds->NDL_def_carrier_sense;
	_local_ndl_database->NDL_receive_signal_tresholds->NDL_def_carrier_sense = temp_rx_power;

	temp_channel_load = _global_ndl_database->NDL_channel_load_tresholds->NDL_min_channel_load;
	_local_ndl_database->NDL_channel_load_tresholds->NDL_min_channel_load = temp_channel_load;

	temp_channel_load = _global_ndl_database->NDL_channel_load_tresholds->NDL_max_channel_load;
	_local_ndl_database->NDL_channel_load_tresholds->NDL_max_channel_load = temp_channel_load;

	for (i = 0; i< NL80211_NUM_ACS; i++) {

	        temp_tx_power = _global_ndl_database->NDL_transmit_power_tresholds->NDL_def_tx_power[i];
		_local_ndl_database->NDL_transmit_power_tresholds->NDL_def_tx_power[i] = temp_tx_power;

                temp_packet_duration = _global_ndl_database->NDL_packet_timing_tresholds->NDL_max_packet_duration[i];
		_local_ndl_database->NDL_packet_timing_tresholds->NDL_max_packet_duration[i] = temp_packet_duration;

		temp_packet_interval = _global_ndl_database->NDL_packet_timing_tresholds->NDL_def_packet_interval[i];
		_local_ndl_database->NDL_packet_timing_tresholds->NDL_def_packet_interval[i] = temp_packet_interval;

		temp_datarate = _global_ndl_database->NDL_packet_datarate_tresholds->NDL_def_datarate[i];
		_global_ndl_database->NDL_packet_datarate_tresholds->NDL_def_datarate[i] = temp_datarate;
        }

        spin_unlock_irqrestore(&(_local_ndl_database->database_in_use),flags);
        spin_unlock_irqrestore(&(_global_ndl_database->database_in_use),flags);
        *local_ndl_database = _local_ndl_database;
        *global_ndl_database = _global_ndl_database;

	print_debug("Default values from global NDL database cloned into local NDL database successfully.");

}


/**
*
*	Copy reference and actual state values from DCC access local NDL database to global NDL database
*
**/
void dcc_save_ref_values_to_gNDL_database(struct ndlType_ndl_database **local_ndl_database, struct ndlType_ndl_database **global_ndl_database)
{
	unsigned long flags;
	int i;
	struct ndlType_ndl_database *_local_ndl_database = *local_ndl_database;
	struct ndlType_ndl_database *_global_ndl_database = *global_ndl_database;

	spin_lock_irqsave(&(_local_ndl_database->database_in_use), flags);
	spin_lock_irqsave(&(_global_ndl_database->database_in_use), flags);

	_global_ndl_database->NDL_general_configuration->NDL_cur_active_state = _local_ndl_database->NDL_general_configuration->NDL_cur_active_state;

	_global_ndl_database->NDL_receive_signal_tresholds->NDL_ref_carrier_sense = _local_ndl_database->NDL_receive_signal_tresholds->NDL_ref_carrier_sense;

	for (i = 0; i< NL80211_NUM_ACS; i++) {
		_global_ndl_database->NDL_transmit_power_tresholds->NDL_ref_tx_power[i] = _local_ndl_database->NDL_transmit_power_tresholds->NDL_ref_tx_power[i];
		_global_ndl_database->NDL_packet_timing_tresholds->NDL_ref_packet_interval[i] = _local_ndl_database->NDL_packet_timing_tresholds->NDL_ref_packet_interval[i];
                _global_ndl_database->NDL_packet_datarate_tresholds->NDL_ref_datarate[i] = _local_ndl_database->NDL_packet_datarate_tresholds->NDL_ref_datarate[i];
        }

        spin_unlock_irqrestore(&(_local_ndl_database->database_in_use),flags);
        spin_unlock_irqrestore(&(_global_ndl_database->database_in_use),flags);
        *local_ndl_database = _local_ndl_database;
        *global_ndl_database = _global_ndl_database;

	print_debug("Reference values from DCC access local NDL database cloned into global NDL database successfully.");

}

/**
*
*	Copy channel load measures to global NDL database
*
**/
void dcc_save_load_measures_to_gNDL_database(struct ndlType_channel_load_measures **local_channel_load_measures, struct ndlType_ndl_database **global_ndl_database)
{
	unsigned long flags;

        struct ndlType_channel_load_measures *_local_channel_load_measures = *local_channel_load_measures;
	struct ndlType_ndl_database *_global_ndl_database = *global_ndl_database;

	spin_lock_irqsave(&(_global_ndl_database->database_in_use), flags);

        _global_ndl_database->NDL_channel_load_measures->NDL_channel_busy_time = _local_channel_load_measures->NDL_channel_busy_time;
        _global_ndl_database->NDL_channel_load_measures->NDL_channel_load = _local_channel_load_measures->NDL_channel_load;
        _global_ndl_database->NDL_channel_load_measures->NDL_load_arrival_rate= _local_channel_load_measures->NDL_load_arrival_rate;
        _global_ndl_database->NDL_channel_load_measures->NDL_load_avg_duration = _local_channel_load_measures->NDL_load_avg_duration;
        _global_ndl_database->NDL_channel_load_measures->NDL_packet_arrival_rate = _local_channel_load_measures->NDL_packet_arrival_rate;
        _global_ndl_database->NDL_channel_load_measures->NDL_packet_avg_duration = _local_channel_load_measures->NDL_packet_avg_duration;

        spin_unlock_irqrestore(&(_global_ndl_database->database_in_use),flags);


        *global_ndl_database = _global_ndl_database;

	kfree(_local_channel_load_measures);

        *local_channel_load_measures = _local_channel_load_measures;

	print_debug("Measured values of channel load cloned into global NDL database successfully.");
}


/**
*
*	Copy tx stats to global NDL database
*
**/
void dcc_save_tx_stats_to_gNDL_database(struct ndlType_transmit_packet_statictics **local_tx_stats, struct ndlType_ndl_database **global_ndl_database)
{
	unsigned long flags;
	int i;

        struct ndlType_transmit_packet_statictics *_local_tx_stats = *local_tx_stats;
	struct ndlType_ndl_database *_global_ndl_database = *global_ndl_database;

	spin_lock_irqsave(&(_global_ndl_database->database_in_use), flags);

	for (i = 0; i< NL80211_NUM_ACS; i++) {

		_global_ndl_database->NDL_transmit_packet_statistics->NDL_tx_channel_use[i] = _local_tx_stats->NDL_tx_channel_use[i];
		_global_ndl_database->NDL_transmit_packet_statistics->NDL_tx_packet_arrival_rate[i] = _local_tx_stats->NDL_tx_packet_arrival_rate[i];
		_global_ndl_database->NDL_transmit_packet_statistics->NDL_tx_packet_avg_duration[i] = _local_tx_stats->NDL_tx_packet_avg_duration[i];
		_global_ndl_database->NDL_transmit_packet_statistics->NDL_tx_signal_avg_power[i] = _local_tx_stats->NDL_tx_signal_avg_power[i];
	}
        spin_unlock_irqrestore(&(_global_ndl_database->database_in_use),flags);


        *global_ndl_database = _global_ndl_database;

        kfree(_local_tx_stats->NDL_tx_channel_use);
	kfree(_local_tx_stats->NDL_tx_packet_arrival_rate);
	kfree(_local_tx_stats->NDL_tx_packet_avg_duration);
	kfree(_local_tx_stats->NDL_tx_signal_avg_power);
	kfree(_local_tx_stats);

        *local_tx_stats = _local_tx_stats;

	print_debug("Measured values of tx stats cloned into global NDL database successfully.");
}


/**
*
*	Copy communication ranges to global NDL database
*
**/
void dcc_save_comm_ranges_to_gNDL_database(struct ndlType_communication_ranges **local_comm_ranges, struct ndlType_ndl_database **global_ndl_database)
{
	unsigned long flags;

        struct ndlType_communication_ranges *_local_comm_ranges = *local_comm_ranges;
	struct ndlType_ndl_database *_global_ndl_database = *global_ndl_database;

	spin_lock_irqsave(&(_global_ndl_database->database_in_use), flags);

	_global_ndl_database->NDL_communication_ranges->NDL_carrier_sense_range = _local_comm_ranges->NDL_carrier_sense_range;
	_global_ndl_database->NDL_communication_ranges->NDL_est_comm_range = _local_comm_ranges->NDL_est_comm_range;

	_global_ndl_database->NDL_communication_ranges->NDL_est_comm_range_intf = _local_comm_ranges->NDL_est_comm_range_intf;

        spin_unlock_irqrestore(&(_global_ndl_database->database_in_use),flags);


        *global_ndl_database = _global_ndl_database;

	kfree(_local_comm_ranges);

        *local_comm_ranges = _local_comm_ranges;

	print_debug("Measured values of communication ranges cloned into global NDL database successfully.");
}

/**
*
*	Copy values from global DP table to local DP table
*
**/
void dcc_process_DP_tables(struct dpType_dp_table **local_dp_table, struct dpType_dp_table **global_dp_table)
{
	unsigned long flags;
	int ntx;

        struct dpType_dp_table *_local_dp_table = *local_dp_table;
	struct dpType_dp_table *_global_dp_table = *global_dp_table;

	spin_lock_irqsave(&(_local_dp_table->table_in_use), flags);
	spin_lock_irqsave(&(_global_dp_table->table_in_use), flags);

	for(ntx = 0; ntx<32; ntx++) {
                _local_dp_table->active[ntx]->dp_not_available = _global_dp_table->active[ntx]->dp_not_available;
                _local_dp_table->active[ntx]->id = _global_dp_table->active[ntx]->id;
                _local_dp_table->active[ntx]->num_queueu = _global_dp_table->active[ntx]->num_queueu;
                _local_dp_table->active[ntx]->t_off = _global_dp_table->active[ntx]->t_off;

		_local_dp_table->relaxed[ntx]->dp_not_available = _global_dp_table->relaxed[ntx]->dp_not_available;
                _local_dp_table->relaxed[ntx]->id = _global_dp_table->relaxed[ntx]->id;
                _local_dp_table->relaxed[ntx]->num_queueu = _global_dp_table->relaxed[ntx]->num_queueu;
                _local_dp_table->relaxed[ntx]->t_off = _global_dp_table->relaxed[ntx]->t_off;

		_local_dp_table->restrictive[ntx]->dp_not_available = _global_dp_table->restrictive[ntx]->dp_not_available;
                _local_dp_table->restrictive[ntx]->id = _global_dp_table->restrictive[ntx]->id;
                _local_dp_table->restrictive[ntx]->num_queueu = _global_dp_table->restrictive[ntx]->num_queueu;
                _local_dp_table->restrictive[ntx]->t_off = _global_dp_table->restrictive[ntx]->t_off;
	}

        spin_unlock_irqrestore(&(_local_dp_table->table_in_use),flags);
        spin_unlock_irqrestore(&(_global_dp_table->table_in_use),flags);
        *local_dp_table= _local_dp_table;
        *global_dp_table= _global_dp_table;

	print_debug("Default values from global DP table cloned into local DP table successfully.");

}

#endif // __KERNEL__

