#ifndef DCC_ACCESS_H_INCLUDED
#define DCC_ACCESS_H_INCLUDED


int init_dcc_module(void);
void cleanup_dcc_module(void);

static int init_memory(int _mode);
static int destroy_memory(void);
static int init_control_loop(void);
static int destroy_control_loop(void);
static int init_queues(void);
static int destroy_queues(void);


#endif // DCC_ACCESS_H_INCLUDED
