
#include <linux/module.h>
#include <linux/slab.h>
#include <linux/types.h>
#include <linux/kernel.h>
#include <linux/skbuff.h>
#include <net/pkt_sched.h>

#include <linux/ip.h>
#include <net/ip.h>

#define DCC_PREFIX	"DCC QUEUE"

#define DCC_DEBUG

#include "dcc_queue.h"
#include "dcc_cross.h"
#include "dcc_err.h"

extern struct ndlType_ndl_database *local_ndl_database;
//extern struct dpType_dp_table *local_dp_state_cch;
extern struct dpType_dp_table *local_dp_state_sch;

extern unsigned int **tx_packets;
extern unsigned int **t_air_of_packets;
extern int stats_array_index;
extern spinlock_t stats_tables_in_use;
extern int ndl_datarate_table[];


struct dcc_qdisc_data {
	u16 bands;
	u16 max_bands;
	struct timeval **pkt_arrival_time;
	struct Qdisc **queues;
};


static struct Qdisc *dcc_qdisc_classify(struct sk_buff *skb, struct Qdisc *sch, int *qerr)
{
	struct dcc_qdisc_data *priv = qdisc_priv(sch);
	struct dcc_msg_data *dcc_data;
	struct dpType_dcc_profile ** temp_profile;
	unsigned char *data;
	unsigned long flags;
	int datarate = 30;
	int state_id;
	unsigned int t_air;
	struct timeval actual_time, *old_time;
	struct iphdr *iph;
	int temp = 0;
	int temp1 = 0;
	int data_len = 0;
	u8 band = 3;

	*qerr = NET_XMIT_SUCCESS; // | __NET_XMIT_BYPASS;

// handle packages

        if(skb->protocol != htons(ETH_P_IP)) // ETH_P_IPV6
		return priv->queues[3];

	if(check_dcc_message((skb->data)+42) == 0) {
		//*qerr = NET_XMIT_DROP;
		return priv->queues[3];
        }


	actual_time = ns_to_timeval(skb->tstamp.tv64);


        iph = ip_hdr(skb);

        if(iph->tos == NDL_IP_COS1 || iph->tos == NDL_IP_COS2)
		band = 3;
	if(iph->tos == NDL_IP_COS0 || iph->tos == NDL_IP_COS3)
		band = 2;
	if(iph->tos == NDL_IP_COS4 || iph->tos == NDL_IP_COS5)
		band = 1;
	if(iph->tos == NDL_IP_COS6 || iph->tos == NDL_IP_COS7)
		band = 0;





	dcc_data = kmalloc(sizeof(struct dcc_msg_data), GFP_ATOMIC);
	data_len = 0;

	data = (skb->data+42);

	memcpy(&dcc_data->dcc_mark, data, 4);
	data_len += 4;

	memcpy(&dcc_data->msg_type, data+data_len, sizeof(unsigned char));
	data_len += sizeof(unsigned char);

	//memcpy(&dcc_data->cmd_ref, data+data_len, sizeof(unsigned char));

	memcpy(&dcc_data->cmd_ref, data+data_len, sizeof(unsigned char));
	data_len += sizeof(unsigned char);

	memcpy(&dcc_data->dcc_profile, data+data_len, sizeof(unsigned char));
	data_len += sizeof(unsigned char);

	memcpy(&dcc_data->tx_power, data+data_len, sizeof(unsigned int));
	data_len += sizeof(unsigned int);

	memcpy(&dcc_data->mcs, data+data_len, sizeof(unsigned char));
	data_len += sizeof(unsigned char);

	memcpy(&dcc_data->cs_range, data+data_len, sizeof(unsigned int));
	data_len += sizeof(unsigned int);

	memcpy(&dcc_data->est_comm_range, data+data_len, sizeof(unsigned int));
	data_len += sizeof(unsigned int);

	memcpy(&dcc_data->est_comm_range_inf, data+data_len, sizeof(unsigned int));
	data_len += sizeof(unsigned int);

	memcpy(&dcc_data->data_len, data+data_len, sizeof(unsigned int));
	data_len += sizeof(unsigned int);


	//calculate datarate and make DCC mechanisms to run

	spin_lock_irqsave(&(local_ndl_database->database_in_use), flags);
	state_id = get_actual_state(&local_ndl_database);


	if(state_id== 0) {
		temp_profile = local_dp_state_sch->relaxed;
	}else {
		if(local_ndl_database->NDL_general_configuration->NDL_num_active_state==1)
		{
			if(state_id== 1)
				temp_profile = local_dp_state_sch->active;
			else
				temp_profile = local_dp_state_sch->restrictive;
		}else
		{
			if(state_id== 5)
				temp_profile = local_dp_state_sch->restrictive;
			else
				temp_profile = local_dp_state_sch->active;
		}
	}



	band = (temp_profile[dcc_data->dcc_profile]->num_queueu)-1;
	if(band < 0)
		band = 0;

	spin_unlock_irqrestore(&(local_ndl_database->database_in_use),flags);




	print_debug("TOS field -> %d, queue selected -> %d",iph->tos, band);
	old_time = priv->pkt_arrival_time[band];

	temp = (actual_time.tv_sec - old_time->tv_sec) * 1000;
	temp1 = (actual_time.tv_usec - old_time->tv_usec) / 1000;

        temp = temp + temp1;


	memcpy(old_time, &actual_time, sizeof(struct timeval));

	if(temp_profile[dcc_data->dcc_profile]->dp_not_available == 1) {	//drop packet
		print_error("Packet has been dropped.");
		*qerr = NET_XMIT_DROP;
		return priv->queues[3];
        }

        temp1 = temp_profile[dcc_data->dcc_profile]->t_off;

        print_debug("Minimum packet arrival rate is %dms, actual is %dms", temp1, temp);

	if(temp1>temp) {	//drop packet
		print_error("Packet arrival rate is too high.");
		*qerr = NET_XMIT_DROP;
		return priv->queues[3];
        }



	t_air = temp_profile[dcc_data->dcc_profile]->t_off;


	spin_lock_irqsave(&(local_ndl_database->database_in_use), flags);
	temp =  local_ndl_database->NDL_packet_datarate_tresholds->NDL_ref_datarate[band];

	//TDC
	if (temp > dcc_data->mcs)
		dcc_data->mcs = temp;

	temp =  local_ndl_database->NDL_transmit_power_tresholds->NDL_ref_tx_power[band];
	//TPC
	if (temp < encode_to_NDL_tx_power(dcc_data->tx_power))
		dcc_data->tx_power = decode_from_NDL_tx_power(temp);

	spin_unlock_irqrestore(&(local_ndl_database->database_in_use),flags);



	datarate = ndl_datarate_table[dcc_data->mcs];

	t_air = get_air_time(skb->len,datarate);


	spin_lock_irqsave(&stats_tables_in_use, flags);

	tx_packets[stats_array_index][band] += 1;
	t_air_of_packets[stats_array_index][band] += t_air;

        spin_unlock_irqrestore(&stats_tables_in_use,flags);

	spin_lock_irqsave(&(local_ndl_database->database_in_use), flags);
	local_ndl_database->NDL_communication_ranges->NDL_carrier_sense_range = dcc_data->cs_range;
	local_ndl_database->NDL_communication_ranges->NDL_est_comm_range= dcc_data->est_comm_range;
	local_ndl_database->NDL_communication_ranges->NDL_est_comm_range_intf= dcc_data->est_comm_range_inf;
	spin_unlock_irqrestore(&(local_ndl_database->database_in_use),flags);

	if (band >= priv->bands)
		return priv->queues[1];

	return priv->queues[band];
}

static int dcc_qdisc_enqueue(struct sk_buff *skb, struct Qdisc *sch)
{
	struct Qdisc *qdisc;
	int ret = 0;

	qdisc = dcc_qdisc_classify(skb, sch, &ret);

	if(ret == NET_XMIT_DROP) {

		qdisc_qstats_drop(qdisc);
		qdisc_qstats_drop(sch);
		return ret;
	}

	ret = qdisc_enqueue(skb, qdisc);
	if (ret == NET_XMIT_SUCCESS) {
		sch->q.qlen++;
		return NET_XMIT_SUCCESS;
	}
	if (net_xmit_drop_count(ret))
		qdisc_qstats_drop(sch);
	return ret;
}

static struct sk_buff *dcc_qdisc_dequeue(struct Qdisc *sch)
{
	struct dcc_qdisc_data *priv = qdisc_priv(sch);
	struct Qdisc *qdisc;
	struct sk_buff *skb;
	int ntx;

	for (ntx = 0; ntx < priv->bands; ntx++) {
		qdisc = priv->queues[ntx];
                skb = qdisc_dequeue_peeked(qdisc);
		if (skb) {
			qdisc_bstats_update(sch, skb);
			sch->q.qlen--;
			return skb;
		}
	}
	return NULL;

}

static struct sk_buff *dcc_qdisc_peek(struct Qdisc *sch)
{
	struct dcc_qdisc_data *priv = qdisc_priv(sch);
	struct Qdisc *qdisc;
	struct sk_buff *skb;
	int ntx;

	for (ntx = 0; ntx < priv->bands; ntx++) {
		qdisc = priv->queues[ntx];
                skb = qdisc->ops->peek(qdisc);
		if (skb) {
			return skb;
		}
	}
	return NULL;
}

static unsigned int dcc_qdisc_drop(struct Qdisc *sch)
{
	struct dcc_qdisc_data *priv = qdisc_priv(sch);
	unsigned int len;
	struct Qdisc *qdisc;
	int ntx;

	for (ntx = priv->bands - 1; ntx >= 0; ntx--) {
		qdisc = priv->queues[ntx];
		if (qdisc->ops->drop) {
			len = qdisc->ops->drop(qdisc);
			if (len != 0) {
				sch->q.qlen--;
				return len;
			}
		}
	}
	return 0;
}


static void dcc_qdisc_reset(struct Qdisc *sch)
{
	int ntx;
	struct dcc_qdisc_data *priv = qdisc_priv(sch);

	for (ntx = 0; ntx < priv->bands; ntx++)
		qdisc_reset(priv->queues[ntx]);
	sch->q.qlen = 0;
}

static void dcc_qdisc_destroy(struct Qdisc *sch)
{
        struct dcc_qdisc_data *priv = qdisc_priv(sch);
	unsigned int ntx;

        if(!priv->queues)
		return;
	sch_tree_lock(sch);
	for (ntx = 0; ntx < priv->bands; ntx++) {
                qdisc_destroy(priv->queues[ntx]);
	}
	sch_tree_unlock(sch);
	kfree(priv->queues);
}

static int dcc_qdisc_tune(struct Qdisc *sch, struct nlattr *arg)
{
	struct dcc_qdisc_data *priv = qdisc_priv(sch);
//	struct net_device *dev = qdisc_dev(sch);
	struct tc_multiq_qopt *qopt;
	int ntx;

//	if (!netif_is_multiqueue(dev))
//		return -EOPNOTSUPP;

	qopt = kcalloc(1, sizeof(struct tc_multiq_qopt), GFP_ATOMIC);

	qopt->max_bands = 4;
	qopt->bands = 4;

	sch_tree_lock(sch);
	priv->bands = qopt->bands;

	for (ntx = priv->bands; ntx < priv->max_bands; ntx++) {
		if (priv->queues[ntx] != &noop_qdisc) {
			struct Qdisc *child = priv->queues[ntx];
			priv->queues[ntx] = &noop_qdisc;
			qdisc_tree_decrease_qlen(child, child->q.qlen);
			qdisc_destroy(child);
		}
	}
	sch->flags &= ~TCQ_F_CAN_BYPASS;
	sch_tree_unlock(sch);

	for (ntx = 0; ntx < priv->bands; ntx++) {
		if (priv->queues[ntx] == &noop_qdisc) {
			struct Qdisc *child, *old;
			child = qdisc_create_dflt(sch->dev_queue , &pfifo_qdisc_ops,  TC_H_MAKE(sch->handle, ntx + 1));


			if (child) {
				fifo_set_limit(child,8);
				child->flags &= ~TCQ_F_CAN_BYPASS;

				sch_tree_lock(sch);
				old = priv->queues[ntx];
				priv->queues[ntx] = child;

				if (old != &noop_qdisc) {
					qdisc_tree_decrease_qlen(old,
								 old->q.qlen);
					qdisc_destroy(old);
				}
				sch_tree_unlock(sch);
			}
		}
	}
	return 0;
}

static int dcc_qdisc_init(struct Qdisc *sch, struct nlattr *opt)
{
	struct dcc_qdisc_data *priv = qdisc_priv(sch);
//	struct net_device *dev = qdisc_dev(sch);
	unsigned int ntx, err;

	if (sch->parent != TC_H_ROOT)
		return -EOPNOTSUPP;

//	if (!netif_is_multiqueue(dev))
//		return -EOPNOTSUPP;

	priv->queues = NULL;
	priv->bands = 4;

	priv->queues = kcalloc(priv->bands, sizeof(struct Qdisc *), GFP_ATOMIC);
	priv->pkt_arrival_time = kcalloc(priv->bands, sizeof(struct timeval *), GFP_ATOMIC);
        if (priv->queues == NULL || priv->pkt_arrival_time == NULL)
		return -ENOMEM;

	for (ntx = 0; ntx < priv->bands; ntx++) {
		priv->queues[ntx] = &noop_qdisc;
		priv->pkt_arrival_time[ntx] = kcalloc(1, sizeof(struct timeval), GFP_ATOMIC);
	}
	err = dcc_qdisc_tune(sch, NULL);

	if (err)
		kfree(priv->queues);
	return err;
}

struct Qdisc_ops dcc_qdisc_ops __read_mostly = {
	.next		=	NULL,
	.id		=	"dcc_queue",
	.priv_size	=	sizeof(struct dcc_qdisc_data),
	.enqueue	=	dcc_qdisc_enqueue,
	.dequeue	=	dcc_qdisc_dequeue,
	.peek		=	dcc_qdisc_peek,
	.drop		=	dcc_qdisc_drop,
	.init		=	dcc_qdisc_init,
	.reset		=	dcc_qdisc_reset,
	.destroy	=	dcc_qdisc_destroy,
	.change		=	dcc_qdisc_tune,
	.owner		=	THIS_MODULE,
};




