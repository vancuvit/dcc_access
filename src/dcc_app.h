#ifndef DCC_APP_H_INCLUDED
#define DCC_APP_H_INCLUDED

#include "dcc_cross.h"
#include "dcc_err.h"

int main();


int send_ndl_database_attributes(struct ndlType_ndl_database **_database);
int send_rx_stats(struct ndlType_channel_load_measures **ch_load_measures);
int receive_ndl_database_attributes();
int receive_ndl_database_attributes_handler(struct nl_msg *_msg, void * arg);



#endif // DCC_APP_H_INCLUDED
