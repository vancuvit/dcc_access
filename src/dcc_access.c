#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/sched.h>
#include <linux/errno.h>
#include <linux/string.h>
#include <linux/tty.h>
#include <linux/kd.h>
#include <linux/vt.h>
#include <linux/console_struct.h>
#include <linux/skbuff.h>
#include <net/net_namespace.h>
#include <linux/spinlock.h>
#include <linux/kthread.h>
#include <linux/kfifo.h>
#include <linux/netfilter.h>
#include <linux/netfilter_ipv4.h>

#define DCC_DEBUG
#define DCC_PREFIX	"DCC ACCESS"
#define Q_SIZE	1024

#include "dcc_cross.h"
#include "dcc_access.h"
#include "dcc_err.h"
#include "dcc_queue.h"

MODULE_AUTHOR ("Vit Vancura <vancuvit@fel.cvut.cz>, CTU in Prague");
MODULE_DESCRIPTION ("DCC_access support module of part 802.11p");
MODULE_LICENSE("GPL");


extern struct Qdisc_ops dcc_qdisc_ops __read_mostly;
extern int register_qdisc(struct Qdisc_ops *qops);
extern int unregister_qdisc(struct Qdisc_ops *qops);

extern int init_net_module(void);
extern void cleanup_net_module(void);

extern struct nf_hook_ops dcc_nf_hook_out;
extern struct nf_hook_ops dcc_nf_hook_in;

static int mode = G5CC;
module_param(mode, int, 0);
MODULE_PARM_DESC(mode, "This flag controls initial setup. Set 0 for G5CC, 1 for G5SC.");

struct ndlType_ndl_database *local_ndl_database;
//struct dpType_dchannel_state *local_dp_state_cch;
struct dpType_dp_table *local_dp_state_sch;

static struct task_struct *control_loop;

extern struct kfifo dcc_miq_data;
extern struct kfifo dcc_imq_data;


unsigned int **tx_packets;
unsigned int **t_air_of_packets;
int stats_array_index;
spinlock_t stats_tables_in_use;



static int init_memory(int _mode)
{
        int _status = 0;
        int i = 0;
        unsigned long flags;
        _status |= init_NDL_memory(&local_ndl_database);
        //_status |= init_DP_table(&local_dp_state_cch);
        _status |= init_DP_table(&local_dp_state_sch);

        spin_lock_irqsave(&(local_ndl_database->database_in_use), flags);
        _status |= set_NDL_default_values(&local_ndl_database, _mode);
        spin_unlock_irqrestore(&(local_ndl_database->database_in_use),flags);


	spin_lock_init(&stats_tables_in_use);
	stats_array_index = 0;
	spin_lock_irqsave(&stats_tables_in_use, flags);
        tx_packets = kmalloc(10* sizeof(unsigned int *),GFP_ATOMIC);
	t_air_of_packets= kmalloc(10* sizeof(unsigned int *),GFP_ATOMIC);

	for(i =0 ;i<10; i++) {
		tx_packets[i] = kmalloc(4* sizeof(unsigned int),GFP_ATOMIC);
		t_air_of_packets[i] = kmalloc(4* sizeof(unsigned int),GFP_ATOMIC);
	}
        spin_unlock_irqrestore(&stats_tables_in_use,flags);


        //spin_lock_irqsave(&(local_dp_state_cch->table_in_use), flags);
        //_status |= set_default_values_DP_cch(&local_dp_state_cch);
        //spin_unlock_irqrestore(&(local_dp_state_cch->table_in_use),flags);

        spin_lock_irqsave(&(local_dp_state_sch->table_in_use), flags);
        _status |= set_default_values_DP_sch(&local_dp_state_sch);
        spin_unlock_irqrestore(&(local_dp_state_sch->table_in_use),flags);

        return _status;
}


static int destroy_memory()
{
        int _status = 0;
        destroy_NDL_memory(&local_ndl_database);
        //destroy_DP_table(&local_dp_state_cch);
        destroy_DP_table(&local_dp_state_sch);
	return _status;
}


static void send_channel_load_measures_to_gNDL(int index)
{
	unsigned long flags;
	struct ndlType_channel_load_measures *channel_load_measures = kmalloc( sizeof(struct ndlType_channel_load_measures),GFP_ATOMIC);

	spin_lock_irqsave(&(local_ndl_database->database_in_use), flags);

	channel_load_measures->msg_type = msg_NDL_CHANNEL_LOAD_MEASURES;
	channel_load_measures->NDL_channel_busy_time = local_ndl_database->NDL_channel_load_measures->NDL_channel_busy_time;
	channel_load_measures->NDL_channel_load = local_ndl_database->NDL_channel_load_measures->NDL_channel_load;
	channel_load_measures->NDL_load_arrival_rate = local_ndl_database->NDL_channel_load_measures->NDL_load_arrival_rate;
	channel_load_measures->NDL_load_avg_duration = local_ndl_database->NDL_channel_load_measures->NDL_load_avg_duration;
	channel_load_measures->NDL_packet_arrival_rate = local_ndl_database->NDL_channel_load_measures->NDL_packet_arrival_rate;
	channel_load_measures->NDL_packet_avg_duration = local_ndl_database->NDL_channel_load_measures->NDL_packet_avg_duration;

        spin_unlock_irqrestore(&(local_ndl_database->database_in_use),flags);

	kfifo_in(&dcc_imq_data, &channel_load_measures, sizeof(struct ndlType_channel_load_measures *));
}

static void send_comm_ranges_to_gNDL(int index)
{
	unsigned long flags;
	struct ndlType_communication_ranges *comm_ranges = kmalloc( sizeof(struct ndlType_communication_ranges),GFP_ATOMIC);

	spin_lock_irqsave(&(local_ndl_database->database_in_use), flags);
	comm_ranges->msg_type = msg_NDL_COMM_RANGES;
	comm_ranges->NDL_carrier_sense_range = local_ndl_database->NDL_communication_ranges->NDL_carrier_sense_range;
	comm_ranges->NDL_est_comm_range = local_ndl_database->NDL_communication_ranges->NDL_est_comm_range;
	comm_ranges->NDL_est_comm_range_intf = local_ndl_database->NDL_communication_ranges->NDL_est_comm_range_intf;
        spin_unlock_irqrestore(&(local_ndl_database->database_in_use),flags);

	kfifo_in(&dcc_imq_data, &comm_ranges, sizeof(struct ndlType_communication_ranges *));
}

static void send_tx_stats_to_gNDL(int index)
{
	unsigned long flags;
	int i;
	struct ndlType_transmit_packet_statictics *tx_stats = kmalloc( sizeof(struct ndlType_transmit_packet_statictics),GFP_ATOMIC);

	spin_lock_irqsave(&(local_ndl_database->database_in_use), flags);
	tx_stats->msg_type = msg_NDL_TRANSMIT_PCK_STATS;

	tx_stats->NDL_tx_channel_use = kmalloc(NL80211_NUM_ACS * sizeof(ndlType_channel_use),GFP_ATOMIC);
	tx_stats->NDL_tx_packet_arrival_rate = kmalloc(NL80211_NUM_ACS * sizeof(ndlType_arrival_rate),GFP_ATOMIC);
	tx_stats->NDL_tx_packet_avg_duration = kmalloc(NL80211_NUM_ACS * sizeof(ndlType_packet_duration),GFP_ATOMIC);
	tx_stats->NDL_tx_signal_avg_power= kmalloc(NL80211_NUM_ACS * sizeof(ndlType_tx_power),GFP_ATOMIC);

	for (i = 0; i< NL80211_NUM_ACS; i++) {
		tx_stats->NDL_tx_channel_use[i] = local_ndl_database->NDL_transmit_packet_statistics->NDL_tx_channel_use[i];
		tx_stats->NDL_tx_packet_arrival_rate[i] = local_ndl_database->NDL_transmit_packet_statistics->NDL_tx_packet_arrival_rate[i];
		tx_stats->NDL_tx_packet_avg_duration[i] = local_ndl_database->NDL_transmit_packet_statistics->NDL_tx_packet_avg_duration[i];
		tx_stats->NDL_tx_signal_avg_power[i] = local_ndl_database->NDL_transmit_packet_statistics->NDL_tx_signal_avg_power[i];
	}

        spin_unlock_irqrestore(&(local_ndl_database->database_in_use),flags);

	kfifo_in(&dcc_imq_data, &tx_stats, sizeof(struct ndlType_transmit_packet_statictics *));
}



/**
*
*       Exchange data with global management entity
*
**/
static void dcc_process_local_data(u8 index)
{
	void *tempStruct;
	struct ndlType_ndl_database *database_ptr;
	struct dpType_dp_table *dp_table_ptr;
	struct ndlType_channel_load_measures *channel_load_measures_ptr;

	//prepared to use global array and index
	struct ndlType_ndl_database *_local_ndl_database = local_ndl_database;
	struct dpType_dp_table *_local_dp_state_sch = local_dp_state_sch;

	int temp = 0;
	int _status = 0;
//if
	while(!kfifo_is_empty(&dcc_miq_data)) {
		_status = kfifo_out(&dcc_miq_data, &tempStruct, sizeof(void *));
		temp = *((unsigned char*)tempStruct);
			switch (temp) {
		case msg_NDL_DATABASE:
			database_ptr = (struct ndlType_ndl_database *)tempStruct;
			dcc_process_NDL_databases(&_local_ndl_database, &database_ptr);
			dcc_save_ref_values_to_gNDL_database(&_local_ndl_database, &database_ptr);
			break;
		case msg_DP_TABLE:
			dp_table_ptr = (struct dpType_dp_table *)tempStruct;
			dcc_process_DP_tables(&_local_dp_state_sch, &dp_table_ptr);

			break;
		case msg_NDL_CHANNEL_LOAD_MEASURES:
			channel_load_measures_ptr = (struct ndlType_channel_load_measures *)tempStruct;
			dcc_save_load_measures_to_gNDL_database(&channel_load_measures_ptr, &local_ndl_database);
			break;
		case msg_MI_COMMAND_REQ:
				break;
		case msg_MI_COMMAND_CONF:
				break;
		case msg_MI_REQUEST_REQ:
				break;
		case msg_MI_REQUEST_CONF:
				break;
		case msg_MI_SET_REQ:
				break;
		case msg_MI_SET_CONF:
				break;
		case msg_MI_GET_REQ:
				break;
			case msg_MI_GET_CONF:
				break;
		}
	}
}

int t_loop(void *arg)
{
	struct ndlType_ndl_database *_local_ndl_database = arg;
	int state_id = 0;
	int i = 0;
	int next_state_id = 0;
	int temp = 0;
	int sleep_time = 1000;
	int channel_load_below;
	int channel_load_above;
	int cnt = 0;
	unsigned long flags = 0;
	int ref_CS = 0;


        while (!kthread_should_stop())
	{
		sleep_time = _local_ndl_database->NDL_general_configuration->NDL_min_dcc_sampling;


		if(cnt*sleep_time>=1000)
		{

			spin_lock_irqsave(&stats_tables_in_use, flags);


                        for (i = 0; i< 4; i++)
			{

				int j = 0;
				int pck_sum = 0;
                                int t_air_sum = 0;
				for( j = 0; j< 10; j++)
				{

                                        pck_sum += tx_packets[j][i];
					t_air_sum += t_air_of_packets[j][i];

				}
				local_ndl_database->NDL_transmit_packet_statistics->NDL_tx_channel_use[i] = encode_to_NDL_channel_use((pck_sum/10 + t_air_sum/10)*1000);
				local_ndl_database->NDL_transmit_packet_statistics->NDL_tx_packet_arrival_rate[i] = encode_to_NDL_arrival_rate(pck_sum*100);
				local_ndl_database->NDL_transmit_packet_statistics->NDL_tx_packet_avg_duration[i] = t_air_sum/10;
			}

			temp = local_ndl_database->NDL_transmit_packet_statistics->NDL_tx_channel_use[0];
			local_ndl_database->NDL_transmit_packet_statistics->NDL_tx_channel_use[1] += temp;



			stats_array_index++;
                        if(stats_array_index>=10)
				stats_array_index = 0;

			spin_unlock_irqrestore(&stats_tables_in_use,flags);


			kfifo_in(&dcc_imq_data, &_local_ndl_database, sizeof(struct ndlType_ndl_database *));

			send_channel_load_measures_to_gNDL(0);
			send_comm_ranges_to_gNDL(0);
			send_tx_stats_to_gNDL(0);
                        cnt = 0;
		}


                if(local_ndl_database->NDL_channel_load_measures->NDL_channel_load != ref_CS) {
			ref_CS = local_ndl_database->NDL_channel_load_measures->NDL_channel_load;

			state_id = get_actual_state(&local_ndl_database);
			next_state_id = state_id;

			channel_load_above = local_ndl_database->NDL_state_list[state_id]->NDL_as_chan_load;

			if(ref_CS > channel_load_above && next_state_id <= local_ndl_database->NDL_general_configuration->NDL_num_active_state) {
				next_state_id++;

				spin_lock_irqsave(&(local_ndl_database->database_in_use), flags);
				set_actual_state(&local_ndl_database,next_state_id);
        			spin_unlock_irqrestore(&(local_ndl_database->database_in_use),flags);

                                print_debug("i2 %d",next_state_id);
                        }

                        next_state_id = state_id;
			if(next_state_id>=1)
				next_state_id--;

			channel_load_below = local_ndl_database->NDL_state_list[next_state_id]->NDL_as_chan_load;

			if(ref_CS < channel_load_below) {

				spin_lock_irqsave(&(local_ndl_database->database_in_use), flags);
				set_actual_state(&local_ndl_database, next_state_id);
				spin_unlock_irqrestore(&(local_ndl_database->database_in_use),flags);

                        }

                        state_id = get_actual_state(&local_ndl_database);

			print_debug("ActualCS is %d, actual state %d",ref_CS, state_id);

                }



		// receive data from management
		dcc_process_local_data(0);

		cnt++;
		//print_info("sleep %d", sleep_time);
                msleep(sleep_time);
	}
	return 0;
}

static int init_control_loop()
{
        int _status = 0;

        control_loop = kthread_create(t_loop, local_ndl_database,"DCC_ACCESS_CL"); // instead of NULL can be pushed attribute
        wake_up_process(control_loop);

        return _status;
}


static int destroy_control_loop()
{
        int _status = 0;
        kthread_stop(control_loop);
        return _status;
}

static int init_queues()
{
	int _status = 0;

        _status |= kfifo_alloc(&dcc_imq_data, Q_SIZE, GFP_KERNEL);
        _status |= kfifo_alloc(&dcc_miq_data, Q_SIZE, GFP_KERNEL);

	return _status;
}

static int destroy_queues()
{
	kfifo_free(&dcc_imq_data);
        kfifo_free(&dcc_miq_data);
        return 0;
}

int init_dcc_module(void)
{
        int _status = 0;

	_status |= init_net_module();
        _status |= init_memory(mode);
        _status |= init_queues();
        _status |= init_control_loop();
        _status |= register_qdisc(&dcc_qdisc_ops);


        print_info("DCC_access support module loaded successfully!");
        return _status;
}


void cleanup_dcc_module(void)
{
        destroy_control_loop();
        cleanup_net_module();
        destroy_queues();
        destroy_memory();

	unregister_qdisc(&dcc_qdisc_ops);

        print_info("DCC_access support module unloaded successfully!");
}

module_init(init_dcc_module);
module_exit(cleanup_dcc_module);


