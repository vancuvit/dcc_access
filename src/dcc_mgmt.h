#ifndef DCC_MGMT_H_INCLUDED
#define DCC_MGMT_H_INCLUDED

int init_dcc_module(void);
void cleanup_dcc_module(void);

static int init_memory(int _mode);
static int destroy_memory(void);
static int init_control_loop(void);
static int destroy_control_loop(void);


#endif // DCC_MGMT_H_INCLUDED
