#ifndef DCC_ERR_H_INCLUDED
#define DCC_ERR_H_INCLUDED

#ifndef __KERNEL__
#include <stdio.h>
#include <errno.h>
#include <string.h>


#ifdef DCC_DEBUG
#define print_debug(M, ...)	fprintf(stderr, "%s: [DEBUG] %s:%d: " M "\n", DCC_PREFIX, __FILE__, __LINE__, ##__VA_ARGS__)
#else
#define print_debug(M, ...)
#endif // DCC_DEBUG

#define print_info(M, ...)	fprintf(stderr, "%s: " M "\n", DCC_PREFIX, ##__VA_ARGS__)
#define print_error(M, ...)	fprintf(stderr, "%s: [ERROR] %s:%d: " M "\n", DCC_PREFIX, __FILE__, __LINE__, ##__VA_ARGS__)
#define print_warning(M, ...)	fprintf(stderr, "%s: [WARNING] %s:%d: " M "\n", DCC_PREFIX, __FILE__, __LINE__, ##__VA_ARGS__)


#else
#include <linux/errno.h>
#include <linux/string.h>

#ifdef DCC_DEBUG
#define print_debug(M, ...)	printk(KERN_DEBUG "%s: [DEBUG] %s:%d: " M "\n", DCC_PREFIX, __FILE__, __LINE__, ##__VA_ARGS__)
#else
#define print_debug(M, ...)
#endif // DCC_DEBUG
#define print_info(M, ...)	printk(KERN_INFO "%s: " M "\n", DCC_PREFIX, ##__VA_ARGS__)
#define print_error(M, ...)	printk(KERN_ERR "%s: [ERROR] %s:%d: " M "\n", DCC_PREFIX, __FILE__, __LINE__, ##__VA_ARGS__)
#define print_warning(M, ...)	printk(KERN_WARNING "%s: [WARNING] %s:%d: " M "\n", DCC_PREFIX, __FILE__, __LINE__, ##__VA_ARGS__)


#endif

#define check(A, M, ...)	if(!(A)) { print_error(M, ##__VA_ARGS__); }
#define check_mem(A)		check((A), "Memory is not allocated properly!")

#endif // DCC_ERR_H_INCLUDED
