#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>
#include <sys/socket.h>
#include <errno.h>
#include <unistd.h>
#include <poll.h>
#include <fcntl.h>
#include <signal.h>
#include <pthread.h>
#include <math.h>
#include <time.h>
#include <netinet/if_ether.h>
#include <netinet/in.h>
#include <pcap.h>

#include <linux/genetlink.h>
#include <libnl3/netlink/netlink.h>
#include <libnl3/netlink/socket.h>
#include <libnl3/netlink/genl/genl.h>
#include <libnl3/netlink/genl/ctrl.h>
#include <libnl3/netlink/msg.h>
#include <linux/taskstats.h>
#include <libnl3/netlink/attr.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#define DCC_DEBUG
#define DCC_PREFIX	"DCC APP"
#define MEASURE_PERIOD	10

#include "dcc_app.h"
#include "dcc_cross.h"
#include "dcc_err.h"
#include "radiotap.h"
#include "radiotap_iter.h"


pthread_mutex_t cmds_lock;
int *cmds;

struct ndlType_ndl_database *ndl_database;
struct ndlType_channel_load_measures *ch_load_measures;

    /**
     *  netlink variables definition
     */
#define GENLMSG_DATA(glh) ((void *)(NLMSG_DATA(glh) + GENL_HDRLEN))
#define GENLMSG_PAYLOAD(glh) (NLMSG_PAYLOAD(glh, 0) - GENL_HDRLEN)
#define NLA_DATA(na) ((void *)((char*)(na) + NLA_HDRLEN))

int calc_CS_range(int tx_power);
int est_comm_range(int tx_power, int datarate);
int est_comm_range_inf(int tx_power, int datarate);

void broadcast_udp(char *mess, int cmd_ref)
{
        int sock;
        struct sockaddr_in broadcastAddr;
        char *broadcastIP;
        unsigned short broadcastPort;
        char *sendString;

        struct dcc_msg_data * msg;

        unsigned char *data;

        int broadcastPermision;
        int tos;
        int data_len = 0;
	int temp_measure;
	broadcastIP = "255.255.255.255";
        //broadcastIP = "147.32.216.169";
        //broadcastIP = "192.168.1.10";
	//broadcastIP = "10.4.181.188";

        broadcastPort = 47474;

        msg = malloc(sizeof(struct dcc_msg_data));
	data = malloc(1500);


        if((sock = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP))<0) {
                print_error("socket error");
                exit(1);
        }

        broadcastPermision = 1;
        if(setsockopt(sock, SOL_SOCKET, SO_BROADCAST, (void *) &broadcastPermision, sizeof(broadcastPermision)) < 0) {
		print_error("socket atr error");
		exit(1);
        }


        tos = NDL_IP_COS6;
        if(setsockopt(sock, IPPROTO_IP, IP_TOS,(char *) &tos, sizeof(tos)) < 0) {
		print_error("socket TOS error");
		exit(1);
        }
/*
        tos = NDL_MAC_COS3;
        if(setsockopt(sock, SOL_SOCKET, SO_PRIORITY,(char *) &tos, sizeof(tos)) < 0) {
		print_error("socket MAC error");
		exit(1);
        }
*/
	strcpy(msg->dcc_mark,"DCC");
	msg->msg_type =msg_IN_UNITDATA_REQ;
        msg->cmd_ref = cmd_ref;
	msg->dcc_profile = 7;
	msg->tx_power = 260;
	msg->mcs = 4;
	msg->data = mess;
	msg->data_len = strlen(mess);

	memcpy(data,&msg->dcc_mark, 4);
	data_len += 4;

	memcpy(data+data_len,&msg->msg_type, sizeof(unsigned char));
	data_len += sizeof(unsigned char);

	memcpy(data+data_len,&msg->cmd_ref, sizeof(unsigned char));
	data_len += sizeof(unsigned char);

	memcpy(data+data_len,&msg->dcc_profile, sizeof(unsigned char));
	data_len += sizeof(unsigned char);

	memcpy(data+data_len,&msg->tx_power, sizeof(unsigned int));
	data_len += sizeof(unsigned int);

	memcpy(data+data_len,&msg->mcs, sizeof(unsigned char));
	data_len += sizeof(unsigned char);

	msg->cs_range = calc_CS_range(msg->tx_power *5);
	memcpy(data+data_len,&msg->cs_range, sizeof(unsigned int));
	data_len += sizeof(unsigned int);

	msg->est_comm_range = est_comm_range(msg->tx_power *5, ndl_datarate_table[msg->mcs]);
	memcpy(data+data_len,&msg->est_comm_range, sizeof(unsigned int));
	data_len += sizeof(unsigned int);

	msg->est_comm_range_inf = est_comm_range_inf(msg->tx_power *5, ndl_datarate_table[msg->mcs]);
	memcpy(data+data_len,&msg->est_comm_range_inf, sizeof(unsigned int));
	data_len += sizeof(unsigned int);

	memcpy(data+data_len,&msg->data_len, sizeof(unsigned int));
	data_len += sizeof(unsigned int);

	memcpy(data+data_len,msg->data, msg->data_len);
	data_len += msg->data_len;


        memset(&broadcastAddr,0, sizeof(broadcastAddr));
        broadcastAddr.sin_family = AF_INET;
        broadcastAddr.sin_addr.s_addr = inet_addr(broadcastIP);
        broadcastAddr.sin_port = htons(broadcastPort);

        if(sendto(sock, (unsigned char *)data, data_len, 0, (struct sockaddr *) &broadcastAddr, sizeof(broadcastAddr)) != data_len) {
		print_error("send error");
		free(data);
		free(msg);
		exit(1);
        }
        //print_debug("Packet from app sent %u.",cmd_ref);
	free(data);
	free(msg);

}

pthread_t *dcc_app_thread;
pthread_t *dcc_pcap_thread;
pthread_t *dcc_rcv_thread;

void *t_loop(void *arg)
{
	unsigned char id = *((unsigned char *) arg);
	int i = 0;
	char * text = "DATA_SENT_TO_ANOTHER_ITS_STATION.DATA_SENT_TO_ANOTHER_ITS_STATION.";
        while(1)
	{

		broadcast_udp(text, i);

		i++;
		if(i>255) i= 0;

                pthread_testcancel();
                usleep(1000000);
                //usleep(300000);
	}
}

void *rcv_loop(void *arg)
{
	unsigned char id = *((unsigned char *) arg);
	int sock;
        unsigned short port;
        struct dcc_msg_data * msg;
        unsigned char *data;
        struct sockaddr_in my_addr, from_addr;
	int data_len;
	int addrlen;
        port = 47474;


	data = malloc(1500);

        if((sock = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP))<0) {
                print_error("socket error");
                exit(1);
        }

        memset(&my_addr,0, sizeof(my_addr));
        my_addr.sin_family = AF_INET;
        my_addr.sin_addr.s_addr = INADDR_ANY;
        my_addr.sin_port = htons(port);
	addrlen = sizeof(from_addr);
        bind(sock, (struct sockaddr *) &my_addr, sizeof(my_addr));

	while(1)
	{
		recvfrom(sock, data, 1500, 0,  (struct sockaddr *) &from_addr, &addrlen);

		msg = malloc(sizeof(struct dcc_msg_data));
		data_len = 0;

		memcpy(&msg->dcc_mark, data, 4);
		data_len += 4;

		if(!check_dcc_message(data)) {
			print_error("Received wrong packet");
			continue;
		}

		memcpy(&msg->msg_type, data+data_len, sizeof(unsigned char));
		data_len += sizeof(unsigned char);

		memcpy(&msg->cmd_ref, data+data_len, sizeof(unsigned char));
		data_len += sizeof(unsigned char);

		memcpy(&msg->dcc_profile, data+data_len, sizeof(unsigned char));
		data_len += sizeof(unsigned char);

		memcpy(&msg->tx_power, data+data_len, sizeof(unsigned int));
		data_len += sizeof(unsigned int);

		memcpy(&msg->mcs, data+data_len, sizeof(unsigned char));
		data_len += sizeof(unsigned char);

		memcpy(&msg->cs_range, data+data_len, sizeof(unsigned int));
		data_len += sizeof(unsigned int);

		memcpy(&msg->est_comm_range, data+data_len, sizeof(unsigned int));
		data_len += sizeof(unsigned int);

		memcpy(&msg->est_comm_range_inf, data+data_len, sizeof(unsigned int));
		data_len += sizeof(unsigned int);

		memcpy(&msg->data_len, data+data_len, sizeof(unsigned int));
		data_len += sizeof(unsigned int);

		msg->data = malloc(msg->data_len);
		memcpy(msg->data, data+data_len, msg->data_len);
		data_len += msg->data_len;


		print_info("Received packet ref ID = %d", msg->cmd_ref);


		free(msg->data);
		free(msg);
                pthread_testcancel();
                usleep(100);
	}
}

unsigned int *packets_up_array;
unsigned int *packets_down_array;
unsigned int *air_time_sum_array;

int array_index;


void pcap_callback(u_char *arg, const struct pcap_pkthdr *header, const u_char *packet)
{

	int err;
	int ret;
	int8_t temp;
        time_t t;
        struct tm * timeinfo;
	int delta;
	float result1, result2;
	int rate = 0;
	int rx_rate = 30;
	int8_t rx_signal=0;

	struct ieee80211_radiotap_iterator iter;
        struct ieee80211_radiotap_header * pck = (struct ieee80211_radiotap_header *)packet;

	ret = ieee80211_radiotap_iterator_init(&iter,pck,pck->it_len,NULL);

        while(!ret) {
                ret = ieee80211_radiotap_iterator_next(&iter);

                if(ret)
			continue;

		switch (iter.this_arg_index) {

		case IEEE80211_RADIOTAP_DBM_ANTSIGNAL:
			rx_signal = (*iter.this_arg);
			//print_info("rx %d", rx_signal);
			break;
		case IEEE80211_RADIOTAP_RATE:
			rate = (*iter.this_arg) * 5;
			break;
		}
        }

        if(rate <= 30)
		rx_rate = 30;
	else
		rx_rate = rate;

	ndlType_rx_power treshold = decode_from_NDL_rx_power(ndl_database->NDL_receive_model_parameters->NDL_def_dcc_sensitivity) / 10 + 50;

        if(rx_signal > treshold) {

                packets_up_array[array_index]++;
                air_time_sum_array[array_index] += get_air_time(header->len,rx_rate);

        }else
        {
        	packets_down_array[array_index]++;

        }

}

void *run_pcap_mod()
{
	char *dev, errbuf[PCAP_ERRBUF_SIZE];
	time_t t;
	int timeout;
	int number_of_loop = 0;
	int array_limit = 10000;		//set to array size
	array_index = 0;
	packets_up_array = malloc(10000*sizeof(unsigned int));
	packets_down_array = malloc(10000*sizeof(unsigned int));
	air_time_sum_array = malloc(10000*sizeof(unsigned int));

	int i =0;

	pcap_t *handle;
	int ret = 0;
	struct pcap_pkthdr header;
	struct bpf_program fp;
	dev = "wlan0";//pcap_lookupdev(errbuf);
	timeout = 1;

	handle = pcap_create(dev,errbuf);

        if (handle == NULL) {
		print_error("Failed during interface opening.");
		return NULL;
        }

        ret = pcap_set_rfmon(handle,1);
	if(ret != 0) {
		print_error("Failed during init.");
		return NULL;
	}

	pcap_set_timeout(handle, timeout);
	pcap_activate(handle);


//        if(pcap_compile(handle,&fp, "type data or type mgt",0,PCAP_NETMASK_UNKNOWN) == -1)
        if(pcap_compile(handle,&fp, "llc",0,PCAP_NETMASK_UNKNOWN) == -1)
	{
                print_info("failed compile filter");
                exit(EXIT_FAILURE);
	}

        if(pcap_setfilter(handle,&fp) == -1)
	{
                print_info("failed set filter");
                exit(EXIT_FAILURE);
	}



	//pcap_loop(handle,-1, pcap_callback, NULL);

        while(1)
	{
		number_of_loop++;
                ret = pcap_dispatch(handle, -1, pcap_callback, NULL);



		if(number_of_loop*timeout>=1000) {	//calculate stats every second
			int local_counts = 0;
			int packets_up_sum = 0;
			int packets_down_sum = 0;
			int air_time_sum = 0;
			float temp_result_1 = 0;
			float temp_result_2 = 0;
			unsigned int chanLoad = 0;


			for (i = 0; i<array_limit; i++)
			{
				packets_up_sum += packets_up_array[i];
				packets_down_sum += packets_down_array[i];
				air_time_sum += air_time_sum_array[i];
			}


			temp_result_1 = (packets_up_sum * 1000 );
			if (packets_up_sum == 0)
				packets_up_sum=1;
			if (packets_down_sum == 0)
				packets_down_sum=1;

			chanLoad = temp_result_1 / (packets_up_sum + packets_down_sum);
			ch_load_measures->NDL_channel_load = encode_to_NDL_channel_load((ndlType_channel_load)chanLoad);
			ch_load_measures->NDL_channel_busy_time = encode_to_NDL_channel_load((ndlType_channel_load)chanLoad);

			temp_result_2 = packets_up_sum / 10.0;
			ch_load_measures->NDL_load_arrival_rate = encode_to_NDL_arrival_rate((ndlType_arrival_rate)temp_result_2);
			ch_load_measures->NDL_packet_arrival_rate = encode_to_NDL_arrival_rate((ndlType_arrival_rate)temp_result_2);

			ch_load_measures->NDL_load_avg_duration = encode_to_NDL_packet_duration((ndlType_packet_duration)(temp_result_1 / temp_result_2) * 100);

			temp_result_1 = air_time_sum / packets_up_sum;
			ch_load_measures->NDL_packet_avg_duration = encode_to_NDL_packet_duration((ndlType_packet_duration)temp_result_1);


			print_debug("Actual chanload %d in 0.1%, Packets above %d, Packets below %d",chanLoad, packets_up_sum, packets_down_sum);


			send_rx_stats(&ch_load_measures);

			number_of_loop = 0;
			array_index++;
			if (array_index>=array_limit)
				array_index = 0;

			air_time_sum_array[array_index] = 0;
			packets_up_array[array_index] = 0;
			packets_down_array[array_index] = 0;
                }


                if (array_index>=array_limit)
			array_index = 0;
	}

	ret = pcap_datalink(handle);

	pcap_close(handle);


	return NULL;
}
	//txpower with one shifted point 23.0 = 230
int calc_CS_range(int tx_power)
{
	struct ndlType_ndl_database *database_ptr = ndl_database;
        double pathloss, max_tx_power, temp_result;
	int max_cs_range;

        pathloss = decode_from_NDL_pathloss(ndl_database->NDL_receive_model_parameters->NDL_ref_pathloss)/10.0;
	max_tx_power = decode_from_NDL_tx_power(ndl_database->NDL_transmit_power_tresholds->NDL_max_tx_power)/10.0;
	max_cs_range = ndl_database->NDL_receive_model_parameters->NDL_max_cs_range;

        temp_result = max_tx_power - (tx_power/10);
        temp_result = (-temp_result/pathloss);
        temp_result = temp_result / 10;
	temp_result = max_cs_range * pow(10, temp_result);
	return temp_result;
}

	//txpower with one shifted point 23.0 = 230, datarate in Mbit with shifter point
int est_comm_range(int tx_power, int datarate)
{
	struct ndlType_ndl_database *database_ptr = ndl_database;
        double pathloss, max_tx_power, temp_result;
	int max_cs_range;


	temp_result = (int)get_index_datarate_table(datarate);

	if (temp_result == -1)
		return -1;

	temp_result = ndl_SNR_table[(int)temp_result]/10;


	pathloss = decode_from_NDL_pathloss(ndl_database->NDL_receive_model_parameters->NDL_ref_pathloss)/10.0;

	temp_result = (-temp_result)/pathloss;
	temp_result = temp_result / 10;
	temp_result = pow(10, temp_result) * calc_CS_range(tx_power);

	return temp_result;
}

	//txpower with one shifted point 23.0 = 230, datarate in Mbit with shifter point
int est_comm_range_inf(int tx_power, int datarate)
{
	struct ndlType_ndl_database *database_ptr = ndl_database;
        double pathloss, ref_tx_power, temp_result;
	int max_cs_range;


	temp_result = (int)get_index_datarate_table(datarate);

	if (temp_result == -1)
		return -1;

	ref_tx_power = decode_from_NDL_tx_power(ndl_database->NDL_transmit_power_tresholds->NDL_ref_tx_power[0])/10.0;

	pathloss = decode_from_NDL_pathloss(ndl_database->NDL_receive_model_parameters->NDL_ref_pathloss)/10.0;

	temp_result = ndl_SNR_table[(int)temp_result]/10;
	temp_result = temp_result + ref_tx_power - (tx_power/10);
	temp_result = temp_result / 10;
	temp_result = pow(10, temp_result) +1;
	temp_result = temp_result / pathloss;
	temp_result = calc_CS_range(ref_tx_power*10)/temp_result;

	return temp_result;
}


int main()
{
	char * command;
	int _status = 0;
	unsigned char *index = malloc(sizeof(unsigned char));
	int temp = 0;

	command = malloc(80 * sizeof(char));


	pthread_mutex_init(&(cmds_lock), NULL);
	cmds = malloc(4 * sizeof(int));


	_status |= init_NDL_memory(&ndl_database);

	pthread_mutex_lock(&(ndl_database->database_in_use));
	_status |= set_NDL_default_values(&ndl_database, G5CC);
	pthread_mutex_unlock(&(ndl_database->database_in_use));

	ch_load_measures = ndl_database->NDL_channel_load_measures;


	*index = 0;
	dcc_app_thread = malloc(sizeof(pthread_t));
	_status = pthread_create(dcc_app_thread, NULL, t_loop, index);

	index = malloc(sizeof(unsigned char));
	*index = 1;
	dcc_rcv_thread = malloc(sizeof(pthread_t));
	_status = pthread_create(dcc_rcv_thread, NULL, rcv_loop, index);

	dcc_pcap_thread = malloc(sizeof(pthread_t));
	_status = pthread_create(dcc_pcap_thread, NULL, run_pcap_mod, NULL);

	while(1)
	{
                print_info("Enter command:");
                scanf("%s", command);
		usleep(500000);


                if(strcmp(command,"exit") == 0)
			break;
	}

        _status = pthread_cancel(*dcc_app_thread);
        _status = pthread_cancel(*dcc_pcap_thread);
        _status = pthread_cancel(*dcc_rcv_thread);

//	send_ndl_database_attributes(&ndl_database);
//      receive_ndl_database_attributes();
	destroy_NDL_memory(&ndl_database);

    return 0;
}



/**
*
*	NETLINK related functions
*
*
**/


#if 0
static inline  int16_t nla_get_s16(const struct nlattr *nla)
{
	return *(int16_t *) nla_data(nla);
}

static inline  int32_t nla_get_s32(const struct nlattr *nla)
{
	return *(int32_t *) nla_data(nla);
}
#endif

int send_rx_stats(struct ndlType_channel_load_measures **ch_load_measures)
{
	struct nl_sock *sock;
	struct nl_msg *msg;
	struct ndlType_channel_load_measures *_ch_load_measures = *ch_load_measures;
	int family;
	int temp = 0;
	int err = 0;

	sock = nl_socket_alloc();
	genl_connect(sock);

	family = genl_ctrl_resolve(sock,"DCC_PROTOCOL");

	msg = nlmsg_alloc();
	genlmsg_put(msg, NL_AUTO_PID, NL_AUTO_SEQ, family, 0, 0, NDL_CMD_SEND_RX_STATS, 0);

       	temp = _ch_load_measures->NDL_channel_load;
	nla_put_u32(msg, NDL_STATS_CHANNEL_LOAD, temp);

	temp = _ch_load_measures->NDL_load_arrival_rate;
	nla_put_u32(msg, NDL_STATS_LOAD_ARR_RATE, temp);

	temp = _ch_load_measures->NDL_load_avg_duration;
	nla_put_u32(msg, NDL_STATS_LOAD_AVG_DURATION, temp);

	temp = _ch_load_measures->NDL_packet_arrival_rate;
	nla_put_u32(msg, NDL_STATS_PKT_ARR_RATE, temp);

	temp = _ch_load_measures->NDL_packet_avg_duration;
	nla_put_u32(msg, NDL_STATS_PKT_AVG_DURATION, temp);

	temp = _ch_load_measures->NDL_channel_busy_time;
	nla_put_u32(msg, NDL_STATS_CHANNEL_BUSY, temp);

	err = nl_send_auto(sock, msg);

	if(err<0) {
		print_error("Netlink message sending failed");
		exit(err);
	}

	nlmsg_free(msg);
	nl_socket_free(sock);

	return 0;
}


int send_ndl_database_attributes(struct ndlType_ndl_database **_database)
{
	struct nl_sock *sock;
	struct nl_msg *msg;
	struct ndlType_ndl_database *database_ptr = *_database;
	int family;
	int temp = 0;
	int i = 0;

	sock = nl_socket_alloc();
	genl_connect(sock);

	family = genl_ctrl_resolve(sock,"DCC_PROTOCOL");

	msg = nlmsg_alloc();
	genlmsg_put(msg, NL_AUTO_PID, NL_AUTO_SEQ, family, 0, 0, NDL_CMD_SET_DATABASE_ATTRIBUTES, 0);

	pthread_mutex_lock(&(database_ptr->database_in_use));

// LOCK MEMORY FOR NDL DATABASE
	temp = decode_from_NDL_tx_power(database_ptr->NDL_transmit_power_tresholds->NDL_min_tx_power);
	nla_put_u32(msg, NDL_MIN_TX_POWER, temp);

	temp = decode_from_NDL_tx_power(database_ptr->NDL_transmit_power_tresholds->NDL_max_tx_power);
	nla_put_u32(msg, NDL_MAX_TX_POWER, temp);

	for (i=0 ; i<4 ; i++) {
		temp = decode_from_NDL_tx_power(database_ptr->NDL_transmit_power_tresholds->NDL_def_tx_power[i]);
		nla_put_u32(msg, NDL_DEF_TX_POWER_INDEX_0+i, temp);
	}

	for (i=0 ; i<4 ; i++) {
		temp = decode_from_NDL_packet_duration(database_ptr->NDL_packet_timing_tresholds->NDL_max_packet_duration[i]);
		nla_put_u32(msg, NDL_MAX_PACKET_DURATION_INDEX_0+i, temp);
	}

	temp = decode_from_NDL_packet_interval(database_ptr->NDL_packet_timing_tresholds->NDL_min_packet_interval);
	nla_put_u32(msg, NDL_MIN_PACKET_INTERVAL, temp);

	temp = decode_from_NDL_packet_interval(database_ptr->NDL_packet_timing_tresholds->NDL_max_packet_interval);
	nla_put_u32(msg, NDL_MAX_PACKET_INTERVAL, temp);

	for (i=0 ; i<4 ; i++) {
		temp = decode_from_NDL_packet_interval(database_ptr->NDL_packet_timing_tresholds->NDL_def_packet_interval[i]);
		nla_put_u32(msg, NDL_DEF_PACKET_INTERVAL_INDEX_0+i, temp);
	}

	temp = database_ptr->NDL_packet_datarate_tresholds->NDL_min_datarate;
        nla_put_u16(msg, NDL_MIN_DATARATE, temp);

	temp = database_ptr->NDL_packet_datarate_tresholds->NDL_max_datarate;
        nla_put_u16(msg, NDL_MAX_DATARATE, temp);

        for (i=0 ; i<4 ; i++) {
		temp = database_ptr->NDL_packet_datarate_tresholds->NDL_def_datarate[i];
		nla_put_u16(msg, NDL_DEF_DATARATE_INDEX_0+i, temp);
	}

	temp = decode_from_NDL_rx_power(database_ptr->NDL_receive_signal_tresholds->NDL_min_carrier_sense);
        nla_put_u32(msg, NDL_MIN_CARRIER_SENSE, temp);

	temp = decode_from_NDL_rx_power(database_ptr->NDL_receive_signal_tresholds->NDL_max_carrier_sense);
        nla_put_u32(msg, NDL_MAX_CARRIER_SENSE, temp);

	temp = decode_from_NDL_rx_power(database_ptr->NDL_receive_signal_tresholds->NDL_def_carrier_sense);
        nla_put_u32(msg, NDL_DEF_CARRIER_SENSE, temp);

	temp = decode_from_NDL_rx_power(database_ptr->NDL_receive_model_parameters->NDL_def_dcc_sensitivity);
        nla_put_u32(msg, NDL_DEF_DCC_SENSITIVITY, temp);

	temp = database_ptr->NDL_receive_model_parameters->NDL_max_cs_range;
        nla_put_u32(msg, NDL_MAX_CS_RANGE, temp);

	temp = decode_from_NDL_pathloss(database_ptr->NDL_receive_model_parameters->NDL_ref_pathloss);
        nla_put_u32(msg, NDL_REF_PATHLOSS, temp);

	temp = decode_from_NDL_pathloss(database_ptr->NDL_receive_model_parameters->NDL_min_snr);
        nla_put_u32(msg, NDL_MIN_SNR, temp);

	for (i=0 ; i<8 ; i++) {
		temp = decode_from_NDL_snr(database_ptr->NDL_demodulation_model_parameters->NDL_snr_backoff[i]);
		nla_put_u32(msg, NDL_SNR_BACKOFF_INDEX_0+i, temp);
	}

	temp = decode_from_NDL_channel_load(database_ptr->NDL_channel_load_tresholds->NDL_min_channel_load);
        nla_put_u32(msg, NDL_MIN_CHANNEL_LOAD, temp);

	temp = decode_from_NDL_channel_load(database_ptr->NDL_channel_load_tresholds->NDL_max_channel_load);
        nla_put_u32(msg, NDL_MAX_CHANNEL_LOAD, temp);

	temp = database_ptr->NDL_transmit_queue_parameters->NDL_num_queue;
        nla_put_u16(msg, NDL_NUM_QUEUE, temp);

	for (i=0 ; i<4 ; i++) {
		temp = database_ptr->NDL_transmit_queue_parameters->NDL_queue_len[i];
		nla_put_u16(msg, NDL_QUEUE_LEN_INDEX_0+i, temp);
	}

	pthread_mutex_unlock(&(database_ptr->database_in_use));

// UNLOCK MEMORY FOR NDL DATABASE

	nl_send_auto(sock, msg);
	print_debug("Attributes sent.");
	nlmsg_free(msg);
	nl_socket_free(sock);

	return 0;
}

int receive_ndl_database_attributes()
{
	struct nl_sock *sock;
	struct nl_msg *msg;
	int family;

	sock = nl_socket_alloc();
	genl_connect(sock);

	family = genl_ctrl_resolve(sock,"DCC_PROTOCOL");

	msg = nlmsg_alloc();
	genlmsg_put(msg, NL_AUTO_PID, NL_AUTO_SEQ, family, 0, 0, NDL_CMD_GET_DATABASE_ATTRIBUTES, 0);

	print_debug("Sending request for NDL attributes.");

	nla_put_string(msg, 0,"Request for NDL attributes.");

	nl_send_auto(sock, msg);
	nlmsg_free(msg);
	nl_socket_modify_cb(sock,NL_CB_VALID,NL_CB_CUSTOM, receive_ndl_database_attributes_handler, NULL);
	nl_recvmsgs_default(sock);
	nl_socket_free(sock);
	return 0;
}

int receive_ndl_database_attributes_handler(struct nl_msg *_msg, void * arg)
{
	print_debug("Receive NDL attributes, handler function.");

	struct nlmsghdr *nlhdr;
	struct nlattr *nlattrs [NDL_ATTR_MAX];
	struct nlattr *attributes;
	int temp;
	struct ndlType_ndl_database *database_ptr = ndl_database;
	int i = 0;

	nlhdr = nlmsg_hdr(_msg);
	genlmsg_parse(nlhdr,0,nlattrs, NDL_ATTR_MAX, NULL);



	pthread_mutex_lock(&(database_ptr->database_in_use));

	for (i=0 ; i<4 ; i++) {
                attributes = nlattrs[NDL_REF_TX_POWER_INDEX_0 + i];
		if (attributes) {
			temp = nla_get_s32(attributes);
			database_ptr->NDL_transmit_power_tresholds->NDL_ref_tx_power[i] = encode_to_NDL_tx_power(temp);
		} else {
			print_warning("no attribute %d.\n", NDL_REF_TX_POWER_INDEX_0 + i);
		}
	}

	for (i=0 ; i<4 ; i++) {
                attributes = nlattrs[NDL_REF_PACKET_INTERVAL_INDEX_0 + i];
		if (attributes) {
			temp = nla_get_u32(attributes);
			database_ptr->NDL_packet_timing_tresholds->NDL_ref_packet_interval[i] = encode_to_NDL_packet_interval(temp);
		} else {
			print_warning("no attribute %d.\n", NDL_REF_PACKET_INTERVAL_INDEX_0 + i);
		}
	}

	for (i=0 ; i<4 ; i++) {
                attributes = nlattrs[NDL_REF_DATARATE_INDEX_0 + i];
		if (attributes) {
			temp = nla_get_u16(attributes);
			database_ptr->NDL_packet_datarate_tresholds->NDL_ref_datarate[i] = temp;
		} else {
			print_warning("no attribute %d.\n", NDL_REF_DATARATE_INDEX_0 + i);
		}
	}

	attributes = nlattrs[NDL_REF_CARRIER_SENSE];
	if (attributes) {
		temp = nla_get_s32(attributes);
		database_ptr->NDL_receive_signal_tresholds->NDL_ref_carrier_sense = encode_to_NDL_rx_power(temp);
	} else {
		print_warning("no attribute %d.\n", NDL_REF_CARRIER_SENSE);
	}

	for (i=0 ; i<4 ; i++) {
                attributes = nlattrs[NDL_TM_PACKET_ARRIVAL_RATE_INDEX_0 + i];
		if (attributes) {
			temp = nla_get_u32(attributes);
			database_ptr->NDL_transmit_model_parameters->NDL_tm_packet_arrival_rate[i] = encode_to_NDL_arrival_rate(temp);
		} else {
			print_warning("no attribute %d.\n", NDL_TM_PACKET_ARRIVAL_RATE_INDEX_0 + i);
		}
	}

	for (i=0 ; i<4 ; i++) {
                attributes = nlattrs[NDL_TM_PACKET_AVG_DURATION_INDEX_0 + i];
		if (attributes) {
			temp = nla_get_u32(attributes);
			database_ptr->NDL_transmit_model_parameters->NDL_tm_packet_avg_duration[i] = encode_to_NDL_packet_duration(temp);
		} else {
			print_warning("no attribute %d.\n", NDL_TM_PACKET_AVG_DURATION_INDEX_0 + i);
		}
	}

	for (i=0 ; i<4 ; i++) {
                attributes = nlattrs[NDL_TM_SIGNAL_AVG_POWER_INDEX_0 + i];
		if (attributes) {
			temp = nla_get_s32(attributes);
			database_ptr->NDL_transmit_model_parameters->NDL_tm_signal_avg_power[i] = encode_to_NDL_tx_power(temp);
		} else {
			print_warning("no attribute %d.\n", NDL_TM_SIGNAL_AVG_POWER_INDEX_0 + i);
		}
	}

	attributes = nlattrs[NDL_MAX_CHANNEL_USE];
	if (attributes) {
		temp = nla_get_u32(attributes);
		database_ptr->NDL_transmit_model_parameters->NDL_max_channel_use = encode_to_NDL_channel_use(temp);
	} else {
		print_warning("no attribute %d.\n", NDL_MAX_CHANNEL_USE);
	}

	for (i=0 ; i<4 ; i++) {
                attributes = nlattrs[NDL_TM_CHANNEL_USE_INDEX_0 + i];
		if (attributes) {
			temp = nla_get_u32(attributes);
			database_ptr->NDL_transmit_model_parameters->NDL_tm_channel_use[i] = encode_to_NDL_channel_use(temp);
		} else {
			print_warning("no attribute %d.\n", NDL_TM_CHANNEL_USE_INDEX_0 + i);
		}
	}

	for (i=0 ; i<4 ; i++) {
                attributes = nlattrs[NDL_REF_QUEUE_STATUS_INDEX_0 + i];
		if (attributes) {
			temp = nla_get_u16(attributes);
			database_ptr->NDL_transmit_queue_parameters->NDL_ref_queue_status[i] = temp;
		} else {
			print_warning("no attribute %d.\n", NDL_REF_QUEUE_STATUS_INDEX_0 + i);
		}
	}

	for (i=0 ; i<8 ; i++) {
                attributes = nlattrs[NDL_REQUIRED_SNR_INDEX_0 + i];
		if (attributes) {
			temp = nla_get_s32(attributes);
			database_ptr->NDL_demodulation_model_parameters->NDL_required_snr[i] = encode_to_NDL_snr(temp);
		} else {
			print_warning("no attribute %d.\n", NDL_REQUIRED_SNR_INDEX_0 + i);
		}
	}

	attributes = nlattrs[NDL_CARRIER_SENSE_RANGE];
	if (attributes) {
		temp = nla_get_u32(attributes);
		database_ptr->NDL_communication_ranges->NDL_carrier_sense_range = temp;
	} else {
		print_warning("no attribute %d.\n", NDL_CARRIER_SENSE_RANGE);
	}

	attributes = nlattrs[NDL_EST_COMM_RANGE];
	if (attributes) {
		temp = nla_get_u32(attributes);
		database_ptr->NDL_communication_ranges->NDL_est_comm_range = temp;
	} else {
		print_warning("no attribute %d.\n", NDL_EST_COMM_RANGE);
	}

	attributes = nlattrs[NDL_EST_COMM_RANGE_INTF];
	if (attributes) {
		temp = nla_get_u32(attributes);
		database_ptr->NDL_communication_ranges->NDL_est_comm_range_intf = temp;
	} else {
		print_warning("no attribute %d.\n", NDL_EST_COMM_RANGE_INTF);
	}

	attributes = nlattrs[NDL_CHANNEL_LOAD];
	if (attributes) {
		temp = nla_get_u32(attributes);
		database_ptr->NDL_channel_load_measures->NDL_channel_load= encode_to_NDL_channel_load(temp);
	} else {
		print_warning("no attribute %d.\n", NDL_CHANNEL_LOAD);
	}

	attributes = nlattrs[NDL_LOAD_ARRIVAL_RATE];
	if (attributes) {
		temp = nla_get_u32(attributes);
		database_ptr->NDL_channel_load_measures->NDL_load_arrival_rate = encode_to_NDL_arrival_rate(temp);
	} else {
		print_warning("no attribute %d.\n", NDL_LOAD_ARRIVAL_RATE);
	}

	attributes = nlattrs[NDL_LOAD_AVG_DURATION];
	if (attributes) {
		temp = nla_get_u32(attributes);
		database_ptr->NDL_channel_load_measures->NDL_load_avg_duration = encode_to_NDL_packet_duration(temp);
	} else {
		print_warning("no attribute %d.\n", NDL_LOAD_AVG_DURATION);
	}

	attributes = nlattrs[NDL_PACKET_ARRIVAL_RATE];
	if (attributes) {
		temp = nla_get_u32(attributes);
		database_ptr->NDL_channel_load_measures->NDL_packet_arrival_rate = encode_to_NDL_arrival_rate(temp);
	} else {
		print_warning("no attribute %d.\n", NDL_PACKET_ARRIVAL_RATE);
	}

	attributes = nlattrs[NDL_PACKET_AVG_DURATION];
	if (attributes) {
		temp = nla_get_u32(attributes);
		database_ptr->NDL_channel_load_measures->NDL_packet_avg_duration = encode_to_NDL_packet_duration(temp);
	} else {
		print_warning("no attribute %d.\n", NDL_PACKET_AVG_DURATION);
	}

	attributes = nlattrs[NDL_CHANNEL_BUSY_TIME];
	if (attributes) {
		temp = nla_get_u32(attributes);
		database_ptr->NDL_channel_load_measures->NDL_channel_busy_time = encode_to_NDL_channel_load(temp);
	} else {
		print_warning("no attribute %d.\n", NDL_CHANNEL_BUSY_TIME);
	}

	for (i=0 ; i<4 ; i++) {
                attributes = nlattrs[NDL_TX_PACKET_ARRIVAL_RATE_INDEX_0 + i];
		if (attributes) {
			temp = nla_get_u32(attributes);
			database_ptr->NDL_transmit_packet_statistics->NDL_tx_packet_arrival_rate[i] = encode_to_NDL_arrival_rate(temp);
		} else {
			print_warning("no attribute %d.\n", NDL_TX_PACKET_ARRIVAL_RATE_INDEX_0 + i);
		}
	}

	for (i=0 ; i<4 ; i++) {
                attributes = nlattrs[NDL_TX_PACKET_AVG_DURATION_INDEX_0 + i];
		if (attributes) {
			temp = nla_get_u32(attributes);
			database_ptr->NDL_transmit_packet_statistics->NDL_tx_packet_avg_duration[i] = encode_to_NDL_packet_duration(temp);
		} else {
			print_warning("no attribute %d.\n", NDL_TX_PACKET_AVG_DURATION_INDEX_0 + i);
		}
	}

	for (i=0 ; i<4 ; i++) {
                attributes = nlattrs[NDL_TX_CHANNEL_USE_INDEX_0 + i];
		if (attributes) {
			temp = nla_get_u32(attributes);
			database_ptr->NDL_transmit_packet_statistics->NDL_tx_channel_use[i] = encode_to_NDL_channel_use(temp);
		} else {
			print_warning("no attribute %d.\n", NDL_TX_CHANNEL_USE_INDEX_0 + i);
		}
	}

	for (i=0 ; i<4 ; i++) {
		attributes = nlattrs[NDL_TX_SIGNAL_AVG_POWER_INDEX_0 + i];
		if (attributes) {
			temp = nla_get_s32(attributes);
			database_ptr->NDL_transmit_packet_statistics->NDL_tx_signal_avg_power[i] = encode_to_NDL_tx_power(temp);
		} else {
			print_warning("no attribute %d.\n", NDL_TX_SIGNAL_AVG_POWER_INDEX_0 + i);
		}
	}
	pthread_mutex_unlock(&(database_ptr->database_in_use));
        ndl_database = database_ptr;
        return 0;
}

