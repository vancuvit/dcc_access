#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/netfilter.h>
#include <linux/netfilter_ipv4.h>
#include <linux/slab.h>
#include <linux/inet.h>
#include <linux/time.h>
#include <linux/ktime.h>
#include <linux/ip.h>
#include <linux/skbuff.h>
#include <linux/udp.h>
#include <net/checksum.h>

//#define DCC_DEBUG
#define DCC_PREFIX	"DCC NET"

#include "dcc_cross.h"
#include "dcc_err.h"

MODULE_LICENSE("GPL");

struct nf_hook_ops dcc_nf_hook_out;
struct nf_hook_ops dcc_nf_hook_in;

unsigned int dcc_nf_hook_out_func( unsigned int hooknum, struct sk_buff *skb, const struct net_device *in, const struct net_device *out, int (*okfn)(struct sk_buff *))
{
	//struct iphdr *iph;
	//struct ethhdr *mh;
	struct dcc_msg_data *dcc_data;
	//unsigned char temp, *c;
	//int i;

        if(skb->protocol != htons(ETH_P_IP)) // ETH_P_IPV6
		return NF_ACCEPT;


	// ETH + IP HEADER SIZE
        if(check_dcc_message(skb->data+28) == 0) {
		return NF_ACCEPT;
        }
        //make timestamp
        if (skb->tstamp.tv64 == 0)
	{
                struct timeval now;
                do_gettimeofday(&now);
                skb->tstamp = timeval_to_ktime(now);
	}

	print_debug("Arrival time %u s: %u us", ns_to_timeval(skb->tstamp.tv64).tv_sec, ns_to_timeval(skb->tstamp.tv64).tv_usec);

	dcc_data = (struct dcc_msg_data*) (skb->data+28);

	//print_debug("DCC packet");

/*
	mh = eth_hdr(skb);
	iph = ip_hdr(skb);
        temp = iph->tos;
        iph->tos = NDL_IP_COS5;

        c = skb->data;
	print_debug("Source MAC=%x:%x:%x:%x:%x:%x\n",mh->h_source[0],mh->h_source[1],mh->h_source[2],mh->h_source[3],mh->h_source[4],mh->h_source[5]);
	print_debug("Out source IP = %pI4, destination IP = %pI4 ", &(iph->saddr),&(iph->daddr));

        for (i = 28; i<skb->len; i++)
	{
		printk(KERN_ERR "%d - %c", *(c+i), i);

	}
        csum_replace2(&iph->check, htons(temp),htons(iph->tos)); // recalculate ip checksum
*/
	return NF_ACCEPT;
}


int init_net_module(void)
{
	dcc_nf_hook_out.hook = dcc_nf_hook_out_func;
	dcc_nf_hook_out.hooknum = NF_INET_POST_ROUTING;
	dcc_nf_hook_out.pf = PF_INET;
	dcc_nf_hook_out.priority = NF_IP_PRI_FIRST;

	nf_register_hook(&dcc_nf_hook_out);

	print_info("DCC_net support module loaded successfully!");
	return 0;
}

void cleanup_net_module(void)
{
	nf_unregister_hook(&dcc_nf_hook_out);

	print_info("DCC_net support module unloaded successfully!");
}
