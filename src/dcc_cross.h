#ifndef DCC_CROSS_H_INCLUDED
#define DCC_CROSS_H_INCLUDED

#ifdef __KERNEL__
#include <linux/slab.h>
#include <net/netlink.h>
#include <net/genetlink.h>
#include <net/sock.h>
#include <linux/spinlock.h>
#include <linux/nl80211.h>

#else

#include <stdio.h>
#include <stdlib.h>
#include <linux/netlink.h>
#include <linux/genetlink.h>
#include <pthread.h>
#include <linux/nl80211.h>

#define kmalloc(x,y)    malloc(x)
#define kfree(x)    free(x)

#endif


#define G5CC    0
#define G5SC    1



    /**
    *   Definition of basic variables for DCC protocol.
    */
typedef unsigned char ndlType_ac_prio;
typedef unsigned char ndlType_control_loop;
typedef unsigned int ndlType_arrival_rate;
typedef unsigned int ndlType_channel_load;
typedef unsigned int ndlType_channel_use;
typedef unsigned char ndlType_datarate;
typedef unsigned int ndlType_distance;
typedef unsigned char ndlType_number_elements;
typedef unsigned int ndlType_packet_duration;
typedef unsigned int ndlType_packet_interval;
typedef unsigned int ndlType_pathloss;
typedef int ndlType_rx_power;
typedef int ndlType_snr;
typedef unsigned int ndlType_timing;
typedef int ndlType_tx_power;
typedef unsigned char ndlType_ratio;
typedef unsigned int ndlType_exponent;

//
extern int ndl_datarate_table [8];;
extern int ndl_SNR_table [8];


enum  ndlType_queue_status {
        DCC_QUEUE_CLOSED = 0,
        DCC_QUEUE_OPEN = 1
};

enum ndlType_dcc_mechanism {
	DCCm_NONE,
	DCCm_TPC_bit	= 1 << 0,
	DCCm_TRC_bit	= 1 << 1,
	DCCm_TDC_bit	= 1 << 2,
	DCCm_DSC_bit	= 1 << 3,
	DCCm_TAC_bit	= 1 << 4,
	/* bit 5 is reserved */
	DCCm_ALL	= (1 << 6) - 1,
};


enum ndlType_DCC_access_CL_state {
        DCC_RELAXED = 0,
        DCC_RESTRICTIVE = 1,
        DCC_ACTIVE = 2
};


struct ndlType_transmit_power_tresholds {
	unsigned char msg_type;
        ndlType_tx_power NDL_min_tx_power;
        ndlType_tx_power NDL_max_tx_power;
        ndlType_tx_power *NDL_def_tx_power;    //according queues
        ndlType_tx_power *NDL_ref_tx_power;    //according queues
};

struct ndlType_packet_timing_tresholds {
	unsigned char msg_type;
        ndlType_packet_duration *NDL_max_packet_duration;  //according queues
        ndlType_packet_interval NDL_min_packet_interval;
        ndlType_packet_interval NDL_max_packet_interval;
        ndlType_packet_interval *NDL_def_packet_interval;  //according queues
        ndlType_packet_interval *NDL_ref_packet_interval;  //according queues
};

struct ndlType_packet_datarate_tresholds {
	unsigned char msg_type;
        ndlType_datarate NDL_min_datarate;
        ndlType_datarate NDL_max_datarate;
        ndlType_datarate *NDL_def_datarate;  //according queues
        ndlType_datarate *NDL_ref_datarate;  //according queues
};

struct ndlType_receive_signal_tresholds {
	unsigned char msg_type;
        ndlType_rx_power NDL_min_carrier_sense;
        ndlType_rx_power NDL_max_carrier_sense;
        ndlType_rx_power NDL_def_carrier_sense;
        ndlType_rx_power NDL_ref_carrier_sense;
};

struct ndlType_receive_model_parameters {
	unsigned char msg_type;
        ndlType_rx_power NDL_def_dcc_sensitivity;
        ndlType_distance NDL_max_cs_range;
        ndlType_pathloss NDL_ref_pathloss;
        ndlType_snr NDL_min_snr;
};

struct ndlType_demodulation_model_parameters {
	unsigned char msg_type;
        ndlType_snr *NDL_snr_backoff;    //according used datarate
        ndlType_snr *NDL_required_snr;   //according used datarate
};

struct ndlType_transmit_model_parameters {
	unsigned char msg_type;
        ndlType_arrival_rate *NDL_tm_packet_arrival_rate; //according queues
        ndlType_packet_duration *NDL_tm_packet_avg_duration;    //according queues
        ndlType_tx_power *NDL_tm_signal_avg_power;  //according queues
        ndlType_channel_use NDL_max_channel_use;
        ndlType_channel_use *NDL_tm_channel_use;   //according queues
};

struct ndlType_channel_load_tresholds {
	unsigned char msg_type;
        ndlType_channel_load NDL_min_channel_load;
        ndlType_channel_load NDL_max_channel_load;
};

struct ndlType_transmit_queue_parameters {
	unsigned char msg_type;
        ndlType_ac_prio NDL_num_queue;
        enum ndlType_queue_status *NDL_ref_queue_status;    //according queues
        ndlType_number_elements * NDL_queue_len; //according queues
};

struct ndlType_communication_ranges {
	unsigned char msg_type;
        ndlType_distance NDL_carrier_sense_range;
        ndlType_distance NDL_est_comm_range;
        ndlType_distance NDL_est_comm_range_intf;

};

struct ndlType_channel_load_measures {
	unsigned char msg_type;
        ndlType_channel_load NDL_channel_load;
        ndlType_arrival_rate NDL_load_arrival_rate;
        ndlType_packet_duration NDL_load_avg_duration;
        ndlType_arrival_rate NDL_packet_arrival_rate;
        ndlType_packet_duration NDL_packet_avg_duration;
        ndlType_channel_load NDL_channel_busy_time;
};

struct ndlType_transmit_packet_statictics {
	unsigned char msg_type;
        ndlType_arrival_rate *NDL_tx_packet_arrival_rate;
        ndlType_packet_duration *NDL_tx_packet_avg_duration;
        ndlType_channel_use *NDL_tx_channel_use;
        ndlType_tx_power *NDL_tx_signal_avg_power;
};

struct ndlType_general_configuration {
	unsigned char msg_type;
        ndlType_timing NDL_time_up;
        ndlType_timing NDL_time_down;
	ndlType_timing NDL_min_dcc_sampling;
        ndlType_number_elements NDL_num_active_state;
        ndlType_number_elements NDL_cur_active_state;
};

struct ndlType_state_configuration {
	unsigned char msg_type;
        ndlType_number_elements NDL_as_state_id;
        ndlType_timing NDL_as_time_dn;
        ndlType_channel_load NDL_as_chan_load;
        enum ndlType_dcc_mechanism *NDL_as_dcc;
        ndlType_tx_power *NDL_as_tx_power;
        ndlType_packet_interval *NDL_as_packet_interval;
        ndlType_datarate *NDL_as_datarate;
        ndlType_rx_power *NDL_as_carrier_sense;
};

enum dcc_msg_type {
	msg_NDL_DATABASE,
	msg_DP_TABLE,
	msg_NDL_COMM_RANGES,
	msg_NDL_CHANNEL_LOAD_MEASURES,
	msg_NDL_TRANSMIT_PCK_STATS,
	msg_NDL_DEMODULATION_MODEL,
	msg_NDL_TX_POWER_TH,
	msg_NDL_PCK_TIME_TH,
        msg_NDL_PCK_DRATE_TH,
        msg_NDL_RM_PARAMS,
        msg_NDL_TM_PARAMS,
	msg_IN_UNITDATA_REQ,
	msg_IN_UNITDATA_STAT,
	msg_IN_UNITDATA_INDIC,
	msg_MI_COMMAND_REQ,
	msg_MI_COMMAND_CONF,
        msg_MI_REQUEST_REQ,
        msg_MI_REQUEST_CONF,
        msg_MI_SET_REQ,
        msg_MI_SET_CONF,
        msg_MI_GET_REQ,
        msg_MI_GET_CONF,
};

struct ndlType_ndl_database {
	unsigned char msg_type;
        struct ndlType_transmit_power_tresholds *NDL_transmit_power_tresholds;
        struct ndlType_packet_timing_tresholds *NDL_packet_timing_tresholds;
        struct ndlType_packet_datarate_tresholds *NDL_packet_datarate_tresholds;
        struct ndlType_receive_signal_tresholds *NDL_receive_signal_tresholds;
        struct ndlType_receive_model_parameters *NDL_receive_model_parameters;
        struct ndlType_demodulation_model_parameters *NDL_demodulation_model_parameters;
        struct ndlType_transmit_model_parameters *NDL_transmit_model_parameters;
        struct ndlType_channel_load_tresholds *NDL_channel_load_tresholds;
        struct ndlType_transmit_queue_parameters *NDL_transmit_queue_parameters;
        struct ndlType_communication_ranges *NDL_communication_ranges;
        struct ndlType_channel_load_measures *NDL_channel_load_measures;
        struct ndlType_transmit_packet_statictics *NDL_transmit_packet_statistics;
        struct ndlType_general_configuration *NDL_general_configuration;
        struct ndlType_state_configuration **NDL_state_list;
#ifdef __KERNEL__
        spinlock_t database_in_use;
#else
        pthread_mutex_t database_in_use;
#endif // __KERNEL__
};


struct dpType_dcc_profile {
	unsigned char msg_type;
        unsigned char id;
        int dp_not_available;
        unsigned char num_queueu;
        unsigned int t_off;
};

struct dpType_dp_table {
	unsigned char msg_type;
        struct dpType_dcc_profile ** relaxed;
	struct dpType_dcc_profile ** active;
	struct dpType_dcc_profile ** restrictive;
#ifdef __KERNEL__
        spinlock_t table_in_use;
#else
        pthread_mutex_t table_in_use;
#endif // __KERNEL__
};


        /**
        * enum tos_class_selector - enum of basic transmit priority
	* @NDL_IP_COS1:   belongs to priority queue with priority AC_BE
        * @NDL_IP_COS2:   belongs to priority queue with priority AC_BK, the lowest
        * @NDL_IP_COS0:   belongs to priority queue with priority AC_BK, the lowest
        * @NDL_IP_COS3:   belongs to priority queue with priority AC_BE
        * @NDL_IP_COS4:   belongs to priority queue with priority AC_VI
        * @NDL_IP_COS5:   belongs to priority queue with priority AC_VI
        * @NDL_IP_COS6:   belongs to priority queue with priority AC_VO, the highest
        * @NDL_IP_COS7:   belongs to priority queue with priority AC_VO, the highest
        * @NDL_MAC_COS1:   belongs to priority queue with priority AC_BE
        * @NDL_MAC_COS2:   belongs to priority queue with priority AC_BK, the lowest
        * @NDL_MAC_COS0:   belongs to priority queue with priority AC_BK, the lowest
        * @NDL_MAC_COS3:   belongs to priority queue with priority AC_BE
        * @NDL_MAC_COS4:   belongs to priority queue with priority AC_VI
        * @NDL_MAC_COS5:   belongs to priority queue with priority AC_VI
        * @NDL_MAC_COS6:   belongs to priority queue with priority AC_VO, the highest
        * @NDL_MAC_COS7:   belongs to priority queue with priority AC_VO, the highest
        *
        *
        * Because of mac80211 support only IP QoS; MAC QoS is supported only with below values in testing phase.
        * This kind of priorities is used in 802.11 standard
        */
enum tos_class_selector {

	NDL_IP_COS0 = 0,	//AC_BE
	NDL_IP_COS1 = 8 << 2,	//AC_BK
	NDL_IP_COS2 = 16 << 2,	//AC_BK
	NDL_IP_COS3 = 24 << 2,	//AC_BE
        NDL_IP_COS4 = 32 << 2,	//AC_VI
        NDL_IP_COS5 = 40 << 2,	//AC_VI
        NDL_IP_COS6 = 48 << 2,	//AC_VO
        NDL_IP_COS7 = 56 << 2,	//AC_VO

	NDL_MAC_COS0 = 256,	//AC_BE
        NDL_MAC_COS1 = 257,	//AC_BK
        NDL_MAC_COS2 = 258,	//AC_BK
        NDL_MAC_COS3 = 259,	//AC_BE
        NDL_MAC_COS4 = 260,	//AC_VI
        NDL_MAC_COS5 = 261,	//AC_VI
        NDL_MAC_COS6 = 262,	//AC_VO
        NDL_MAC_COS7 = 263,	//AC_VO
};

struct dcc_msg_command_req {
	unsigned char msg_type;
	unsigned char id;	//MAC-ID or DP-ID
	unsigned char command_ref;
	unsigned char mi_command;
	unsigned int mi_command_data;
};

struct dcc_msg_command_conf {
	unsigned char msg_type;
	unsigned char id;	//MAC-ID or DP-ID
	unsigned char command_ref;
	unsigned char err;
};

struct dcc_msg_params {
	unsigned char msg_type;
	unsigned char id;	//MAC-ID or DP-ID
	unsigned char command_ref;
	unsigned char number_of_params;
	unsigned int *data;
};

struct dcc_msg_data {
	char dcc_mark[4];
        unsigned char msg_type;
        unsigned char cmd_ref;
	unsigned char dcc_profile;
	unsigned int tx_power;
	unsigned char mcs;
        unsigned int cs_range;
        unsigned int est_comm_range;
        unsigned int est_comm_range_inf;
	unsigned int data_len;
	unsigned char *data;
};




enum NDL_netlink_commands {
        NDL_CMD_UNSPEC = 0,
        NDL_CMD_SET_DATABASE_ATTRIBUTES,
        NDL_CMD_GET_DATABASE_ATTRIBUTES,
        NDL_CMD_SEND_RX_STATS,
        __NDL_CMD_MAX,
};
#define NDL_CMD_MAX (__NDL_CMD_MAX - 1)


enum NDL_stats_nl_attributes {
        NDL_STATS_ATTR_UNSPEC = 0,
        NDL_STATS_CHANNEL_LOAD,
        NDL_STATS_LOAD_ARR_RATE,
        NDL_STATS_LOAD_AVG_DURATION,
	NDL_STATS_PKT_ARR_RATE,
	NDL_STATS_PKT_AVG_DURATION,
	NDL_STATS_CHANNEL_BUSY,

        __NDL_STATS_ATTR_MAX
};
#define NDL_STATS_ATTR_MAX (__NDL_STATS_ATTR_MAX - 1)

enum NDL_database_nl_attributes {
        NDL_ATTR_UNSPEC = 0,
        NDL_MIN_TX_POWER,
        NDL_MAX_TX_POWER,
        NDL_DEF_TX_POWER_INDEX_0,
        NDL_DEF_TX_POWER_INDEX_1,
        NDL_DEF_TX_POWER_INDEX_2,
        NDL_DEF_TX_POWER_INDEX_3,
        NDL_REF_TX_POWER_INDEX_0,
        NDL_REF_TX_POWER_INDEX_1,
        NDL_REF_TX_POWER_INDEX_2,
        NDL_REF_TX_POWER_INDEX_3,
        NDL_MAX_PACKET_DURATION_INDEX_0,
        NDL_MAX_PACKET_DURATION_INDEX_1,
        NDL_MAX_PACKET_DURATION_INDEX_2,
        NDL_MAX_PACKET_DURATION_INDEX_3,
        NDL_MIN_PACKET_INTERVAL,
        NDL_MAX_PACKET_INTERVAL,
        NDL_DEF_PACKET_INTERVAL_INDEX_0,
        NDL_DEF_PACKET_INTERVAL_INDEX_1,
        NDL_DEF_PACKET_INTERVAL_INDEX_2,
        NDL_DEF_PACKET_INTERVAL_INDEX_3,
        NDL_REF_PACKET_INTERVAL_INDEX_0,
        NDL_REF_PACKET_INTERVAL_INDEX_1,
        NDL_REF_PACKET_INTERVAL_INDEX_2,
        NDL_REF_PACKET_INTERVAL_INDEX_3,

        NDL_MIN_DATARATE,
        NDL_MAX_DATARATE,
        NDL_DEF_DATARATE_INDEX_0,
        NDL_DEF_DATARATE_INDEX_1,
        NDL_DEF_DATARATE_INDEX_2,
        NDL_DEF_DATARATE_INDEX_3,
        NDL_REF_DATARATE_INDEX_0,
        NDL_REF_DATARATE_INDEX_1,
        NDL_REF_DATARATE_INDEX_2,
        NDL_REF_DATARATE_INDEX_3,

        NDL_MIN_CARRIER_SENSE,
        NDL_MAX_CARRIER_SENSE,
        NDL_DEF_CARRIER_SENSE,
        NDL_REF_CARRIER_SENSE,

        NDL_DEF_DCC_SENSITIVITY,
        NDL_MAX_CS_RANGE,
        NDL_REF_PATHLOSS,
        NDL_MIN_SNR,

        NDL_SNR_BACKOFF_INDEX_0,
        NDL_SNR_BACKOFF_INDEX_1,
        NDL_SNR_BACKOFF_INDEX_2,
        NDL_SNR_BACKOFF_INDEX_3,
        NDL_SNR_BACKOFF_INDEX_4,
        NDL_SNR_BACKOFF_INDEX_5,
        NDL_SNR_BACKOFF_INDEX_6,
        NDL_SNR_BACKOFF_INDEX_7,

        NDL_TM_PACKET_ARRIVAL_RATE_INDEX_0,
        NDL_TM_PACKET_ARRIVAL_RATE_INDEX_1,
        NDL_TM_PACKET_ARRIVAL_RATE_INDEX_2,
        NDL_TM_PACKET_ARRIVAL_RATE_INDEX_3,
        NDL_TM_PACKET_AVG_DURATION_INDEX_0,
        NDL_TM_PACKET_AVG_DURATION_INDEX_1,
        NDL_TM_PACKET_AVG_DURATION_INDEX_2,
        NDL_TM_PACKET_AVG_DURATION_INDEX_3,
        NDL_TM_SIGNAL_AVG_POWER_INDEX_0,
        NDL_TM_SIGNAL_AVG_POWER_INDEX_1,
        NDL_TM_SIGNAL_AVG_POWER_INDEX_2,
        NDL_TM_SIGNAL_AVG_POWER_INDEX_3,
        NDL_MAX_CHANNEL_USE,
        NDL_TM_CHANNEL_USE_INDEX_0,
        NDL_TM_CHANNEL_USE_INDEX_1,
        NDL_TM_CHANNEL_USE_INDEX_2,
        NDL_TM_CHANNEL_USE_INDEX_3,

        NDL_MIN_CHANNEL_LOAD,
        NDL_MAX_CHANNEL_LOAD,

        NDL_NUM_QUEUE,
        NDL_REF_QUEUE_STATUS_INDEX_0,
        NDL_REF_QUEUE_STATUS_INDEX_1,
        NDL_REF_QUEUE_STATUS_INDEX_2,
        NDL_REF_QUEUE_STATUS_INDEX_3,
        NDL_QUEUE_LEN_INDEX_0,
        NDL_QUEUE_LEN_INDEX_1,
        NDL_QUEUE_LEN_INDEX_2,
        NDL_QUEUE_LEN_INDEX_3,

        NDL_REQUIRED_SNR_INDEX_0,
        NDL_REQUIRED_SNR_INDEX_1,
        NDL_REQUIRED_SNR_INDEX_2,
        NDL_REQUIRED_SNR_INDEX_3,
        NDL_REQUIRED_SNR_INDEX_4,
        NDL_REQUIRED_SNR_INDEX_5,
        NDL_REQUIRED_SNR_INDEX_6,
        NDL_REQUIRED_SNR_INDEX_7,

        NDL_CARRIER_SENSE_RANGE,
        NDL_EST_COMM_RANGE,
        NDL_EST_COMM_RANGE_INTF,

        NDL_CHANNEL_LOAD,
        NDL_LOAD_ARRIVAL_RATE,
        NDL_LOAD_AVG_DURATION,
        NDL_PACKET_ARRIVAL_RATE,
        NDL_PACKET_AVG_DURATION,
        NDL_CHANNEL_BUSY_TIME,

        NDL_TX_PACKET_ARRIVAL_RATE_INDEX_0,
        NDL_TX_PACKET_ARRIVAL_RATE_INDEX_1,
        NDL_TX_PACKET_ARRIVAL_RATE_INDEX_2,
        NDL_TX_PACKET_ARRIVAL_RATE_INDEX_3,
        NDL_TX_PACKET_AVG_DURATION_INDEX_0,
        NDL_TX_PACKET_AVG_DURATION_INDEX_1,
        NDL_TX_PACKET_AVG_DURATION_INDEX_2,
        NDL_TX_PACKET_AVG_DURATION_INDEX_3,
        NDL_TX_CHANNEL_USE_INDEX_0,
        NDL_TX_CHANNEL_USE_INDEX_1,
        NDL_TX_CHANNEL_USE_INDEX_2,
        NDL_TX_CHANNEL_USE_INDEX_3,
        NDL_TX_SIGNAL_AVG_POWER_INDEX_0,
        NDL_TX_SIGNAL_AVG_POWER_INDEX_1,
        NDL_TX_SIGNAL_AVG_POWER_INDEX_2,
        NDL_TX_SIGNAL_AVG_POWER_INDEX_3,

        NDL_TIME_UP,
        NDL_TIME_DOWN,
        NDL_NUM_ACTIVE_STATE,
        NDL_CUR_ACRIVE_STATE,

        NDL_AS_STATE_ID,
        NDL_AS_TIME_DN,
        NDL_AS_CHAN_LOAD,
        NDL_AS_DCC_INDEX_0,
        NDL_AS_DCC_INDEX_1,
        NDL_AS_DCC_INDEX_2,
        NDL_AS_DCC_INDEX_3,
        NDL_AS_TX_POWER_INDEX_0,
        NDL_AS_TX_POWER_INDEX_1,
        NDL_AS_TX_POWER_INDEX_2,
        NDL_AS_TX_POWER_INDEX_3,
        NDL_AS_PACKET_INTERVAL_INDEX_0,
        NDL_AS_PACKET_INTERVAL_INDEX_1,
        NDL_AS_PACKET_INTERVAL_INDEX_2,
        NDL_AS_PACKET_INTERVAL_INDEX_3,
        NDL_AS_DATARATE_INDEX_0,
        NDL_AS_DATARATE_INDEX_1,
        NDL_AS_DATARATE_INDEX_2,
        NDL_AS_DATARATE_INDEX_3,
        NDL_AS_CARRIER_SENSE_INDEX_0,
        NDL_AS_CARRIER_SENSE_INDEX_1,
        NDL_AS_CARRIER_SENSE_INDEX_2,
        NDL_AS_CARRIER_SENSE_INDEX_3,

        __NDL_ATTR_MAX
};
#define NDL_ATTR_MAX (__NDL_ATTR_MAX - 1)

int init_NDL_memory(struct ndlType_ndl_database **_database);
int destroy_NDL_memory(struct ndlType_ndl_database **_database);
int set_NDL_default_values(struct ndlType_ndl_database **_database, int mode);

int init_DP_table (struct dpType_dp_table ** _state);
int destroy_DP_table (struct dpType_dp_table ** _state);
int set_default_values_DP_cch (struct dpType_dp_table ** _state);
int set_default_values_DP_sch (struct dpType_dp_table ** _state);



int decode_from_NDL_snr(ndlType_snr ndl_value);
int decode_from_NDL_tx_power(ndlType_tx_power ndl_value);
int decode_from_NDL_rx_power(ndlType_rx_power ndl_value);
int decode_from_NDL_timing(ndlType_timing ndl_value);
int decode_from_NDL_pathloss(ndlType_pathloss ndl_value);
int decode_from_NDL_packet_interval(ndlType_packet_interval ndl_value);
int decode_from_NDL_exponent(ndlType_exponent ndl_value);
int decode_from_NDL_channel_load(ndlType_channel_load ndl_value);
int decode_from_NDL_channel_use(ndlType_channel_use ndl_value);
int decode_from_NDL_arrival_rate(ndlType_arrival_rate ndl_value);
int decode_from_NDL_packet_duration(ndlType_packet_duration ndl_value);

ndlType_snr encode_to_NDL_snr(int physical_value);
ndlType_tx_power encode_to_NDL_tx_power(int physical_value);
ndlType_rx_power encode_to_NDL_rx_power(int physical_value);
ndlType_timing encode_to_NDL_timing(int physical_value);
ndlType_pathloss encode_to_NDL_pathloss(int physical_value);
ndlType_packet_interval encode_to_NDL_packet_interval(int physical_value);
ndlType_exponent encode_to_NDL_exponent(int physical_value);
ndlType_channel_load encode_to_NDL_channel_load(int physical_value);
ndlType_channel_use encode_to_NDL_channel_use(int physical_value);
ndlType_arrival_rate encode_to_NDL_arrival_rate(int physical_value);
ndlType_packet_duration encode_to_NDL_packet_duration(int physical_value);

void set_default_values_G5CC(struct ndlType_ndl_database **_database);
void set_default_values_G5SC(struct ndlType_ndl_database **_database);


int check_dcc_message(unsigned char *toCheck);

int get_actual_state(struct ndlType_ndl_database **_database);
void set_actual_state(struct ndlType_ndl_database **_database, int state);
int get_air_time(unsigned int pkt_len, unsigned int datarate);


int get_index_snr_table(ndlType_snr snr);
int get_index_datarate_table(ndlType_datarate datarate);


#ifdef __KERNEL__

void dcc_process_NDL_databases(struct ndlType_ndl_database **local_ndl_database, struct ndlType_ndl_database **global_ndl_database);
void dcc_process_DP_tables(struct dpType_dp_table **local_dp_table, struct dpType_dp_table **global_dp_table);
void dcc_save_load_measures_to_gNDL_database(struct ndlType_channel_load_measures **local_channel_load_measures, struct ndlType_ndl_database **global_ndl_database);
void dcc_save_tx_stats_to_gNDL_database(struct ndlType_transmit_packet_statictics **local_tx_stats, struct ndlType_ndl_database **global_ndl_database);
void dcc_save_comm_ranges_to_gNDL_database(struct ndlType_communication_ranges **local_comm_ranges, struct ndlType_ndl_database **global_ndl_database);
void dcc_save_ref_values_to_gNDL_database(struct ndlType_ndl_database **local_ndl_database, struct ndlType_ndl_database **global_ndl_database);


#endif // __KERNEL__

#endif // DCC_CROSS_H_INCLUDED

